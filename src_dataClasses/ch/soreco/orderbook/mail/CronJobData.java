package ch.soreco.orderbook.mail;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class CronJobData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class CronJobData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 1133917423770991477L;

  /**
   * Contains information if CronJob is already running
   */
  private transient java.lang.Boolean isProcessAlreadyRunning;

  /**
   * Gets the field isProcessAlreadyRunning.
   * @return the value of the field isProcessAlreadyRunning; may be null.
   */
  public java.lang.Boolean getIsProcessAlreadyRunning()
  {
    return isProcessAlreadyRunning;
  }

  /**
   * Sets the field isProcessAlreadyRunning.
   * @param _isProcessAlreadyRunning the new value of the field isProcessAlreadyRunning.
   */
  public void setIsProcessAlreadyRunning(java.lang.Boolean _isProcessAlreadyRunning)
  {
    isProcessAlreadyRunning = _isProcessAlreadyRunning;
  }

  /**
   * Counter for loop over MailBoxes to Scan
   */
  private transient java.lang.Integer counter;

  /**
   * Gets the field counter.
   * @return the value of the field counter; may be null.
   */
  public java.lang.Integer getCounter()
  {
    return counter;
  }

  /**
   * Sets the field counter.
   * @param _counter the new value of the field counter.
   */
  public void setCounter(java.lang.Integer _counter)
  {
    counter = _counter;
  }

  /**
   * List of Mail Box Configurations
   */
  private transient ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.MailBoxConfiguration> mailBoxConfigurations;

  /**
   * Gets the field mailBoxConfigurations.
   * @return the value of the field mailBoxConfigurations; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.MailBoxConfiguration> getMailBoxConfigurations()
  {
    return mailBoxConfigurations;
  }

  /**
   * Sets the field mailBoxConfigurations.
   * @param _mailBoxConfigurations the new value of the field mailBoxConfigurations.
   */
  public void setMailBoxConfigurations(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.MailBoxConfiguration> _mailBoxConfigurations)
  {
    mailBoxConfigurations = _mailBoxConfigurations;
  }

  /**
   * Current Mail Box Configuration
   */
  private transient ch.soreco.orderbook.bo.MailBoxConfiguration currentMailBoxConfiguration;

  /**
   * Gets the field currentMailBoxConfiguration.
   * @return the value of the field currentMailBoxConfiguration; may be null.
   */
  public ch.soreco.orderbook.bo.MailBoxConfiguration getCurrentMailBoxConfiguration()
  {
    return currentMailBoxConfiguration;
  }

  /**
   * Sets the field currentMailBoxConfiguration.
   * @param _currentMailBoxConfiguration the new value of the field currentMailBoxConfiguration.
   */
  public void setCurrentMailBoxConfiguration(ch.soreco.orderbook.bo.MailBoxConfiguration _currentMailBoxConfiguration)
  {
    currentMailBoxConfiguration = _currentMailBoxConfiguration;
  }

  /**
   * Contains information about thrown exceptions
   */
  private transient ch.soreco.utilities.exception.ExceptionInformationWrapperException exceptionInformation;

  /**
   * Gets the field exceptionInformation.
   * @return the value of the field exceptionInformation; may be null.
   */
  public ch.soreco.utilities.exception.ExceptionInformationWrapperException getExceptionInformation()
  {
    return exceptionInformation;
  }

  /**
   * Sets the field exceptionInformation.
   * @param _exceptionInformation the new value of the field exceptionInformation.
   */
  public void setExceptionInformation(ch.soreco.utilities.exception.ExceptionInformationWrapperException _exceptionInformation)
  {
    exceptionInformation = _exceptionInformation;
  }

}
