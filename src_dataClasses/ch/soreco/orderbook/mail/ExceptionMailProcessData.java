package ch.soreco.orderbook.mail;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ExceptionMailProcessData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ExceptionMailProcessData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 3525371601349276885L;

  /**
   * Exception Information of Exception Handling
   */
  private transient ch.soreco.utilities.exception.ExceptionInformationWrapperException exceptionInformation;

  /**
   * Gets the field exceptionInformation.
   * @return the value of the field exceptionInformation; may be null.
   */
  public ch.soreco.utilities.exception.ExceptionInformationWrapperException getExceptionInformation()
  {
    return exceptionInformation;
  }

  /**
   * Sets the field exceptionInformation.
   * @param _exceptionInformation the new value of the field exceptionInformation.
   */
  public void setExceptionInformation(ch.soreco.utilities.exception.ExceptionInformationWrapperException _exceptionInformation)
  {
    exceptionInformation = _exceptionInformation;
  }

  /**
   * Mail Recipient
   */
  private transient java.lang.String mailTo;

  /**
   * Gets the field mailTo.
   * @return the value of the field mailTo; may be null.
   */
  public java.lang.String getMailTo()
  {
    return mailTo;
  }

  /**
   * Sets the field mailTo.
   * @param _mailTo the new value of the field mailTo.
   */
  public void setMailTo(java.lang.String _mailTo)
  {
    mailTo = _mailTo;
  }

  /**
   * Mail From
   */
  private transient java.lang.String mailFrom;

  /**
   * Gets the field mailFrom.
   * @return the value of the field mailFrom; may be null.
   */
  public java.lang.String getMailFrom()
  {
    return mailFrom;
  }

  /**
   * Sets the field mailFrom.
   * @param _mailFrom the new value of the field mailFrom.
   */
  public void setMailFrom(java.lang.String _mailFrom)
  {
    mailFrom = _mailFrom;
  }

  /**
   * Mail Subject
   */
  private transient java.lang.String mailSubject;

  /**
   * Gets the field mailSubject.
   * @return the value of the field mailSubject; may be null.
   */
  public java.lang.String getMailSubject()
  {
    return mailSubject;
  }

  /**
   * Sets the field mailSubject.
   * @param _mailSubject the new value of the field mailSubject.
   */
  public void setMailSubject(java.lang.String _mailSubject)
  {
    mailSubject = _mailSubject;
  }

  /**
   * Mail Subject Prefix
   */
  private transient java.lang.String mailSubjectPrefix;

  /**
   * Gets the field mailSubjectPrefix.
   * @return the value of the field mailSubjectPrefix; may be null.
   */
  public java.lang.String getMailSubjectPrefix()
  {
    return mailSubjectPrefix;
  }

  /**
   * Sets the field mailSubjectPrefix.
   * @param _mailSubjectPrefix the new value of the field mailSubjectPrefix.
   */
  public void setMailSubjectPrefix(java.lang.String _mailSubjectPrefix)
  {
    mailSubjectPrefix = _mailSubjectPrefix;
  }

  /**
   * Mail Body
   */
  private transient java.lang.String mailBody;

  /**
   * Gets the field mailBody.
   * @return the value of the field mailBody; may be null.
   */
  public java.lang.String getMailBody()
  {
    return mailBody;
  }

  /**
   * Sets the field mailBody.
   * @param _mailBody the new value of the field mailBody.
   */
  public void setMailBody(java.lang.String _mailBody)
  {
    mailBody = _mailBody;
  }

  /**
   * Exception Information to be sent
   */
  private transient ch.soreco.utilities.exception.ExceptionInformationWrapperException mailExceptionInformation;

  /**
   * Gets the field mailExceptionInformation.
   * @return the value of the field mailExceptionInformation; may be null.
   */
  public ch.soreco.utilities.exception.ExceptionInformationWrapperException getMailExceptionInformation()
  {
    return mailExceptionInformation;
  }

  /**
   * Sets the field mailExceptionInformation.
   * @param _mailExceptionInformation the new value of the field mailExceptionInformation.
   */
  public void setMailExceptionInformation(ch.soreco.utilities.exception.ExceptionInformationWrapperException _mailExceptionInformation)
  {
    mailExceptionInformation = _mailExceptionInformation;
  }

}
