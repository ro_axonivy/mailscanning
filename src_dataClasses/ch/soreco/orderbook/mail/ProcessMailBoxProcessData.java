package ch.soreco.orderbook.mail;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ProcessMailBoxProcessData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ProcessMailBoxProcessData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -2859478503692205721L;

  /**
   * Contains information about thrown exceptions
   */
  private transient ch.soreco.utilities.exception.ExceptionInformationWrapperException exceptionInformation;

  /**
   * Gets the field exceptionInformation.
   * @return the value of the field exceptionInformation; may be null.
   */
  public ch.soreco.utilities.exception.ExceptionInformationWrapperException getExceptionInformation()
  {
    return exceptionInformation;
  }

  /**
   * Sets the field exceptionInformation.
   * @param _exceptionInformation the new value of the field exceptionInformation.
   */
  public void setExceptionInformation(ch.soreco.utilities.exception.ExceptionInformationWrapperException _exceptionInformation)
  {
    exceptionInformation = _exceptionInformation;
  }

  /**
   * Defines if mail box should be scanned
   */
  private transient java.lang.Boolean isMailScanToBeStarted;

  /**
   * Gets the field isMailScanToBeStarted.
   * @return the value of the field isMailScanToBeStarted; may be null.
   */
  public java.lang.Boolean getIsMailScanToBeStarted()
  {
    return isMailScanToBeStarted;
  }

  /**
   * Sets the field isMailScanToBeStarted.
   * @param _isMailScanToBeStarted the new value of the field isMailScanToBeStarted.
   */
  public void setIsMailScanToBeStarted(java.lang.Boolean _isMailScanToBeStarted)
  {
    isMailScanToBeStarted = _isMailScanToBeStarted;
  }

  /**
   * The Mail Box Configuration.
   */
  private transient ch.soreco.orderbook.bo.MailBoxConfiguration mailBoxConfiguration;

  /**
   * Gets the field mailBoxConfiguration.
   * @return the value of the field mailBoxConfiguration; may be null.
   */
  public ch.soreco.orderbook.bo.MailBoxConfiguration getMailBoxConfiguration()
  {
    return mailBoxConfiguration;
  }

  /**
   * Sets the field mailBoxConfiguration.
   * @param _mailBoxConfiguration the new value of the field mailBoxConfiguration.
   */
  public void setMailBoxConfiguration(ch.soreco.orderbook.bo.MailBoxConfiguration _mailBoxConfiguration)
  {
    mailBoxConfiguration = _mailBoxConfiguration;
  }

  /**
   * Count of unread mails within mail box
   */
  private transient java.lang.Integer countOfUnreadMails;

  /**
   * Gets the field countOfUnreadMails.
   * @return the value of the field countOfUnreadMails; may be null.
   */
  public java.lang.Integer getCountOfUnreadMails()
  {
    return countOfUnreadMails;
  }

  /**
   * Sets the field countOfUnreadMails.
   * @param _countOfUnreadMails the new value of the field countOfUnreadMails.
   */
  public void setCountOfUnreadMails(java.lang.Integer _countOfUnreadMails)
  {
    countOfUnreadMails = _countOfUnreadMails;
  }

  /**
   * Counter for loop over mails to process
   */
  private transient java.lang.Integer counter;

  /**
   * Gets the field counter.
   * @return the value of the field counter; may be null.
   */
  public java.lang.Integer getCounter()
  {
    return counter;
  }

  /**
   * Sets the field counter.
   * @param _counter the new value of the field counter.
   */
  public void setCounter(java.lang.Integer _counter)
  {
    counter = _counter;
  }

  /**
   * Defines the subject prefix of the exception Mail
   */
  private transient java.lang.String exceptionInformationMailSubjectPrefix;

  /**
   * Gets the field exceptionInformationMailSubjectPrefix.
   * @return the value of the field exceptionInformationMailSubjectPrefix; may be null.
   */
  public java.lang.String getExceptionInformationMailSubjectPrefix()
  {
    return exceptionInformationMailSubjectPrefix;
  }

  /**
   * Sets the field exceptionInformationMailSubjectPrefix.
   * @param _exceptionInformationMailSubjectPrefix the new value of the field exceptionInformationMailSubjectPrefix.
   */
  public void setExceptionInformationMailSubjectPrefix(java.lang.String _exceptionInformationMailSubjectPrefix)
  {
    exceptionInformationMailSubjectPrefix = _exceptionInformationMailSubjectPrefix;
  }

  /**
   * Defines if saving of Order succeded
   */
  private transient java.lang.Boolean successOfSaveOrder;

  /**
   * Gets the field successOfSaveOrder.
   * @return the value of the field successOfSaveOrder; may be null.
   */
  public java.lang.Boolean getSuccessOfSaveOrder()
  {
    return successOfSaveOrder;
  }

  /**
   * Sets the field successOfSaveOrder.
   * @param _successOfSaveOrder the new value of the field successOfSaveOrder.
   */
  public void setSuccessOfSaveOrder(java.lang.Boolean _successOfSaveOrder)
  {
    successOfSaveOrder = _successOfSaveOrder;
  }

  /**
   * Error message as returned by save Order
   */
  private transient java.lang.String saveOrderErrorMessage;

  /**
   * Gets the field saveOrderErrorMessage.
   * @return the value of the field saveOrderErrorMessage; may be null.
   */
  public java.lang.String getSaveOrderErrorMessage()
  {
    return saveOrderErrorMessage;
  }

  /**
   * Sets the field saveOrderErrorMessage.
   * @param _saveOrderErrorMessage the new value of the field saveOrderErrorMessage.
   */
  public void setSaveOrderErrorMessage(java.lang.String _saveOrderErrorMessage)
  {
    saveOrderErrorMessage = _saveOrderErrorMessage;
  }

  /**
   * Order as created from Mail
   */
  private transient ch.soreco.orderbook.bo.Order order;

  /**
   * Gets the field order.
   * @return the value of the field order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return order;
  }

  /**
   * Sets the field order.
   * @param _order the new value of the field order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _order)
  {
    order = _order;
  }

  /**
   * Mail as retrieved by MailScannerService
   */
  private transient ch.soreco.orderbook.mail.domain.Mail mail;

  /**
   * Gets the field mail.
   * @return the value of the field mail; may be null.
   */
  public ch.soreco.orderbook.mail.domain.Mail getMail()
  {
    return mail;
  }

  /**
   * Sets the field mail.
   * @param _mail the new value of the field mail.
   */
  public void setMail(ch.soreco.orderbook.mail.domain.Mail _mail)
  {
    mail = _mail;
  }

  /**
   * Defines the mail sender address
   */
  private transient java.lang.String exceptionInformationMailFrom;

  /**
   * Gets the field exceptionInformationMailFrom.
   * @return the value of the field exceptionInformationMailFrom; may be null.
   */
  public java.lang.String getExceptionInformationMailFrom()
  {
    return exceptionInformationMailFrom;
  }

  /**
   * Sets the field exceptionInformationMailFrom.
   * @param _exceptionInformationMailFrom the new value of the field exceptionInformationMailFrom.
   */
  public void setExceptionInformationMailFrom(java.lang.String _exceptionInformationMailFrom)
  {
    exceptionInformationMailFrom = _exceptionInformationMailFrom;
  }

  /**
   * Defines the mail recipient
   */
  private transient java.lang.String exceptionInformationMailTo;

  /**
   * Gets the field exceptionInformationMailTo.
   * @return the value of the field exceptionInformationMailTo; may be null.
   */
  public java.lang.String getExceptionInformationMailTo()
  {
    return exceptionInformationMailTo;
  }

  /**
   * Sets the field exceptionInformationMailTo.
   * @param _exceptionInformationMailTo the new value of the field exceptionInformationMailTo.
   */
  public void setExceptionInformationMailTo(java.lang.String _exceptionInformationMailTo)
  {
    exceptionInformationMailTo = _exceptionInformationMailTo;
  }

  /**
   * Defines the mapping of initial values for an Order
   */
  private transient ch.soreco.orderbook.bo.OrderMatching orderMatching;

  /**
   * Gets the field orderMatching.
   * @return the value of the field orderMatching; may be null.
   */
  public ch.soreco.orderbook.bo.OrderMatching getOrderMatching()
  {
    return orderMatching;
  }

  /**
   * Sets the field orderMatching.
   * @param _orderMatching the new value of the field orderMatching.
   */
  public void setOrderMatching(ch.soreco.orderbook.bo.OrderMatching _orderMatching)
  {
    orderMatching = _orderMatching;
  }

}
