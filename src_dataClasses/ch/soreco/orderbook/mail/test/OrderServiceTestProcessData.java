package ch.soreco.orderbook.mail.test;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderServiceTestProcessData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderServiceTestProcessData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -2881371879334402528L;

  /**
   * The Order to be created
   */
  private transient ch.soreco.orderbook.bo.Order order;

  /**
   * Gets the field order.
   * @return the value of the field order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return order;
  }

  /**
   * Sets the field order.
   * @param _order the new value of the field order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _order)
  {
    order = _order;
  }

  /**
   * The OrderMatching found by docType
   */
  private transient ch.soreco.orderbook.bo.OrderMatching orderMatching;

  /**
   * Gets the field orderMatching.
   * @return the value of the field orderMatching; may be null.
   */
  public ch.soreco.orderbook.bo.OrderMatching getOrderMatching()
  {
    return orderMatching;
  }

  /**
   * Sets the field orderMatching.
   * @param _orderMatching the new value of the field orderMatching.
   */
  public void setOrderMatching(ch.soreco.orderbook.bo.OrderMatching _orderMatching)
  {
    orderMatching = _orderMatching;
  }

  private transient ch.soreco.utilities.exception.ExceptionInformationWrapperException exceptionInformation;

  /**
   * Gets the field exceptionInformation.
   * @return the value of the field exceptionInformation; may be null.
   */
  public ch.soreco.utilities.exception.ExceptionInformationWrapperException getExceptionInformation()
  {
    return exceptionInformation;
  }

  /**
   * Sets the field exceptionInformation.
   * @param _exceptionInformation the new value of the field exceptionInformation.
   */
  public void setExceptionInformation(ch.soreco.utilities.exception.ExceptionInformationWrapperException _exceptionInformation)
  {
    exceptionInformation = _exceptionInformation;
  }

  /**
   * The docType by which the orderMatching is searched
   */
  private transient java.lang.String docType;

  /**
   * Gets the field docType.
   * @return the value of the field docType; may be null.
   */
  public java.lang.String getDocType()
  {
    return docType;
  }

  /**
   * Sets the field docType.
   * @param _docType the new value of the field docType.
   */
  public void setDocType(java.lang.String _docType)
  {
    docType = _docType;
  }

}
