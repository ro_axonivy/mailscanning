/**
 * Creates tbl_Attachment if the table is not existing
 * tbl_Attachment has a foreign key to tbl_Scanning ([scanningId])
 * and [tbl_Files] ([fileId])
 */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN TRANSACTION
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Attachment]') AND type in (N'U'))
BEGIN
    CREATE TABLE tbl_Attachment (
        attachmentId int PRIMARY KEY IDENTITY NOT NULL,
        scanningId int NOT NULL,
        fileId int  NOT NULL
    ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbl_Attachment_tbl_Scanning]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Attachment]'))
	ALTER TABLE [dbo].[tbl_Attachment] 
		WITH CHECK ADD CONSTRAINT [FK_tbl_Attachment_tbl_Scanning] 
		FOREIGN KEY([scanningId])
		REFERENCES [dbo].[tbl_Scanning] ([scanningId])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbl_Attachment_tbl_Scanning]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Attachment]'))
	ALTER TABLE [dbo].[tbl_Attachment] 
		CHECK CONSTRAINT [FK_tbl_Attachment_tbl_Scanning]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK2841286495021C7B]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Attachment]'))
	ALTER TABLE [dbo].[tbl_Attachment] 
		WITH CHECK ADD CONSTRAINT [FK2841286495021C7B] 
		FOREIGN KEY([fileId])
		REFERENCES [dbo].[tbl_Files] ([fileId])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK2841286495021C7B]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Attachment]'))
	ALTER TABLE [dbo].[tbl_Attachment] 
	CHECK CONSTRAINT [FK2841286495021C7B]
GO
COMMIT