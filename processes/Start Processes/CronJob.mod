[Ivy]
[>Created: Thu Aug 06 16:28:00 CEST 2015]
141EECAE76F7D62D 3.17 #module
>Proto >Proto Collection #zClass
Cb0 CronJob Big #zClass
Cb0 B #cInfo
Cb0 #process
Cb0 @TextInP .resExport .resExport #zField
Cb0 @TextInP .type .type #zField
Cb0 @TextInP .processKind .processKind #zField
Cb0 @AnnotationInP-0n ai ai #zField
Cb0 @TextInP .xml .xml #zField
Cb0 @TextInP .responsibility .responsibility #zField
Cb0 @StartEvent f0 '' #zField
Cb0 @EndTask f5 '' #zField
Cb0 @ProgramInterface f7 '' #zField
Cb0 @Alternative f9 '' #zField
Cb0 @PushWFArc f10 '' #zField
Cb0 @CallSub f13 '' #zField
Cb0 @GridStep f20 '' #zField
Cb0 @PushWFArc f18 '' #zField
Cb0 @ProcessException f23 '' #zField
Cb0 @ProcessException f25 '' #zField
Cb0 @Alternative f35 '' #zField
Cb0 @PushWFArc f36 '' #zField
Cb0 @PushWFArc f26 '' #zField
Cb0 @Alternative f37 '' #zField
Cb0 @PushWFArc f6 '' #zField
Cb0 @PushWFArc f39 '' #zField
Cb0 @GridStep f24 '' #zField
Cb0 @PushWFArc f17 '' #zField
Cb0 @PushWFArc f41 '' #zField
Cb0 @PushWFArc f11 '' #zField
Cb0 @PushWFArc f12 '' #zField
Cb0 @PushWFArc f1 '' #zField
>Proto Cb0 Cb0 CronJob #zField
Cb0 f0 outerBean "ch.ivyteam.ivy.process.eventstart.beans.AutoProcessStarterEventBean" #txt
Cb0 f0 beanConfig "60" #txt
Cb0 f0 outLink eventLink.ivp #txt
Cb0 f0 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>AutoProcessStart every 60s</name>
        <nameStyle>26,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f0 @C|.responsibility Everybody #txt
Cb0 f0 99 19 26 26 17 -5 #rect
Cb0 f0 @|StartEventIcon #fIcon
Cb0 f5 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f5 99 571 26 26 14 0 #rect
Cb0 f5 @|EndIcon #fIcon
Cb0 f7 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f7 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Cb0 f7 timeout 0 #txt
Cb0 f7 beanConfig '"#
#Tue Jul 28 23:36:10 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=in.mailBoxConfigurations \\= ivy.persistence.Adressmutation.findAll(ch.soreco.orderbook.bo.MailBoxConfiguration.class);\\nif(in.mailBoxConfigurations\\=\\=null||in.mailBoxConfigurations.size()\\=\\=0){\\n\\tivy.log.debug(""No MailBoxConfigurations found\\!"");\\n\\t}
"' #txt
Cb0 f7 exceptionHandler 141EECAE76F7D62D-f23-buffer #txt
Cb0 f7 timeoutExceptionHandler 141EECAE76F7D62D-f25-buffer #txt
Cb0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>load List of Mail Box Configurations</name>
        <nameStyle>36,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f7 94 132 36 24 22 -8 #rect
Cb0 f7 @|ProgramInterfaceIcon #fIcon
Cb0 f9 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>has More MailBoxConfigurations?</name>
        <nameStyle>31,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f9 98 218 28 28 12 -29 #rect
Cb0 f9 @|AlternativeIcon #fIcon
Cb0 f10 expr out #txt
Cb0 f10 112 156 112 218 #arcP
Cb0 f13 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f13 processCall 'Functional Processes/ProcessMailBoxProcess:call(ch.soreco.orderbook.bo.MailBoxConfiguration)' #txt
Cb0 f13 doCall true #txt
Cb0 f13 requestActionDecl '<ch.soreco.orderbook.bo.MailBoxConfiguration mailBoxConfiguration> param;
' #txt
Cb0 f13 requestMappingAction 'param.mailBoxConfiguration=in.currentMailBoxConfiguration;
' #txt
Cb0 f13 responseActionDecl 'ch.soreco.orderbook.mail.CronJobData out;
' #txt
Cb0 f13 responseMappingAction 'out=in;
' #txt
Cb0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>process MailBox</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f13 278 348 36 24 21 -8 #rect
Cb0 f13 @|CallSubIcon #fIcon
Cb0 f20 actionDecl 'ch.soreco.orderbook.mail.CronJobData out;
' #txt
Cb0 f20 actionTable 'out=in;
' #txt
Cb0 f20 actionCode in.counter=in.counter+1; #txt
Cb0 f20 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>increment counter</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f20 278 404 36 24 21 -7 #rect
Cb0 f20 @|StepIcon #fIcon
Cb0 f18 expr out #txt
Cb0 f18 296 428 120 238 #arcP
Cb0 f18 1 296 456 #addKink
Cb0 f18 2 168 456 #addKink
Cb0 f18 3 168 280 #addKink
Cb0 f18 2 0.4898707578792419 0 0 #arcLabel
Cb0 f23 .resExport export #txt
Cb0 f23 actionDecl 'ch.soreco.orderbook.mail.CronJobData out;
' #txt
Cb0 f23 actionTable 'out=in;
' #txt
Cb0 f23 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Cb0 f23 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in CronJob initialization</name>
        <nameStyle>35,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f23 739 443 26 26 18 -8 #rect
Cb0 f23 @|ExceptionIcon #fIcon
Cb0 f25 .resExport export #txt
Cb0 f25 actionDecl 'ch.soreco.orderbook.mail.CronJobData out;
' #txt
Cb0 f25 actionTable 'out=in;
' #txt
Cb0 f25 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Cb0 f25 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in CronJob initialization</name>
        <nameStyle>33,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f25 683 387 26 26 19 -7 #rect
Cb0 f25 @|ExceptionIcon #fIcon
Cb0 f35 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f35 682 506 28 28 14 0 #rect
Cb0 f35 @|AlternativeIcon #fIcon
Cb0 f35 -1|-1|-16777216 #nodeStyle
Cb0 f36 expr out #txt
Cb0 f36 696 413 696 506 #arcP
Cb0 f36 0 0.3495867476678249 0 0 #arcLabel
Cb0 f26 expr out #txt
Cb0 f26 752 469 707 517 #arcP
Cb0 f26 1 752 504 #addKink
Cb0 f26 1 0.19622984268097154 0 0 #arcLabel
Cb0 f37 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f37 98 506 28 28 14 0 #rect
Cb0 f37 @|AlternativeIcon #fIcon
Cb0 f37 -1|-1|-16777216 #nodeStyle
Cb0 f6 expr in #txt
Cb0 f6 112 534 112 571 #arcP
Cb0 f39 expr in #txt
Cb0 f39 682 520 126 520 #arcP
Cb0 f24 actionDecl 'ch.soreco.orderbook.mail.CronJobData out;
' #txt
Cb0 f24 actionTable 'out=in;
out.currentMailBoxConfiguration=in.mailBoxConfigurations.get(in.counter);
' #txt
Cb0 f24 type ch.soreco.orderbook.mail.CronJobData #txt
Cb0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get current MailBoxConfiguration</name>
        <nameStyle>32,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f24 278 284 36 24 20 -2 #rect
Cb0 f24 @|StepIcon #fIcon
Cb0 f17 expr out #txt
Cb0 f17 296 308 296 348 #arcP
Cb0 f41 expr in #txt
Cb0 f41 outCond in.counter<in.mailBoxConfigurations.size() #txt
Cb0 f41 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f41 6 #arcStyle
Cb0 f41 126 232 296 284 #arcP
Cb0 f41 1 296 232 #addKink
Cb0 f41 0 0.14150943396226415 0 10 #arcLabel
Cb0 f11 expr out #txt
Cb0 f11 296 372 296 404 #arcP
Cb0 f12 expr out #txt
Cb0 f12 112 45 112 132 #arcP
Cb0 f1 expr in #txt
Cb0 f1 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>no</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cb0 f1 3 #arcStyle
Cb0 f1 112 246 112 506 #arcP
Cb0 f1 0 0.074853198867368 11 0 #arcLabel
>Proto Cb0 .type ch.soreco.orderbook.mail.CronJobData #txt
>Proto Cb0 .processKind NORMAL #txt
>Proto Cb0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel></swimlaneLabel>
        <swimlaneLabel>Exception Handling</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
    </language>
    <swimlaneSize>592</swimlaneSize>
    <swimlaneSize>424</swimlaneSize>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-39322</swimlaneColor>
</elementInfo>
' #txt
>Proto Cb0 0 0 32 24 18 0 #rect
>Proto Cb0 @|BIcon #fIcon
Cb0 f7 mainOut f10 tail #connect
Cb0 f10 head f9 in #connect
Cb0 f20 mainOut f18 tail #connect
Cb0 f18 head f9 in #connect
Cb0 f25 mainOut f36 tail #connect
Cb0 f36 head f35 in #connect
Cb0 f23 mainOut f26 tail #connect
Cb0 f26 head f35 in #connect
Cb0 f37 out f6 tail #connect
Cb0 f6 head f5 mainIn #connect
Cb0 f35 out f39 tail #connect
Cb0 f39 head f37 in #connect
Cb0 f24 mainOut f17 tail #connect
Cb0 f17 head f13 mainIn #connect
Cb0 f9 out f41 tail #connect
Cb0 f41 head f24 mainIn #connect
Cb0 f13 mainOut f11 tail #connect
Cb0 f11 head f20 mainIn #connect
Cb0 f0 mainOut f12 tail #connect
Cb0 f12 head f7 mainIn #connect
Cb0 f9 out f1 tail #connect
Cb0 f1 head f37 in #connect
