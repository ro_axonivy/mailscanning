[Ivy]
[>Created: Fri Jul 24 09:33:20 CEST 2015]
141EFAD2BCE499D3 3.17 #module
>Proto >Proto Collection #zClass
Es0 ExceptionMailProcess Big #zClass
Es0 B #cInfo
Es0 #process
Es0 @TextInP .resExport .resExport #zField
Es0 @TextInP .type .type #zField
Es0 @TextInP .processKind .processKind #zField
Es0 @AnnotationInP-0n ai ai #zField
Es0 @TextInP .xml .xml #zField
Es0 @TextInP .responsibility .responsibility #zField
Es0 @StartSub f0 '' #zField
Es0 @EndSub f1 '' #zField
Es0 @EMail f3 '' #zField
Es0 @StartSub f2 '' #zField
Es0 @EndSub f4 '' #zField
Es0 @PushWFArc f7 '' #zField
Es0 @ProgramInterface f66 '' #zField
Es0 @ProcessException f9 '' #zField
Es0 @Alternative f37 '' #zField
Es0 @Alternative f10 '' #zField
Es0 @PushWFArc f5 '' #zField
Es0 @PushWFArc f12 '' #zField
Es0 @PushWFArc f13 '' #zField
Es0 @ProcessException f14 '' #zField
Es0 @PushWFArc f15 '' #zField
Es0 @CallSub f16 '' #zField
Es0 @PushWFArc f17 '' #zField
Es0 @PushWFArc f11 '' #zField
Es0 @ProcessException f19 '' #zField
Es0 @Alternative f20 '' #zField
Es0 @PushWFArc f25 '' #zField
Es0 @PushWFArc f6 '' #zField
Es0 @PushWFArc f21 '' #zField
Es0 @ProgramInterface f18 '' #zField
Es0 @PushWFArc f22 '' #zField
Es0 @PushWFArc f8 '' #zField
>Proto Es0 Es0 ExceptionMailProcess #zField
Es0 f0 inParamDecl '<ch.soreco.utilities.exception.ExceptionInformationWrapperException exceptionInformation,java.lang.String mailFrom,java.lang.String mailTo,java.lang.String mailSubjectPrefix> param;' #txt
Es0 f0 inParamTable 'out.mailExceptionInformation=param.exceptionInformation;
out.mailFrom=param.mailFrom;
out.mailSubjectPrefix=param.mailSubjectPrefix;
out.mailTo=param.mailTo;
' #txt
Es0 f0 outParamDecl '<> result;
' #txt
Es0 f0 actionDecl 'ch.soreco.orderbook.mail.ExceptionMailProcessData out;
' #txt
Es0 f0 callSignature call(ch.soreco.utilities.exception.ExceptionInformationWrapperException,String,String,String) #txt
Es0 f0 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(ExceptionInformationWrapperException,String,String,String)</name>
        <nameStyle>63,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Es0 f0 83 51 26 26 14 0 #rect
Es0 f0 @|StartSubIcon #fIcon
Es0 f1 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f1 83 403 26 26 14 0 #rect
Es0 f1 @|EndSubIcon #fIcon
Es0 f3 beanConfig '"{/emailSubject ""<%=in.mailSubject%>""/emailFrom ""<%=in.mailFrom%>""/emailReplyTo """"/emailTo ""<%=in.mailTo%>""/emailCC """"/emailBCC """"/exceptionMissingEmailAttachments ""false""/emailMessage ""<html>\\n\\t<head>\\n\\t\\t<style>\\n\\t\\t\\t#MailContent {\\n\\t\\t\\t\\t\\n\\t\\t\\t\\t}\\n\\t\\t\\t.ExceptionInformationTitle {\\n\\t\\t\\t\\t\\n\\t\\t\\t\\t}\\n\\t\\t\\t.ExceptionInformationMessage {\\n\\t\\t\\t\\t\\n\\t\\t\\t\\t}\\n\\t\\t\\t.ExceptionInformationCausingMessage {\\n\\t\\t\\t\\t\\n\\t\\t\\t\\t}\\n\\t\\t\\t.ExceptionInformationStackTrace {\\n\\t\\t\\t\\t\\n\\t\\t\\t\\t}\\t\\n\\t\\t</style>\\n\\t</head>\\n\\t<body>\\n\\t\\t<div id=\\""MailContent\\"">\\n<%=in.mailBody%>\\t\\t\\n\\t\\t</div>\\n\\t</body>\\n</html>""/emailAttachments * }"' #txt
Es0 f3 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f3 timeout 0 #txt
Es0 f3 exceptionHandler 141EFAD2BCE499D3-f19-buffer #txt
Es0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>send Mail</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Es0 f3 902 132 36 24 20 -2 #rect
Es0 f3 @|EMailIcon #fIcon
Es0 f2 inParamDecl '<java.lang.String mailFrom,java.lang.String mailBody,java.lang.String mailTo,java.lang.String mailSubject> param;' #txt
Es0 f2 inParamTable 'out.mailBody=param.mailBody;
out.mailFrom=param.mailFrom;
out.mailSubject=param.mailSubject;
out.mailTo=param.mailTo;
' #txt
Es0 f2 outParamDecl '<> result;
' #txt
Es0 f2 actionDecl 'ch.soreco.orderbook.mail.ExceptionMailProcessData out;
' #txt
Es0 f2 callSignature sendMail(String,String,String,String) #txt
Es0 f2 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>sendMail(String,String,String,String)</name>
        <nameStyle>37,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Es0 f2 907 51 26 26 14 0 #rect
Es0 f2 @|StartSubIcon #fIcon
Es0 f4 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f4 907 267 26 26 14 0 #rect
Es0 f4 @|EndSubIcon #fIcon
Es0 f7 expr out #txt
Es0 f7 920 77 920 132 #arcP
Es0 f66 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f66 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Es0 f66 timeout 0 #txt
Es0 f66 beanConfig '"#
#Fri Jul 24 09:31:08 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=StringBuilder html \\= new StringBuilder("""");\\nhtml.append(""<h3 class\\=''ExceptionInformationTitle''>Exception Type\\:</h3>"");\\ntry {\\n\\thtml.append(""<p class\\=''ExceptionInformationMessage''>""+in.mailExceptionInformation.getCausingThrowableType().getName()+""</p>"");\\n} catch (Exception e){\\n\\t//\\n}\\nhtml.append(""<h3 class\\=''ExceptionInformationTitle''>Causing Message\\:</h3>"");\\ntry {\\n\\thtml.append(""<p class\\=''ExceptionInformationCausingMessage''>""+in.mailExceptionInformation.getCausingMessage()+""</p>"");\\n} catch (Exception e){\\n\\t//\\n}\\nhtml.append(""<h3 class\\=''ExceptionInformationTitle''>Stack Trace\\:</h3>"");\\ntry {\\n\\thtml.append(""<p class\\=''ExceptionInformationStackTrace''>""+in.mailExceptionInformation.getStackTraceAsString()+""</p>"");\\n} catch (Exception e){\\n\\t//\\n}\\nin.mailBody \\= html.toString();
"' #txt
Es0 f66 exceptionHandler 141EFAD2BCE499D3-f9-buffer #txt
Es0 f66 timeoutExceptionHandler 141EFAD2BCE499D3-f14-buffer #txt
Es0 f66 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>collect mailExceptionInformation to Body</name>
        <nameStyle>40,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Es0 f66 78 164 36 24 22 -8 #rect
Es0 f66 @|ProgramInterfaceIcon #fIcon
Es0 f9 .resExport export #txt
Es0 f9 actionDecl 'ch.soreco.orderbook.mail.ExceptionMailProcessData out;
' #txt
Es0 f9 actionTable 'out=in;
' #txt
Es0 f9 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Es0 f9 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in collect mailExceptionInformation</name>
        <nameStyle>45,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Es0 f9 523 211 26 26 14 0 #rect
Es0 f9 @|ExceptionIcon #fIcon
Es0 f37 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f37 522 330 28 28 14 0 #rect
Es0 f37 @|AlternativeIcon #fIcon
Es0 f37 -1|-1|-16777216 #nodeStyle
Es0 f10 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f10 82 330 28 28 14 0 #rect
Es0 f10 @|AlternativeIcon #fIcon
Es0 f10 -1|-1|-16777216 #nodeStyle
Es0 f5 expr in #txt
Es0 f5 96 358 96 403 #arcP
Es0 f12 expr out #txt
Es0 f12 536 237 536 330 #arcP
Es0 f13 expr in #txt
Es0 f13 522 344 110 344 #arcP
Es0 f14 .resExport export #txt
Es0 f14 actionDecl 'ch.soreco.orderbook.mail.ExceptionMailProcessData out;
' #txt
Es0 f14 actionTable 'out=in;
' #txt
Es0 f14 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);' #txt
Es0 f14 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in collect mailExceptionInformation</name>
        <nameStyle>43,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Es0 f14 579 267 26 26 14 0 #rect
Es0 f14 @|ExceptionIcon #fIcon
Es0 f15 expr out #txt
Es0 f15 592 293 547 341 #arcP
Es0 f15 1 592 328 #addKink
Es0 f15 1 0.0983013505176163 0 0 #arcLabel
Es0 f16 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f16 processCall 'Functional Processes/ExceptionMailProcess:sendMail(String,String,String,String)' #txt
Es0 f16 doCall true #txt
Es0 f16 requestActionDecl '<java.lang.String mailFrom,java.lang.String mailBody,java.lang.String mailTo,java.lang.String mailSubject> param;
' #txt
Es0 f16 requestMappingAction 'param.mailFrom=in.mailFrom;
param.mailBody=in.mailBody;
param.mailTo=in.mailTo;
param.mailSubject=in.mailSubject;
' #txt
Es0 f16 responseActionDecl 'ch.soreco.orderbook.mail.ExceptionMailProcessData out;
' #txt
Es0 f16 responseMappingAction 'out=in;
' #txt
Es0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>send Mail</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Es0 f16 78 220 36 24 20 -2 #rect
Es0 f16 @|CallSubIcon #fIcon
Es0 f17 expr out #txt
Es0 f17 96 188 96 220 #arcP
Es0 f11 expr out #txt
Es0 f11 96 244 96 330 #arcP
Es0 f19 .resExport export #txt
Es0 f19 actionDecl 'ch.soreco.orderbook.mail.ExceptionMailProcessData out;
' #txt
Es0 f19 actionTable 'out=in;
' #txt
Es0 f19 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Es0 f19 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in send Mail</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Es0 f19 1187 123 26 26 14 0 #rect
Es0 f19 @|ExceptionIcon #fIcon
Es0 f20 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f20 906 194 28 28 14 0 #rect
Es0 f20 @|AlternativeIcon #fIcon
Es0 f20 -1|-1|-16777216 #nodeStyle
Es0 f25 expr out #txt
Es0 f25 920 156 920 194 #arcP
Es0 f6 expr in #txt
Es0 f6 920 222 920 267 #arcP
Es0 f21 expr out #txt
Es0 f21 1200 149 934 208 #arcP
Es0 f21 1 1200 208 #addKink
Es0 f21 1 0.23349103762344683 0 0 #arcLabel
Es0 f18 type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
Es0 f18 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Es0 f18 timeout 0 #txt
Es0 f18 beanConfig '"#
#Fri Jul 24 09:33:18 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=StringBuilder subject \\= new StringBuilder("""");\\nsubject.append(in.mailSubjectPrefix);\\nif(in.mailSubjectPrefix\\!\\=null&&\\!in.mailSubjectPrefix.endsWith(""\\:"")){\\n\\tsubject.append(""\\:"");\\n\\t}\\nString message \\= """";\\nif(in.mailExceptionInformation.getCausingThrowableType()\\!\\=null){\\n\\tmessage \\= in.mailExceptionInformation.getCausingThrowableType().getName();\\n\\t}\\nif(in.mailExceptionInformation.getCausingMessage()\\!\\=null){\\n\\tmessage \\= message+"" - ""+in.mailExceptionInformation.getCausingMessage();\\n\\tif(message.indexOf(""\\\\n"")>-1){\\n\\t\\tmessage \\= message.substring(0,message.indexOf(""\\\\n"")).trim();\\n\\t\\t}\\n\\t}\\nsubject.append(message);\\nin.mailSubject \\= subject.toString();
"' #txt
Es0 f18 exceptionHandler 141EFAD2BCE499D3-f9-buffer #txt
Es0 f18 timeoutExceptionHandler 141EFAD2BCE499D3-f14-buffer #txt
Es0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>collect mailExceptionInformation to Subject</name>
        <nameStyle>43,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Es0 f18 78 108 36 24 22 -8 #rect
Es0 f18 @|ProgramInterfaceIcon #fIcon
Es0 f22 expr out #txt
Es0 f22 96 77 96 108 #arcP
Es0 f8 expr out #txt
Es0 f8 96 132 96 164 #arcP
>Proto Es0 .type ch.soreco.orderbook.mail.ExceptionMailProcessData #txt
>Proto Es0 .processKind CALLABLE_SUB #txt
>Proto Es0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel></swimlaneLabel>
        <swimlaneLabel>Exception Handling</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
        <swimlaneLabel>Exception Handling</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
    </language>
    <swimlaneSize>480</swimlaneSize>
    <swimlaneSize>384</swimlaneSize>
    <swimlaneSize>288</swimlaneSize>
    <swimlaneSize>256</swimlaneSize>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-39322</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-39322</swimlaneColor>
</elementInfo>
' #txt
>Proto Es0 0 0 32 24 18 0 #rect
>Proto Es0 @|BIcon #fIcon
Es0 f2 mainOut f7 tail #connect
Es0 f7 head f3 mainIn #connect
Es0 f10 out f5 tail #connect
Es0 f5 head f1 mainIn #connect
Es0 f9 mainOut f12 tail #connect
Es0 f12 head f37 in #connect
Es0 f37 out f13 tail #connect
Es0 f13 head f10 in #connect
Es0 f14 mainOut f15 tail #connect
Es0 f15 head f37 in #connect
Es0 f66 mainOut f17 tail #connect
Es0 f17 head f16 mainIn #connect
Es0 f16 mainOut f11 tail #connect
Es0 f11 head f10 in #connect
Es0 f3 mainOut f25 tail #connect
Es0 f25 head f20 in #connect
Es0 f20 out f6 tail #connect
Es0 f6 head f4 mainIn #connect
Es0 f19 mainOut f21 tail #connect
Es0 f21 head f20 in #connect
Es0 f0 mainOut f22 tail #connect
Es0 f22 head f18 mainIn #connect
Es0 f18 mainOut f8 tail #connect
Es0 f8 head f66 mainIn #connect
