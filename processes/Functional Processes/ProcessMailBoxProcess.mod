[Ivy]
[>Created: Fri Nov 18 01:24:22 CET 2016]
141EEE4C24C8C88C 3.17 #module
>Proto >Proto Collection #zClass
Ps0 ProcessMailBoxProcess Big #zClass
Ps0 B #cInfo
Ps0 #process
Ps0 @TextInP .resExport .resExport #zField
Ps0 @TextInP .type .type #zField
Ps0 @TextInP .processKind .processKind #zField
Ps0 @AnnotationInP-0n ai ai #zField
Ps0 @TextInP .xml .xml #zField
Ps0 @TextInP .responsibility .responsibility #zField
Ps0 @StartSub f0 '' #zField
Ps0 @EndSub f1 '' #zField
Ps0 @ProcessException f25 '' #zField
Ps0 @ProcessException f23 '' #zField
Ps0 @Alternative f35 '' #zField
Ps0 @PushWFArc f36 '' #zField
Ps0 @PushWFArc f26 '' #zField
Ps0 @Alternative f4 '' #zField
Ps0 @ProgramInterface f12 '' #zField
Ps0 @ProgramInterface f14 '' #zField
Ps0 @PushWFArc f15 '' #zField
Ps0 @Alternative f16 '' #zField
Ps0 @PushWFArc f17 '' #zField
Ps0 @ProgramInterface f18 '' #zField
Ps0 @PushWFArc f19 '' #zField
Ps0 @GridStep f20 '' #zField
Ps0 @ProgramInterface f22 '' #zField
Ps0 @PushWFArc f24 '' #zField
Ps0 @ProcessException f28 '' #zField
Ps0 @Alternative f29 '' #zField
Ps0 @ProcessException f30 '' #zField
Ps0 @ProcessException f34 '' #zField
Ps0 @Alternative f37 '' #zField
Ps0 @ProcessException f38 '' #zField
Ps0 @PushWFArc f42 '' #zField
Ps0 @PushWFArc f43 '' #zField
Ps0 @Alternative f44 '' #zField
Ps0 @Alternative f49 '' #zField
Ps0 @PushWFArc f50 '' #zField
Ps0 @Alternative f52 '' #zField
Ps0 @PushWFArc f21 '' #zField
Ps0 @ProgramInterface f51 '' #zField
Ps0 @PushWFArc f55 '' #zField
Ps0 @ProgramInterface f66 '' #zField
Ps0 @PushWFArc f53 '' #zField
Ps0 @InfoButton f68 '' #zField
Ps0 @Alternative f69 '' #zField
Ps0 @PushWFArc f73 '' #zField
Ps0 @PushWFArc f8 '' #zField
Ps0 @Alternative f76 '' #zField
Ps0 @ProcessException f77 '' #zField
Ps0 @ProcessException f78 '' #zField
Ps0 @PushWFArc f79 '' #zField
Ps0 @PushWFArc f80 '' #zField
Ps0 @Alternative f9 '' #zField
Ps0 @PushWFArc f75 '' #zField
Ps0 @Alternative f84 '' #zField
Ps0 @PushWFArc f81 '' #zField
Ps0 @PushWFArc f86 '' #zField
Ps0 @Alternative f87 '' #zField
Ps0 @PushWFArc f88 '' #zField
Ps0 @PushWFArc f32 '' #zField
Ps0 @PushWFArc f33 '' #zField
Ps0 @CallSub f89 '' #zField
Ps0 @PushWFArc f47 '' #zField
Ps0 @ProcessException f31 '' #zField
Ps0 @ProcessException f41 '' #zField
Ps0 @Alternative f90 '' #zField
Ps0 @PushWFArc f92 '' #zField
Ps0 @PushWFArc f93 '' #zField
Ps0 @PushWFArc f94 '' #zField
Ps0 @CallSub f95 '' #zField
Ps0 @PushWFArc f11 '' #zField
Ps0 @ProgramInterface f2 '' #zField
Ps0 @PushWFArc f3 '' #zField
Ps0 @PushWFArc f5 '' #zField
Ps0 @PushWFArc f7 '' #zField
Ps0 @ProgramInterface f56 '' #zField
Ps0 @PushWFArc f57 '' #zField
Ps0 @PushWFArc f13 '' #zField
Ps0 @ProgramInterface f65 '' #zField
Ps0 @PushWFArc f70 '' #zField
Ps0 @PushWFArc f71 '' #zField
Ps0 @PushWFArc f45 '' #zField
Ps0 @PushWFArc f46 '' #zField
Ps0 @ProgramInterface f39 '' #zField
Ps0 @PushWFArc f40 '' #zField
Ps0 @PushWFArc f54 '' #zField
Ps0 @ProgramInterface f48 '' #zField
Ps0 @PushWFArc f58 '' #zField
Ps0 @ProgramInterface f59 '' #zField
Ps0 @PushWFArc f60 '' #zField
Ps0 @PushWFArc f27 '' #zField
Ps0 @PushWFArc f6 '' #zField
Ps0 @ProgramInterface f10 '' #zField
Ps0 @PushWFArc f61 '' #zField
Ps0 @ProgramInterface f63 '' #zField
Ps0 @PushWFArc f64 '' #zField
Ps0 @PushWFArc f62 '' #zField
>Proto Ps0 Ps0 ProcessMailBoxProcess #zField
Ps0 f0 inParamDecl '<ch.soreco.orderbook.bo.MailBoxConfiguration mailBoxConfiguration> param;' #txt
Ps0 f0 inParamTable 'out.mailBoxConfiguration=param.mailBoxConfiguration;
' #txt
Ps0 f0 outParamDecl '<> result;
' #txt
Ps0 f0 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f0 callSignature call(ch.soreco.orderbook.bo.MailBoxConfiguration) #txt
Ps0 f0 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(MailBoxConfiguration)</name>
    </language>
</elementInfo>
' #txt
Ps0 f0 195 27 26 26 14 0 #rect
Ps0 f0 @|StartSubIcon #fIcon
Ps0 f1 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f1 195 1603 26 26 14 0 #rect
Ps0 f1 @|EndSubIcon #fIcon
Ps0 f25 .resExport export #txt
Ps0 f25 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f25 actionTable 'out=in;
' #txt
Ps0 f25 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Ps0 f25 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in MailScanner Initialization</name>
        <nameStyle>37,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f25 987 1187 26 26 19 -7 #rect
Ps0 f25 @|ExceptionIcon #fIcon
Ps0 f23 .resExport export #txt
Ps0 f23 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f23 actionTable 'out=in;
' #txt
Ps0 f23 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Ps0 f23 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in MailScanner Initialization</name>
        <nameStyle>39,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f23 915 1147 26 26 18 -8 #rect
Ps0 f23 @|ExceptionIcon #fIcon
Ps0 f35 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f35 914 1250 28 28 14 0 #rect
Ps0 f35 @|AlternativeIcon #fIcon
Ps0 f35 -1|-1|-16777216 #nodeStyle
Ps0 f36 expr out #txt
Ps0 f36 1000 1213 939 1261 #arcP
Ps0 f36 1 1000 1248 #addKink
Ps0 f36 0 0.9059616266775412 0 0 #arcLabel
Ps0 f26 expr out #txt
Ps0 f26 928 1173 928 1250 #arcP
Ps0 f26 0 0.7660026292901275 0 0 #arcLabel
Ps0 f4 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>is Mail Scan to be Started?</name>
        <nameStyle>27,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f4 194 186 28 28 13 -24 #rect
Ps0 f4 @|AlternativeIcon #fIcon
Ps0 f12 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f12 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f12 timeout 0 #txt
Ps0 f12 beanConfig '"#
#Thu Aug 06 16:08:00 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.MailScannerService;\\n\\nString username \\= in.mailBoxConfiguration.getUsername();\\nString password \\= in.mailBoxConfiguration.getPassword();\\nInteger port \\= in.mailBoxConfiguration.getPort();\\nString serverAddress \\= in.mailBoxConfiguration.getHost();\\nMailScannerService.getInstance().connect(serverAddress, in.mailBoxConfiguration.getPort(), username, password);\\nivy.log.debug(String.format(""Connected to %s@%s\\:%d"", username, serverAddress, port));
"' #txt
Ps0 f12 exceptionHandler 141EEE4C24C8C88C-f23-buffer #txt
Ps0 f12 timeoutExceptionHandler 141EEE4C24C8C88C-f25-buffer #txt
Ps0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Connect to MailScannerService</name>
        <nameStyle>29,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f12 382 340 36 24 22 -8 #rect
Ps0 f12 @|ProgramInterfaceIcon #fIcon
Ps0 f14 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f14 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f14 timeout 0 #txt
Ps0 f14 beanConfig '"#
#Thu Nov 07 14:02:01 CET 2013
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.MailScannerService;\\n\\nin.countOfUnreadMails \\= MailScannerService.getInstance().countMailsInInbox();
"' #txt
Ps0 f14 exceptionHandler 141EEE4C24C8C88C-f23-buffer #txt
Ps0 f14 timeoutExceptionHandler 141EEE4C24C8C88C-f25-buffer #txt
Ps0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>count Unread Mails</name>
        <nameStyle>18,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f14 382 436 36 24 22 -8 #rect
Ps0 f14 @|ProgramInterfaceIcon #fIcon
Ps0 f15 expr out #txt
Ps0 f15 400 364 400 436 #arcP
Ps0 f16 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>has more mails to process?</name>
        <nameStyle>26,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f16 386 546 28 28 15 -22 #rect
Ps0 f16 @|AlternativeIcon #fIcon
Ps0 f17 expr out #txt
Ps0 f17 400 460 400 546 #arcP
Ps0 f18 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f18 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f18 timeout 0 #txt
Ps0 f18 beanConfig '"#
#Wed Jul 29 10:26:59 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.MailScannerService;\\nimport ch.soreco.orderbook.mail.domain.Mail;\\n\\nin.mail \\= MailScannerService.getInstance().retrieveNextMail();
"' #txt
Ps0 f18 exceptionHandler 141EEE4C24C8C88C-f30-buffer #txt
Ps0 f18 timeoutExceptionHandler 141EEE4C24C8C88C-f28-buffer #txt
Ps0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>retrieve next Mail</name>
        <nameStyle>18,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f18 654 596 36 24 22 -8 #rect
Ps0 f18 @|ProgramInterfaceIcon #fIcon
Ps0 f19 expr in #txt
Ps0 f19 outCond in.counter<in.countOfUnreadMails #txt
Ps0 f19 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f19 6 #arcStyle
Ps0 f19 414 560 672 596 #arcP
Ps0 f19 1 672 560 #addKink
Ps0 f19 0 0.18233624693410896 0 8 #arcLabel
Ps0 f20 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f20 actionTable 'out=in;
' #txt
Ps0 f20 actionCode in.counter=in.counter+1; #txt
Ps0 f20 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>increment counter</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f20 654 1092 36 24 21 -7 #rect
Ps0 f20 @|StepIcon #fIcon
Ps0 f22 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f22 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f22 timeout 0 #txt
Ps0 f22 beanConfig '"#
#Fri Nov 18 01:24:04 CET 2016
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.MailScannerService;\\n\\nMailScannerService.getInstance().disconnect();
"' #txt
Ps0 f22 exceptionHandler 141EEE4C24C8C88C-f23-buffer #txt
Ps0 f22 timeoutExceptionHandler 141EEE4C24C8C88C-f25-buffer #txt
Ps0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Disconnect from MailScannerService</name>
        <nameStyle>34,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f22 382 1412 36 24 24 -9 #rect
Ps0 f22 @|ProgramInterfaceIcon #fIcon
Ps0 f24 expr out #txt
Ps0 f24 672 1116 408 566 #arcP
Ps0 f24 1 672 1136 #addKink
Ps0 f24 2 472 1136 #addKink
Ps0 f24 3 472 616 #addKink
Ps0 f24 2 0.28817385894640773 0 0 #arcLabel
Ps0 f28 .resExport export #txt
Ps0 f28 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f28 actionTable 'out=in;
' #txt
Ps0 f28 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Ps0 f28 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in mail processing</name>
        <nameStyle>26,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f28 1547 579 26 26 19 -7 #rect
Ps0 f28 @|ExceptionIcon #fIcon
Ps0 f29 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f29 1466 722 28 28 14 0 #rect
Ps0 f29 @|AlternativeIcon #fIcon
Ps0 f29 -1|-1|-16777216 #nodeStyle
Ps0 f30 .resExport export #txt
Ps0 f30 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f30 actionTable 'out=in;
' #txt
Ps0 f30 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Ps0 f30 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in mail processing</name>
        <nameStyle>28,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f30 1467 539 26 26 18 -8 #rect
Ps0 f30 @|ExceptionIcon #fIcon
Ps0 f34 .resExport export #txt
Ps0 f34 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f34 actionTable 'out=in;
' #txt
Ps0 f34 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Ps0 f34 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f34 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in Exception Handling of mail process</name>
        <nameStyle>45,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f34 1747 947 26 26 19 -7 #rect
Ps0 f34 @|ExceptionIcon #fIcon
Ps0 f37 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f37 1674 1026 28 28 14 0 #rect
Ps0 f37 @|AlternativeIcon #fIcon
Ps0 f37 -1|-1|-16777216 #nodeStyle
Ps0 f38 .resExport export #txt
Ps0 f38 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f38 actionTable 'out=in;
' #txt
Ps0 f38 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Ps0 f38 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in Exception Handling of mail process</name>
        <nameStyle>47,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f38 1675 907 26 26 18 -8 #rect
Ps0 f38 @|ExceptionIcon #fIcon
Ps0 f42 expr out #txt
Ps0 f42 1760 973 1696 1034 #arcP
Ps0 f42 1 1760 992 #addKink
Ps0 f42 0 0.9059616266775412 0 0 #arcLabel
Ps0 f43 expr out #txt
Ps0 f43 1688 933 1688 1026 #arcP
Ps0 f43 0 0.7660026292901275 0 0 #arcLabel
Ps0 f44 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f44 914 1450 28 28 14 0 #rect
Ps0 f44 @|AlternativeIcon #fIcon
Ps0 f44 -1|-1|-16777216 #nodeStyle
Ps0 f49 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f49 1466 1026 28 28 14 0 #rect
Ps0 f49 @|AlternativeIcon #fIcon
Ps0 f49 -1|-1|-16777216 #nodeStyle
Ps0 f50 expr in #txt
Ps0 f50 1674 1040 1494 1040 #arcP
Ps0 f50 0 0.033267385831598544 0 0 #arcLabel
Ps0 f52 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f52 658 1026 28 28 14 0 #rect
Ps0 f52 @|AlternativeIcon #fIcon
Ps0 f52 -1|-1|-16777216 #nodeStyle
Ps0 f21 expr in #txt
Ps0 f21 672 1054 672 1092 #arcP
Ps0 f51 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f51 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f51 timeout 0 #txt
Ps0 f51 beanConfig '"#
#Thu Mar 24 00:09:02 CET 2016
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.OrderAdapter;\\nin.order \\= OrderAdapter.toOrder(in.mail, in.mailBoxConfiguration.useFirstPdfAsOrder, in.mailBoxConfiguration.useSubjectForRefNo);
"' #txt
Ps0 f51 exceptionHandler 141EEE4C24C8C88C-f30-buffer #txt
Ps0 f51 timeoutExceptionHandler 141EEE4C24C8C88C-f28-buffer #txt
Ps0 f51 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>convert Mail to new Order</name>
        <nameStyle>25,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f51 654 652 36 24 22 -8 #rect
Ps0 f51 @|ProgramInterfaceIcon #fIcon
Ps0 f55 expr out #txt
Ps0 f55 672 620 672 652 #arcP
Ps0 f66 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f66 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f66 timeout 0 #txt
Ps0 f66 beanConfig '"#
#Tue Aug 04 08:56:47 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.MailScannerService;\\nimport ch.soreco.orderbook.mail.domain.Mail;\\n\\nif(in.mail.getUid()\\!\\=null){\\n\\tMailScannerService.getInstance().markMailAsProcessed(in.mail.getUid(),in.order.getOrderId());\\n\\t}\\n
"' #txt
Ps0 f66 exceptionHandler 141EEE4C24C8C88C-f30-buffer #txt
Ps0 f66 timeoutExceptionHandler 141EEE4C24C8C88C-f28-buffer #txt
Ps0 f66 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>move to Processed folder</name>
        <nameStyle>24,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f66 654 780 36 24 22 -8 #rect
Ps0 f66 @|ProgramInterfaceIcon #fIcon
Ps0 f53 expr out #txt
Ps0 f53 672 804 672 1026 #arcP
Ps0 f68 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>retrieveNextMail should consider only Mails 
which are not &quot;touched&quot; by current thread</name>
        <nameStyle>86,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f68 740 587 248 42 -119 -16 #rect
Ps0 f68 @|IBIcon #fIcon
Ps0 f68 -1|-1|-16777216 #nodeStyle
Ps0 f69 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f69 194 1530 28 28 14 0 #rect
Ps0 f69 @|AlternativeIcon #fIcon
Ps0 f69 -1|-1|-16777216 #nodeStyle
Ps0 f73 expr in #txt
Ps0 f73 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>no</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f73 3 #arcStyle
Ps0 f73 208 214 208 1530 #arcP
Ps0 f73 0 0.01276595744680851 -12 0 #arcLabel
Ps0 f8 expr in #txt
Ps0 f8 208 1558 208 1603 #arcP
Ps0 f76 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f76 1234 434 28 28 14 0 #rect
Ps0 f76 @|AlternativeIcon #fIcon
Ps0 f76 -1|-1|-16777216 #nodeStyle
Ps0 f77 .resExport export #txt
Ps0 f77 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f77 actionTable 'out=in;
' #txt
Ps0 f77 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Ps0 f77 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f77 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in Mark Mail as Read</name>
        <nameStyle>30,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f77 1235 331 26 26 18 -8 #rect
Ps0 f77 @|ExceptionIcon #fIcon
Ps0 f78 .resExport export #txt
Ps0 f78 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f78 actionTable 'out=in;
' #txt
Ps0 f78 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Ps0 f78 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f78 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in Mark Mail as Read</name>
        <nameStyle>28,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f78 1307 371 26 26 19 -7 #rect
Ps0 f78 @|ExceptionIcon #fIcon
Ps0 f79 expr out #txt
Ps0 f79 1320 397 1259 445 #arcP
Ps0 f79 1 1320 427 #addKink
Ps0 f79 0 0.9059616266775412 0 0 #arcLabel
Ps0 f80 expr out #txt
Ps0 f80 1248 357 1248 434 #arcP
Ps0 f80 0 0.7660026292901274 0 0 #arcLabel
Ps0 f9 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>has Order an OrderId?</name>
        <nameStyle>21,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f9 1234 498 28 28 14 -26 #rect
Ps0 f9 @|AlternativeIcon #fIcon
Ps0 f75 expr in #txt
Ps0 f75 1248 462 1248 498 #arcP
Ps0 f75 0 0.8924039648661609 0 0 #arcLabel
Ps0 f84 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f84 1234 610 28 28 14 0 #rect
Ps0 f84 @|AlternativeIcon #fIcon
Ps0 f84 -1|-1|-16777216 #nodeStyle
Ps0 f81 expr in #txt
Ps0 f81 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>no</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f81 3 #arcStyle
Ps0 f81 1248 526 1248 610 #arcP
Ps0 f81 0 0.07258064516129033 -14 0 #arcLabel
Ps0 f86 expr in #txt
Ps0 f86 1248 638 1466 736 #arcP
Ps0 f86 1 1248 736 #addKink
Ps0 f86 1 0.341934675282026 0 0 #arcLabel
Ps0 f87 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f87 1466 634 28 28 14 0 #rect
Ps0 f87 @|AlternativeIcon #fIcon
Ps0 f87 -1|-1|-16777216 #nodeStyle
Ps0 f88 expr out #txt
Ps0 f88 1560 605 1492 646 #arcP
Ps0 f88 1 1560 632 #addKink
Ps0 f88 1 0.0709008959696214 0 0 #arcLabel
Ps0 f32 expr out #txt
Ps0 f32 1480 565 1480 634 #arcP
Ps0 f32 0 0.7660026292901274 0 0 #arcLabel
Ps0 f33 expr in #txt
Ps0 f33 1480 662 1480 722 #arcP
Ps0 f89 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f89 processCall 'Functional Processes/ExceptionMailProcess:call(ch.soreco.utilities.exception.ExceptionInformationWrapperException,String,String,String)' #txt
Ps0 f89 doCall true #txt
Ps0 f89 requestActionDecl '<ch.soreco.utilities.exception.ExceptionInformationWrapperException exceptionInformation,java.lang.String mailFrom,java.lang.String mailTo,java.lang.String mailSubjectPrefix> param;
' #txt
Ps0 f89 requestMappingAction 'param.exceptionInformation=in.exceptionInformation;
param.mailFrom=in.exceptionInformationMailFrom;
param.mailTo=in.exceptionInformationMailTo;
param.mailSubjectPrefix=in.exceptionInformationMailSubjectPrefix;
' #txt
Ps0 f89 responseActionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f89 responseMappingAction 'out=in;
' #txt
Ps0 f89 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Mail Exception to MailBox Administrator Email Address</name>
        <nameStyle>53,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f89 1462 844 36 24 20 -2 #rect
Ps0 f89 @|CallSubIcon #fIcon
Ps0 f47 expr out #txt
Ps0 f47 1480 868 1480 1026 #arcP
Ps0 f31 .resExport export #txt
Ps0 f31 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f31 actionTable 'out=in;
' #txt
Ps0 f31 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Ps0 f31 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in Exception Handling of MailScanner Initialization</name>
        <nameStyle>61,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f31 1283 1331 26 26 18 -8 #rect
Ps0 f31 @|ExceptionIcon #fIcon
Ps0 f41 .resExport export #txt
Ps0 f41 actionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f41 actionTable 'out=in;
' #txt
Ps0 f41 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Ps0 f41 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f41 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in Exception Handling of MailScanner Initialization</name>
        <nameStyle>59,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f41 1355 1371 26 26 19 -7 #rect
Ps0 f41 @|ExceptionIcon #fIcon
Ps0 f90 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f90 1282 1450 28 28 14 0 #rect
Ps0 f90 @|AlternativeIcon #fIcon
Ps0 f90 -1|-1|-16777216 #nodeStyle
Ps0 f92 expr out #txt
Ps0 f92 1368 1397 1304 1458 #arcP
Ps0 f92 1 1368 1416 #addKink
Ps0 f92 0 0.9059616266775412 0 0 #arcLabel
Ps0 f93 expr out #txt
Ps0 f93 1296 1357 1296 1450 #arcP
Ps0 f93 0 0.7660026292901275 0 0 #arcLabel
Ps0 f94 expr in #txt
Ps0 f94 1282 1464 942 1464 #arcP
Ps0 f95 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f95 processCall 'Functional Processes/ExceptionMailProcess:call(ch.soreco.utilities.exception.ExceptionInformationWrapperException,String,String,String)' #txt
Ps0 f95 doCall true #txt
Ps0 f95 requestActionDecl '<ch.soreco.utilities.exception.ExceptionInformationWrapperException exceptionInformation,java.lang.String mailFrom,java.lang.String mailTo,java.lang.String mailSubjectPrefix> param;
' #txt
Ps0 f95 requestMappingAction 'param.exceptionInformation=in.exceptionInformation;
param.mailFrom=in.exceptionInformationMailFrom;
param.mailTo=in.exceptionInformationMailTo;
param.mailSubjectPrefix=in.exceptionInformationMailSubjectPrefix;
' #txt
Ps0 f95 responseActionDecl 'ch.soreco.orderbook.mail.ProcessMailBoxProcessData out;
' #txt
Ps0 f95 responseMappingAction 'out=in;
' #txt
Ps0 f95 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Mail Exception to MailBox Administrator Email Address</name>
        <nameStyle>53,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f95 910 1380 36 24 20 -2 #rect
Ps0 f95 @|CallSubIcon #fIcon
Ps0 f11 expr out #txt
Ps0 f11 928 1404 928 1450 #arcP
Ps0 f2 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f2 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f2 timeout 0 #txt
Ps0 f2 beanConfig '"#
#Tue Jul 28 22:57:17 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.persistence.OrderService;\\nOrderService orderService \\= new OrderService();\\norderService.removeById(in.order.getOrderId());
"' #txt
Ps0 f2 exceptionHandler 141EEE4C24C8C88C-f30-buffer #txt
Ps0 f2 timeoutExceptionHandler 141EEE4C24C8C88C-f28-buffer #txt
Ps0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Deletion of Order</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f2 1278 556 36 24 22 -8 #rect
Ps0 f2 @|ProgramInterfaceIcon #fIcon
Ps0 f3 expr in #txt
Ps0 f3 outCond 'in.#order!=null
&&in.order.#orderId!=null
&&in.order.orderId>0' #txt
Ps0 f3 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f3 6 #arcStyle
Ps0 f3 1262 512 1296 556 #arcP
Ps0 f3 1 1296 512 #addKink
Ps0 f3 0 0.2413793103448276 0 10 #arcLabel
Ps0 f5 expr out #txt
Ps0 f5 1296 580 1262 624 #arcP
Ps0 f5 1 1296 624 #addKink
Ps0 f5 1 0.23684210526315788 0 0 #arcLabel
Ps0 f7 expr out #txt
Ps0 f7 382 1435 217 1539 #arcP
Ps0 f56 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f56 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f56 timeout 0 #txt
Ps0 f56 beanConfig '"#
#Thu Nov 17 23:55:38 CET 2016
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.MailScannerService;\\nimport ch.soreco.utilities.ivy.DateTimeUtils;\\nimport ch.soreco.orderbook.mail.dao.MailBoxConfigurationDao;\\n\\nboolean isConnected \\= MailScannerService.getInstance().isConnected();\\nMailBoxConfigurationDao dao \\= new MailBoxConfigurationDao();\\nin.mailBoxConfiguration \\= dao.refresh(in.mailBoxConfiguration);\\nboolean isRunnable \\= in.mailBoxConfiguration.isRunnable;\\nif(isRunnable \\=\\= null){\\n\\tisRunnable \\= false;\\n}\\nboolean isBeforeOrEqualToNow \\= true;\\nif(in.mailBoxConfiguration.nextScanAt is initialized&&\\!DateTimeUtils.isBeforeOrEqualToNow(in.mailBoxConfiguration.nextScanAt)){\\n\\tisBeforeOrEqualToNow \\= false;\\n}\\nin.isMailScanToBeStarted \\= isRunnable&&\\!isConnected&&isBeforeOrEqualToNow;\\n\\nivy.log.trace(""MailScannerService start\\:""+in.isMailScanToBeStarted+"" is Connected\\:""+isConnected+"", is Runnable\\:""+isRunnable+"", isBeforeOrEqualToNow\\: ""+isBeforeOrEqualToNow+ "" nextScanAt\\:""+in.mailBoxConfiguration.nextScanAt);\\n
"' #txt
Ps0 f56 exceptionHandler 141EEE4C24C8C88C-f23-buffer #txt
Ps0 f56 timeoutExceptionHandler 141EEE4C24C8C88C-f25-buffer #txt
Ps0 f56 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>check wheter the MailScannerService is connected
and MailBoxConfiguration is Runnable</name>
        <nameStyle>85,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f56 190 124 36 24 22 -8 #rect
Ps0 f56 @|ProgramInterfaceIcon #fIcon
Ps0 f57 expr out #txt
Ps0 f57 208 53 208 124 #arcP
Ps0 f13 expr out #txt
Ps0 f13 208 148 208 186 #arcP
Ps0 f65 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f65 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f65 timeout 0 #txt
Ps0 f65 beanConfig '"#
#Tue Jul 28 23:45:03 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=in.exceptionInformationMailSubjectPrefix \\= ""MailBoxConfiguration(""+String.valueOf(in.mailBoxConfiguration.id) + "") processing Error\\:"";\\nin.exceptionInformationMailFrom \\= in.mailBoxConfiguration.onExceptionMailFrom;\\nin.exceptionInformationMailTo \\= in.mailBoxConfiguration.onExceptionMailTo;\\nin.orderMatching \\= in.mailBoxConfiguration.orderMatching;
"' #txt
Ps0 f65 exceptionHandler 141EEE4C24C8C88C-f38-buffer #txt
Ps0 f65 timeoutExceptionHandler 141EEE4C24C8C88C-f34-buffer #txt
Ps0 f65 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>map exceptionInformation attributes
from MailConfiguration</name>
        <nameStyle>58,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f65 382 244 36 24 22 -8 #rect
Ps0 f65 @|ProgramInterfaceIcon #fIcon
Ps0 f70 expr in #txt
Ps0 f70 outCond in.isMailScanToBeStarted #txt
Ps0 f70 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f70 6 #arcStyle
Ps0 f70 222 200 400 244 #arcP
Ps0 f70 1 400 200 #addKink
Ps0 f70 0 0.09550561797752809 0 10 #arcLabel
Ps0 f71 expr out #txt
Ps0 f71 400 268 400 340 #arcP
Ps0 f45 expr in #txt
Ps0 f45 928 1278 928 1380 #arcP
Ps0 f45 0 0.4954991238285976 0 0 #arcLabel
Ps0 f46 expr in #txt
Ps0 f46 1480 750 1480 844 #arcP
Ps0 f46 0 0.4954991238285976 0 0 #arcLabel
Ps0 f39 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f39 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f39 timeout 0 #txt
Ps0 f39 beanConfig '"#
#Tue Jul 28 23:57:00 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.persistence.OrderService;\\nOrderService orderService \\= new OrderService();\\nin.order \\= orderService.save(in.order,in.orderMatching);
"' #txt
Ps0 f39 exceptionHandler 141EEE4C24C8C88C-f30-buffer #txt
Ps0 f39 timeoutExceptionHandler 141EEE4C24C8C88C-f28-buffer #txt
Ps0 f39 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Save of Order</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f39 654 724 36 24 22 -8 #rect
Ps0 f39 @|ProgramInterfaceIcon #fIcon
Ps0 f40 expr out #txt
Ps0 f40 672 676 672 724 #arcP
Ps0 f54 expr out #txt
Ps0 f54 672 748 672 780 #arcP
Ps0 f48 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f48 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f48 timeout 0 #txt
Ps0 f48 beanConfig '"#
#Thu Aug 06 14:42:18 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.utilities.ivy.DateTimeUtils;\\nif(in.mailBoxConfiguration.scanIntervalLiteral is initialized&&\\!in.mailBoxConfiguration.scanIntervalLiteral.isEmpty()){\\n\\tString literal \\= in.mailBoxConfiguration.scanIntervalLiteral;\\n\\tDateTime nextScanAt \\= new DateTime();\\n\\ttry {\\n\\t\\tnextScanAt \\= DateTimeUtils.add(literal,nextScanAt);\\n\\t\\tin.mailBoxConfiguration.setNextScanAt(nextScanAt);\\n\\t\\tivy.log.debug(""MailBoxConfiguration set next scan at\\:""+nextScanAt);\\n\\t} catch (Exception e){\\n\\t\\tin.mailBoxConfiguration.setScanIntervalLiteral(null);\\n\\t\\tin.mailBoxConfiguration.setNextScanAt(null);\\n\\t\\tivy.log.error(""Error during calculation of next MailBoxConfiguration scan time."",e);\\n\\t}\\n} else {\\n\\tin.mailBoxConfiguration.setScanIntervalLiteral(null);\\n\\tin.mailBoxConfiguration.setNextScanAt(null);\\n}
"' #txt
Ps0 f48 exceptionHandler 141EEE4C24C8C88C-f23-buffer #txt
Ps0 f48 timeoutExceptionHandler 141EEE4C24C8C88C-f25-buffer #txt
Ps0 f48 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>calculate next ScanTime</name>
        <nameStyle>23,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f48 382 1300 36 24 22 -8 #rect
Ps0 f48 @|ProgramInterfaceIcon #fIcon
Ps0 f58 expr in #txt
Ps0 f58 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>no</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f58 3 #arcStyle
Ps0 f58 400 574 400 1300 #arcP
Ps0 f58 0 0.05185185185185185 -10 0 #arcLabel
Ps0 f59 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f59 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f59 timeout 0 #txt
Ps0 f59 beanConfig '"#
#Thu Nov 17 23:22:04 CET 2016
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.dao.MailBoxConfigurationDao;\\n\\nMailBoxConfigurationDao dao \\= new MailBoxConfigurationDao();\\ndao.merge(in.mailBoxConfiguration);
"' #txt
Ps0 f59 exceptionHandler 141EEE4C24C8C88C-f23-buffer #txt
Ps0 f59 timeoutExceptionHandler 141EEE4C24C8C88C-f25-buffer #txt
Ps0 f59 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>save next ScanTime to MailBoxConfiguration</name>
        <nameStyle>42,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f59 382 1356 36 24 22 -8 #rect
Ps0 f59 @|ProgramInterfaceIcon #fIcon
Ps0 f60 expr out #txt
Ps0 f60 3 #arcStyle
Ps0 f60 400 1324 400 1356 #arcP
Ps0 f60 0 0.05185185185185184 -10 0 #arcLabel
Ps0 f27 expr out #txt
Ps0 f27 400 1380 400 1412 #arcP
Ps0 f6 expr in #txt
Ps0 f6 1480 1054 1310 1464 #arcP
Ps0 f6 1 1480 1464 #addKink
Ps0 f6 0 0.7111737891036466 0 0 #arcLabel
Ps0 f10 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f10 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f10 timeout 0 #txt
Ps0 f10 beanConfig '"#
#Thu Nov 17 23:22:52 CET 2016
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.dao.MailBoxConfigurationDao;\\n\\nMailBoxConfigurationDao dao \\= new MailBoxConfigurationDao();\\ndao.setAllToRunnable(false);
"' #txt
Ps0 f10 exceptionHandler '>> Ignore Exception' #txt
Ps0 f10 timeoutExceptionHandler '>> Ignore Exception' #txt
Ps0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Configurations to not running</name>
        <nameStyle>33,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f10 910 1532 36 24 -7 21 #rect
Ps0 f10 @|ProgramInterfaceIcon #fIcon
Ps0 f61 expr in #txt
Ps0 f61 928 1478 928 1532 #arcP
Ps0 f63 type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
Ps0 f63 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Ps0 f63 timeout 0 #txt
Ps0 f63 beanConfig '"#
#Fri Nov 18 01:24:20 CET 2016
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.MailScannerService;\\n\\nMailScannerService.getInstance().disconnectSilent();
"' #txt
Ps0 f63 exceptionHandler '>> Ignore Exception' #txt
Ps0 f63 timeoutExceptionHandler '>> Ignore Exception' #txt
Ps0 f63 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Disconnect from MailScannerService</name>
        <nameStyle>34,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f63 382 1532 36 24 -7 21 #rect
Ps0 f63 @|ProgramInterfaceIcon #fIcon
Ps0 f64 expr out #txt
Ps0 f64 910 1544 418 1544 #arcP
Ps0 f62 expr out #txt
Ps0 f62 382 1544 222 1544 #arcP
>Proto Ps0 .type ch.soreco.orderbook.mail.ProcessMailBoxProcessData #txt
>Proto Ps0 .processKind CALLABLE_SUB #txt
>Proto Ps0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel></swimlaneLabel>
        <swimlaneLabel>Exception Handling</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
    </language>
    <swimlaneSize>840</swimlaneSize>
    <swimlaneSize>1256</swimlaneSize>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-39322</swimlaneColor>
</elementInfo>
' #txt
>Proto Ps0 0 0 32 24 18 0 #rect
>Proto Ps0 @|BIcon #fIcon
Ps0 f25 mainOut f36 tail #connect
Ps0 f36 head f35 in #connect
Ps0 f23 mainOut f26 tail #connect
Ps0 f26 head f35 in #connect
Ps0 f12 mainOut f15 tail #connect
Ps0 f15 head f14 mainIn #connect
Ps0 f14 mainOut f17 tail #connect
Ps0 f17 head f16 in #connect
Ps0 f16 out f19 tail #connect
Ps0 f19 head f18 mainIn #connect
Ps0 f20 mainOut f24 tail #connect
Ps0 f24 head f16 in #connect
Ps0 f34 mainOut f42 tail #connect
Ps0 f42 head f37 in #connect
Ps0 f38 mainOut f43 tail #connect
Ps0 f43 head f37 in #connect
Ps0 f37 out f50 tail #connect
Ps0 f50 head f49 in #connect
Ps0 f52 out f21 tail #connect
Ps0 f21 head f20 mainIn #connect
Ps0 f18 mainOut f55 tail #connect
Ps0 f55 head f51 mainIn #connect
Ps0 f66 mainOut f53 tail #connect
Ps0 f53 head f52 in #connect
Ps0 f73 head f69 in #connect
Ps0 f69 out f8 tail #connect
Ps0 f8 head f1 mainIn #connect
Ps0 f78 mainOut f79 tail #connect
Ps0 f79 head f76 in #connect
Ps0 f77 mainOut f80 tail #connect
Ps0 f80 head f76 in #connect
Ps0 f76 out f75 tail #connect
Ps0 f75 head f9 in #connect
Ps0 f81 head f84 in #connect
Ps0 f84 out f86 tail #connect
Ps0 f86 head f29 in #connect
Ps0 f28 mainOut f88 tail #connect
Ps0 f88 head f87 in #connect
Ps0 f30 mainOut f32 tail #connect
Ps0 f32 head f87 in #connect
Ps0 f87 out f33 tail #connect
Ps0 f33 head f29 in #connect
Ps0 f89 mainOut f47 tail #connect
Ps0 f47 head f49 in #connect
Ps0 f41 mainOut f92 tail #connect
Ps0 f92 head f90 in #connect
Ps0 f31 mainOut f93 tail #connect
Ps0 f93 head f90 in #connect
Ps0 f90 out f94 tail #connect
Ps0 f94 head f44 in #connect
Ps0 f95 mainOut f11 tail #connect
Ps0 f11 head f44 in #connect
Ps0 f9 out f3 tail #connect
Ps0 f3 head f2 mainIn #connect
Ps0 f9 out f81 tail #connect
Ps0 f2 mainOut f5 tail #connect
Ps0 f5 head f84 in #connect
Ps0 f22 mainOut f7 tail #connect
Ps0 f7 head f69 in #connect
Ps0 f0 mainOut f57 tail #connect
Ps0 f57 head f56 mainIn #connect
Ps0 f56 mainOut f13 tail #connect
Ps0 f13 head f4 in #connect
Ps0 f4 out f70 tail #connect
Ps0 f70 head f65 mainIn #connect
Ps0 f4 out f73 tail #connect
Ps0 f65 mainOut f71 tail #connect
Ps0 f71 head f12 mainIn #connect
Ps0 f35 out f45 tail #connect
Ps0 f45 head f95 mainIn #connect
Ps0 f29 out f46 tail #connect
Ps0 f46 head f89 mainIn #connect
Ps0 f51 mainOut f40 tail #connect
Ps0 f40 head f39 mainIn #connect
Ps0 f39 mainOut f54 tail #connect
Ps0 f54 head f66 mainIn #connect
Ps0 f16 out f58 tail #connect
Ps0 f58 head f48 mainIn #connect
Ps0 f48 mainOut f60 tail #connect
Ps0 f60 head f59 mainIn #connect
Ps0 f59 mainOut f27 tail #connect
Ps0 f27 head f22 mainIn #connect
Ps0 f49 out f6 tail #connect
Ps0 f6 head f90 in #connect
Ps0 f44 out f61 tail #connect
Ps0 f61 head f10 mainIn #connect
Ps0 f10 mainOut f64 tail #connect
Ps0 f64 head f63 mainIn #connect
Ps0 f63 mainOut f62 tail #connect
Ps0 f62 head f69 in #connect
