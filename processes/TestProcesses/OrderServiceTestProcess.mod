[Ivy]
[>Created: Fri Nov 18 00:05:33 CET 2016]
1422E255D5544B2C 3.17 #module
>Proto >Proto Collection #zClass
Os0 OrderServiceTestProcess Big #zClass
Os0 B #cInfo
Os0 #process
Os0 @TextInP .resExport .resExport #zField
Os0 @TextInP .type .type #zField
Os0 @TextInP .processKind .processKind #zField
Os0 @AnnotationInP-0n ai ai #zField
Os0 @TextInP .xml .xml #zField
Os0 @TextInP .responsibility .responsibility #zField
Os0 @EndTask f1 '' #zField
Os0 @ProgramInterface f3 '' #zField
Os0 @StartRequest f0 '' #zField
Os0 @Alternative f21 '' #zField
Os0 @ProcessException f28 '' #zField
Os0 @ProcessException f27 '' #zField
Os0 @PushWFArc f34 '' #zField
Os0 @PushWFArc f32 '' #zField
Os0 @ProgramInterface f8 '' #zField
Os0 @Alternative f10 '' #zField
Os0 @PushWFArc f2 '' #zField
Os0 @PushWFArc f12 '' #zField
Os0 @ProcessException f7 '' #zField
Os0 @Alternative f13 '' #zField
Os0 @ProcessException f15 '' #zField
Os0 @PushWFArc f16 '' #zField
Os0 @PushWFArc f17 '' #zField
Os0 @Alternative f19 '' #zField
Os0 @PushWFArc f20 '' #zField
Os0 @PushWFArc f4 '' #zField
Os0 @ProgramInterface f14 '' #zField
Os0 @ProgramInterface f23 '' #zField
Os0 @PushWFArc f18 '' #zField
Os0 @GridStep f5 '' #zField
Os0 @PushWFArc f6 '' #zField
Os0 @PushWFArc f24 '' #zField
Os0 @Alternative f25 '' #zField
Os0 @ProcessException f26 '' #zField
Os0 @ProcessException f29 '' #zField
Os0 @PushWFArc f30 '' #zField
Os0 @PushWFArc f31 '' #zField
Os0 @Alternative f33 '' #zField
Os0 @PushWFArc f35 '' #zField
Os0 @PushWFArc f22 '' #zField
Os0 @PushWFArc f36 '' #zField
Os0 @ProgramInterface f39 '' #zField
Os0 @ProcessException f41 '' #zField
Os0 @Alternative f42 '' #zField
Os0 @ProcessException f43 '' #zField
Os0 @Alternative f44 '' #zField
Os0 @PushWFArc f45 '' #zField
Os0 @PushWFArc f46 '' #zField
Os0 @PushWFArc f47 '' #zField
Os0 @PushWFArc f48 '' #zField
Os0 @PushWFArc f9 '' #zField
Os0 @GridStep f50 '' #zField
Os0 @PushWFArc f51 '' #zField
Os0 @GridStep f37 '' #zField
Os0 @PushWFArc f38 '' #zField
Os0 @PushWFArc f11 '' #zField
Os0 @PushWFArc f49 '' #zField
Os0 @StartRequest f40 '' #zField
Os0 @EndTask f52 '' #zField
Os0 @GridStep f54 '' #zField
Os0 @PushWFArc f55 '' #zField
Os0 @PushWFArc f53 '' #zField
Os0 @StartRequest f56 '' #zField
Os0 @ProgramInterface f57 '' #zField
Os0 @PushWFArc f58 '' #zField
Os0 @EndTask f59 '' #zField
Os0 @PushWFArc f60 '' #zField
>Proto Os0 Os0 OrderServiceTestProcess #zField
Os0 f1 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f1 99 1131 26 26 14 0 #rect
Os0 f1 @|EndIcon #fIcon
Os0 f3 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f3 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Os0 f3 timeout 0 #txt
Os0 f3 beanConfig '"#
#Fri Nov 08 10:45:50 CET 2013
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.persistence.OrderService;\\nOrderService orderService \\= new OrderService();\\nin.order \\= orderService.save(in.order);
"' #txt
Os0 f3 exceptionHandler 1422E255D5544B2C-f28-buffer #txt
Os0 f3 timeoutExceptionHandler 1422E255D5544B2C-f27-buffer #txt
Os0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Test Save of Order</name>
        <nameStyle>18,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f3 94 452 36 24 22 -8 #rect
Os0 f3 @|ProgramInterfaceIcon #fIcon
Os0 f0 outLink test.ivp #txt
Os0 f0 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f0 inParamDecl '<> param;' #txt
Os0 f0 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f0 guid 1422E2565E4EEFDB #txt
Os0 f0 requestEnabled true #txt
Os0 f0 triggerEnabled false #txt
Os0 f0 callSignature test() #txt
Os0 f0 persist false #txt
Os0 f0 startName 'Test Insert & Delete of a new Order' #txt
Os0 f0 startDescription 'This process inserts a new Order. 
OrderType, OrderState and priority are derived from OrderMatching ''IL''.
If the order is successfuly inserted, a log message is provided and the new Order will be deleted after.' #txt
Os0 f0 taskData '#
#Thu Aug 13 13:54:12 CEST 2015
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
Os0 f0 caseData '#
#Thu Aug 13 13:54:12 CEST 2015
businessCalendarName=
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
Os0 f0 showInStartList 0 #txt
Os0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();
import ch.ivyteam.ivy.request.impl.DefaultCalendarProxy;
DefaultCalendarProxy calendarProxy = ivy.cal as DefaultCalendarProxy;
taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Os0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>test.ivp</name>
        <nameStyle>8,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f0 @C|.responsibility Everybody #txt
Os0 f0 99 59 26 26 14 0 #rect
Os0 f0 @|StartRequestIcon #fIcon
Os0 f21 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f21 658 554 28 28 14 0 #rect
Os0 f21 @|AlternativeIcon #fIcon
Os0 f21 -1|-1|-16777216 #nodeStyle
Os0 f28 .resExport export #txt
Os0 f28 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f28 actionTable 'out=in;
' #txt
Os0 f28 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Os0 f28 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in Test save of Order</name>
        <nameStyle>31,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f28 715 491 26 26 17 -7 #rect
Os0 f28 @|ExceptionIcon #fIcon
Os0 f27 .resExport export #txt
Os0 f27 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f27 actionTable 'out=in;
' #txt
Os0 f27 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Os0 f27 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in Test save of Order</name>
        <nameStyle>29,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f27 659 451 26 26 19 -7 #rect
Os0 f27 @|ExceptionIcon #fIcon
Os0 f34 expr out #txt
Os0 f34 728 517 682 564 #arcP
Os0 f34 1 728 549 #addKink
Os0 f34 1 0.39981246067624665 0 0 #arcLabel
Os0 f32 expr out #txt
Os0 f32 672 477 672 554 #arcP
Os0 f32 0 0.4937127750056547 0 0 #arcLabel
Os0 f8 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f8 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Os0 f8 timeout 0 #txt
Os0 f8 beanConfig '"#
#Fri Nov 08 10:44:39 CET 2013
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=ivy.log.error(in.exceptionInformation);
"' #txt
Os0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Log Error</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f8 366 1028 36 24 22 -8 #rect
Os0 f8 @|ProgramInterfaceIcon #fIcon
Os0 f10 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f10 98 1074 28 28 14 0 #rect
Os0 f10 @|AlternativeIcon #fIcon
Os0 f10 -1|-1|-16777216 #nodeStyle
Os0 f2 expr in #txt
Os0 f2 112 1102 112 1131 #arcP
Os0 f12 expr out #txt
Os0 f12 384 1052 126 1088 #arcP
Os0 f12 1 384 1088 #addKink
Os0 f12 1 0.3652935238190141 0 0 #arcLabel
Os0 f7 .resExport export #txt
Os0 f7 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f7 actionTable 'out=in;
' #txt
Os0 f7 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Os0 f7 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in Test prepare of Order</name>
        <nameStyle>32,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f7 939 291 26 26 19 -7 #rect
Os0 f7 @|ExceptionIcon #fIcon
Os0 f13 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f13 938 394 28 28 14 0 #rect
Os0 f13 @|AlternativeIcon #fIcon
Os0 f13 -1|-1|-16777216 #nodeStyle
Os0 f15 .resExport export #txt
Os0 f15 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f15 actionTable 'out=in;
' #txt
Os0 f15 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Os0 f15 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in Test prepare of Order</name>
        <nameStyle>34,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f15 995 331 26 26 17 -7 #rect
Os0 f15 @|ExceptionIcon #fIcon
Os0 f16 expr out #txt
Os0 f16 1008 357 962 404 #arcP
Os0 f16 1 1008 388 #addKink
Os0 f16 1 0.39981246067624665 0 0 #arcLabel
Os0 f17 expr out #txt
Os0 f17 952 317 952 394 #arcP
Os0 f17 0 0.4937127750056547 0 0 #arcLabel
Os0 f19 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f19 658 618 28 28 14 0 #rect
Os0 f19 @|AlternativeIcon #fIcon
Os0 f19 -1|-1|-16777216 #nodeStyle
Os0 f20 expr in #txt
Os0 f20 672 582 672 618 #arcP
Os0 f4 expr out #txt
Os0 f4 112 340 112 452 #arcP
Os0 f14 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f14 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Os0 f14 timeout 0 #txt
Os0 f14 beanConfig '"#
#Tue Jul 28 19:57:26 CEST 2015
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.bo.Attachment;\\nimport ch.soreco.webbies.common.db.BlobFile;\\nimport ch.soreco.orderbook.bo.Scanning;\\nimport ch.soreco.orderbook.bo.Order;\\n\\n//Build a BlobFile which represents the mail content body\\nBlobFile mailContentBlob \\= new BlobFile();\\nStringBuilder emailBody \\= new StringBuilder("""");\\nemailBody.append(""<html>"");\\nemailBody.append(""\\\\n\\\\t<body>"");\\nemailBody.append(""\\\\n\\\\t\\\\t<h1>The Mail Content</h1>"");\\nemailBody.append(""\\\\n\\\\t\\\\t<p>This should be the Mail Body PDF</p>"");\\nemailBody.append(""\\\\n\\\\t\\\\t<p>The document shown is coming out of the OrderServiceTestProcess</p>"");\\nemailBody.append(""\\\\n\\\\t</body>"");\\nemailBody.append(""\\\\n</html>"");\\nmailContentBlob.setContentType(""text/html"");\\nmailContentBlob.setExtension(""html"");\\nmailContentBlob.setFileName(""MailBody.html"");\\nmailContentBlob.setBytes(emailBody.toString().getBytes());\\n\\n//initialize a new Order set the transient field scanning\\n//with an initialized Scanning containing the orderMatching\\nOrder order \\= new Order();\\nScanning scanning \\= new Scanning();\\nscanning.setOrderMatching(in.orderMatching);\\nscanning.setScanBlobFile(mailContentBlob);\\n//Prepare a list of BlobFiles representing the attachments of a Mail\\nList<BlobFile> attachmentBlobs \\= new List<BlobFile>();\\nfor(int i\\=0;i<3;i++){\\n\\tBlobFile attachmentBlob \\= new BlobFile();\\n\\tStringBuilder attachmentBody \\= new StringBuilder("""");\\n\\tattachmentBody.append(""<html>"");\\n\\tattachmentBody.append(""\\\\n\\\\t<body>"");\\n\\tattachmentBody.append(""\\\\n\\\\t\\\\t<h1>The Mail Attachment (""+i+"")</h1>"");\\n\\tattachmentBody.append(""\\\\n\\\\t\\\\t<p>This should be the Mail Attachment</p>"");\\n\\tattachmentBody.append(""\\\\n\\\\t\\\\t<p>The document shown is coming out of the OrderServiceTestProcess</p>"");\\n\\tattachmentBody.append(""\\\\n\\\\t</body>"");\\n\\tattachmentBody.append(""\\\\n</html>"");\\n\\tattachmentBlob.setContentType(""text/html"");\\n\\tattachmentBlob.setExtension(""html"");\\n\\tattachmentBlob.setFileName(""MailBody.html"");\\n\\tattachmentBlob.setBytes(attachmentBody.toString().getBytes());\\n\\tattachmentBlobs.add(attachmentBlob);\\n\\t}\\n\\n//Prepare the list of attachments by the list of BlobFiles built above\\nList<ch.soreco.orderbook.bo.Attachment> attachments \\= new List<ch.soreco.orderbook.bo.Attachment>();\\nfor(BlobFile attachmentBlob\\:attachmentBlobs){\\n\\tAttachment attachment \\= new Attachment();\\n\\tch.soreco.common.bo.File attachmentFile \\= new ch.soreco.common.bo.File();\\n\\tattachmentFile.setBlobFile(attachmentBlob);\\n\\tattachment.setFile(attachmentFile);\\n\\tattachments.add(attachment);\\n\\t}\\nscanning.setAttachments(attachments);\\norder.setScanning(scanning);\\n\\nin.order \\= order;
"' #txt
Os0 f14 exceptionHandler 1422E255D5544B2C-f15-buffer #txt
Os0 f14 timeoutExceptionHandler 1422E255D5544B2C-f7-buffer #txt
Os0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Test prepare of Order</name>
        <nameStyle>21,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f14 94 316 36 24 22 -8 #rect
Os0 f14 @|ProgramInterfaceIcon #fIcon
Os0 f23 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f23 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Os0 f23 timeout 0 #txt
Os0 f23 beanConfig '"#
#Fri Nov 08 10:38:40 CET 2013
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.persistence.OrderMatchingService;\\nOrderMatchingService orderMatchingService \\= new OrderMatchingService();\\nin.orderMatching \\= orderMatchingService.findFirstByDocType(in.docType);
"' #txt
Os0 f23 exceptionHandler 1422E255D5544B2C-f26-buffer #txt
Os0 f23 timeoutExceptionHandler 1422E255D5544B2C-f29-buffer #txt
Os0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Test Find of OrderMatching by docType</name>
        <nameStyle>37,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f23 94 172 36 24 22 -8 #rect
Os0 f23 @|ProgramInterfaceIcon #fIcon
Os0 f18 expr out #txt
Os0 f18 112 196 112 316 #arcP
Os0 f5 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f5 actionTable 'out=in;
out.docType="IL";
' #txt
Os0 f5 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set docType</name>
        <nameStyle>11,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f5 94 108 36 24 20 -2 #rect
Os0 f5 @|StepIcon #fIcon
Os0 f6 expr out #txt
Os0 f6 112 85 112 108 #arcP
Os0 f24 expr out #txt
Os0 f24 112 132 112 172 #arcP
Os0 f25 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f25 1130 210 28 28 14 0 #rect
Os0 f25 @|AlternativeIcon #fIcon
Os0 f25 -1|-1|-16777216 #nodeStyle
Os0 f26 .resExport export #txt
Os0 f26 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f26 actionTable 'out=in;
' #txt
Os0 f26 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Os0 f26 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in Test Find of OrderMatching by docType</name>
        <nameStyle>50,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f26 1187 147 26 26 17 -7 #rect
Os0 f26 @|ExceptionIcon #fIcon
Os0 f29 .resExport export #txt
Os0 f29 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f29 actionTable 'out=in;
' #txt
Os0 f29 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Os0 f29 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in Test Find of OrderMatching by docType</name>
        <nameStyle>48,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f29 1131 107 26 26 19 -7 #rect
Os0 f29 @|ExceptionIcon #fIcon
Os0 f30 expr out #txt
Os0 f30 1200 173 1155 221 #arcP
Os0 f30 1 1200 206 #addKink
Os0 f30 1 0.39981246067624665 0 0 #arcLabel
Os0 f31 expr out #txt
Os0 f31 1144 133 1144 210 #arcP
Os0 f31 0 0.4937127750056547 0 0 #arcLabel
Os0 f33 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f33 938 562 28 28 14 0 #rect
Os0 f33 @|AlternativeIcon #fIcon
Os0 f33 -1|-1|-16777216 #nodeStyle
Os0 f35 expr in #txt
Os0 f35 952 422 952 562 #arcP
Os0 f35 0 0.9154305834101121 0 0 #arcLabel
Os0 f22 expr in #txt
Os0 f22 952 590 686 632 #arcP
Os0 f22 1 952 632 #addKink
Os0 f22 1 0.381823515022974 0 0 #arcLabel
Os0 f36 expr in #txt
Os0 f36 1144 238 966 576 #arcP
Os0 f36 1 1144 576 #addKink
Os0 f36 0 0.643552402497588 0 0 #arcLabel
Os0 f39 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f39 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Os0 f39 timeout 0 #txt
Os0 f39 beanConfig '"#
#Fri Nov 08 15:21:36 CET 2013
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.persistence.OrderService;\\nOrderService orderService \\= new OrderService();\\norderService.removeById(in.order.getOrderId());
"' #txt
Os0 f39 exceptionHandler 1422E255D5544B2C-f43-buffer #txt
Os0 f39 timeoutExceptionHandler 1422E255D5544B2C-f41-buffer #txt
Os0 f39 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Test Deletion of Order</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f39 94 748 36 24 22 -8 #rect
Os0 f39 @|ProgramInterfaceIcon #fIcon
Os0 f41 .resExport export #txt
Os0 f41 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f41 actionTable 'out=in;
' #txt
Os0 f41 actionCode 'in.exceptionInformation = new  ch.soreco.utilities.exception.TimeoutSpecificExceptionInformationWrapperException(exception);
' #txt
Os0 f41 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f41 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>TimeOut in Test Deletion of Order</name>
        <nameStyle>33,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f41 371 747 26 26 19 -7 #rect
Os0 f41 @|ExceptionIcon #fIcon
Os0 f42 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f42 370 914 28 28 14 0 #rect
Os0 f42 @|AlternativeIcon #fIcon
Os0 f42 -1|-1|-16777216 #nodeStyle
Os0 f43 .resExport export #txt
Os0 f43 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f43 actionTable 'out=in;
' #txt
Os0 f43 actionCode 'in.exceptionInformation = new ch.soreco.utilities.exception.ExceptionInformationWrapperException(exception);' #txt
Os0 f43 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f43 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Exception in Test Deletion of Order</name>
        <nameStyle>35,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f43 427 787 26 26 17 -7 #rect
Os0 f43 @|ExceptionIcon #fIcon
Os0 f44 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f44 370 850 28 28 14 0 #rect
Os0 f44 @|AlternativeIcon #fIcon
Os0 f44 -1|-1|-16777216 #nodeStyle
Os0 f45 expr out #txt
Os0 f45 440 813 394 860 #arcP
Os0 f45 1 440 845 #addKink
Os0 f45 1 0.39981246067624665 0 0 #arcLabel
Os0 f46 expr out #txt
Os0 f46 384 773 384 850 #arcP
Os0 f46 0 0.4937127750056547 0 0 #arcLabel
Os0 f47 expr in #txt
Os0 f47 384 878 384 914 #arcP
Os0 f48 expr in #txt
Os0 f48 672 646 398 928 #arcP
Os0 f48 1 672 928 #addKink
Os0 f48 0 0.8231450950741397 0 0 #arcLabel
Os0 f9 expr in #txt
Os0 f9 384 942 384 1028 #arcP
Os0 f50 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f50 actionTable 'out=in;
' #txt
Os0 f50 actionCode 'ivy.log.info("Successfully saved order "+in.order.orderId);' #txt
Os0 f50 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f50 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Log Info: Successfully saved order</name>
        <nameStyle>34,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f50 94 604 36 24 20 -2 #rect
Os0 f50 @|StepIcon #fIcon
Os0 f51 expr out #txt
Os0 f51 112 476 112 604 #arcP
Os0 f37 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f37 actionTable 'out=in;
' #txt
Os0 f37 actionCode 'ivy.log.info("Successfully deleted order "+in.order.orderId);' #txt
Os0 f37 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f37 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Log Info: Successfully deleted order</name>
        <nameStyle>36,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f37 94 892 36 24 20 -2 #rect
Os0 f37 @|StepIcon #fIcon
Os0 f38 expr out #txt
Os0 f38 112 772 112 892 #arcP
Os0 f11 expr out #txt
Os0 f11 112 916 112 1074 #arcP
Os0 f49 expr out #txt
Os0 f49 112 628 112 748 #arcP
Os0 f40 outLink start.ivp #txt
Os0 f40 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f40 inParamDecl '<> param;' #txt
Os0 f40 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f40 guid 1541A8C65153F7A9 #txt
Os0 f40 requestEnabled true #txt
Os0 f40 triggerEnabled false #txt
Os0 f40 callSignature start() #txt
Os0 f40 persist false #txt
Os0 f40 startName 'Test Word To PDF Conversion' #txt
Os0 f40 taskData '#
#Fri Apr 15 17:33:27 CEST 2016
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
Os0 f40 caseData '#
#Fri Apr 15 17:33:27 CEST 2016
businessCalendarName=
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
Os0 f40 showInStartList 1 #txt
Os0 f40 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();
import ch.ivyteam.ivy.request.impl.DefaultCalendarProxy;
DefaultCalendarProxy calendarProxy = ivy.cal as DefaultCalendarProxy;
taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Os0 f40 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f40 @C|.responsibility Everybody #txt
Os0 f40 99 1251 26 26 14 0 #rect
Os0 f40 @|StartRequestIcon #fIcon
Os0 f52 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f52 99 1379 26 26 14 0 #rect
Os0 f52 @|EndIcon #fIcon
Os0 f54 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f54 actionTable 'out=in;
' #txt
Os0 f54 actionCode 'import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import ch.soreco.utilities.file.FileUtils;

import com.aspose.words.Document;
		Document doc = new Document("C:/Office/zweiplus/App/workspaces/Mailscanning/resources/01_Test_eMail_FaxMail2Ivy_Word.doc");
		Path tempPdf = Files.createTempFile("WordToPdfConversion_01_Test_eMail_FaxMail2Ivy_Word", ".pdf");
		doc.save(tempPdf.toString());
		ivy.log.debug("Converted Doc File to {0}", tempPdf);
' #txt
Os0 f54 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f54 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Test PDF Conversion</name>
        <nameStyle>19,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f54 94 1316 36 24 20 -2 #rect
Os0 f54 @|StepIcon #fIcon
Os0 f55 expr out #txt
Os0 f55 112 1277 112 1316 #arcP
Os0 f53 expr out #txt
Os0 f53 112 1340 112 1379 #arcP
Os0 f56 outLink resetMailboxConfigurationToRunnable.ivp #txt
Os0 f56 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f56 inParamDecl '<> param;' #txt
Os0 f56 actionDecl 'ch.soreco.orderbook.mail.test.OrderServiceTestProcessData out;
' #txt
Os0 f56 guid 1587488372D76DF5 #txt
Os0 f56 requestEnabled true #txt
Os0 f56 triggerEnabled false #txt
Os0 f56 callSignature resetMailboxConfigurationToRunnable() #txt
Os0 f56 persist false #txt
Os0 f56 startName 'Set all Mailbox Configurations to Runnable' #txt
Os0 f56 taskData '#
#Fri Nov 18 00:05:07 CET 2016
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
Os0 f56 caseData '#
#Fri Nov 18 00:05:07 CET 2016
businessCalendarName=
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
Os0 f56 showInStartList 1 #txt
Os0 f56 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();
import ch.ivyteam.ivy.request.impl.DefaultCalendarProxy;
DefaultCalendarProxy calendarProxy = ivy.cal as DefaultCalendarProxy;
taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Os0 f56 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>resetMailboxConfigurationToRunnable.ivp</name>
        <nameStyle>39,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f56 @C|.responsibility Administrator #txt
Os0 f56 211 51 26 26 14 0 #rect
Os0 f56 @|StartRequestIcon #fIcon
Os0 f57 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f57 outerBean "ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension" #txt
Os0 f57 timeout 0 #txt
Os0 f57 beanConfig '"#
#Fri Nov 18 00:05:31 CET 2016
ch.soreco.utilities.ivy.IvyScriptEditorUserProcessExtension.IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD=import ch.soreco.orderbook.mail.dao.MailBoxConfigurationDao;\\n\\nMailBoxConfigurationDao dao \\= new MailBoxConfigurationDao();\\ndao.setAllToRunnable(true);
"' #txt
Os0 f57 exceptionHandler '>> Ignore Exception' #txt
Os0 f57 timeoutExceptionHandler '>> Ignore Exception' #txt
Os0 f57 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Configurations to not running</name>
        <nameStyle>33,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Os0 f57 230 132 36 24 -7 21 #rect
Os0 f57 @|ProgramInterfaceIcon #fIcon
Os0 f58 expr out #txt
Os0 f58 227 76 248 132 #arcP
Os0 f59 type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
Os0 f59 227 227 26 26 14 0 #rect
Os0 f59 @|EndIcon #fIcon
Os0 f60 expr out #txt
Os0 f60 248 156 241 227 #arcP
>Proto Os0 .type ch.soreco.orderbook.mail.test.OrderServiceTestProcessData #txt
>Proto Os0 .processKind NORMAL #txt
>Proto Os0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel></swimlaneLabel>
        <swimlaneLabel>Exception Handling</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
    </language>
    <swimlaneSize>280</swimlaneSize>
    <swimlaneSize>1032</swimlaneSize>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-39322</swimlaneColor>
</elementInfo>
' #txt
>Proto Os0 0 0 32 24 18 0 #rect
>Proto Os0 @|BIcon #fIcon
Os0 f28 mainOut f34 tail #connect
Os0 f34 head f21 in #connect
Os0 f27 mainOut f32 tail #connect
Os0 f32 head f21 in #connect
Os0 f10 out f2 tail #connect
Os0 f2 head f1 mainIn #connect
Os0 f8 mainOut f12 tail #connect
Os0 f12 head f10 in #connect
Os0 f15 mainOut f16 tail #connect
Os0 f16 head f13 in #connect
Os0 f7 mainOut f17 tail #connect
Os0 f17 head f13 in #connect
Os0 f21 out f20 tail #connect
Os0 f20 head f19 in #connect
Os0 f14 mainOut f4 tail #connect
Os0 f4 head f3 mainIn #connect
Os0 f23 mainOut f18 tail #connect
Os0 f18 head f14 mainIn #connect
Os0 f0 mainOut f6 tail #connect
Os0 f6 head f5 mainIn #connect
Os0 f5 mainOut f24 tail #connect
Os0 f24 head f23 mainIn #connect
Os0 f26 mainOut f30 tail #connect
Os0 f30 head f25 in #connect
Os0 f29 mainOut f31 tail #connect
Os0 f31 head f25 in #connect
Os0 f13 out f35 tail #connect
Os0 f35 head f33 in #connect
Os0 f33 out f22 tail #connect
Os0 f22 head f19 in #connect
Os0 f25 out f36 tail #connect
Os0 f36 head f33 in #connect
Os0 f43 mainOut f45 tail #connect
Os0 f45 head f44 in #connect
Os0 f41 mainOut f46 tail #connect
Os0 f46 head f44 in #connect
Os0 f44 out f47 tail #connect
Os0 f47 head f42 in #connect
Os0 f19 out f48 tail #connect
Os0 f48 head f42 in #connect
Os0 f42 out f9 tail #connect
Os0 f9 head f8 mainIn #connect
Os0 f3 mainOut f51 tail #connect
Os0 f51 head f50 mainIn #connect
Os0 f39 mainOut f38 tail #connect
Os0 f38 head f37 mainIn #connect
Os0 f37 mainOut f11 tail #connect
Os0 f11 head f10 in #connect
Os0 f50 mainOut f49 tail #connect
Os0 f49 head f39 mainIn #connect
Os0 f40 mainOut f55 tail #connect
Os0 f55 head f54 mainIn #connect
Os0 f54 mainOut f53 tail #connect
Os0 f53 head f52 mainIn #connect
Os0 f56 mainOut f58 tail #connect
Os0 f58 head f57 mainIn #connect
Os0 f57 mainOut f60 tail #connect
Os0 f60 head f59 mainIn #connect
