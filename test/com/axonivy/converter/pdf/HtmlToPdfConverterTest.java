package com.axonivy.converter.pdf;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.log.Logger;
import ch.soreco.utilities.file.FileUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Ivy.class)
public class HtmlToPdfConverterTest {
	private static final String[] TEST_FILE_NAMES = new String[] { "htmlconversiontest.html", "test.ms.mail.html" };
	private static final String[] IMAGES_NAMES = new String[] { "smiley.png", "img_girl.jpg",
			"https://www.w3schools.com/images/w3schools_green.jpg", "clouds.jpg" };
	private HtmlToPdfConverter converter;
	private WarningHandler warningHandler;
	private Map<String, byte[]> images;

	@Before
	public void setUp() throws Exception {
		Logger logger = Logger.getLogger(Ivy.class);
		mockStatic(Ivy.class);
		when(Ivy.log()).thenReturn(logger);

		converter = new HtmlToPdfConverter();
		warningHandler = new WarningHandler() {
			@Override
			public void warn(int source, int warningType, String description) {
				Ivy.log().warn("source: {0}, warningType: {1}, description: {2}", source, warningType, description);
			}
		};
		warningHandler.warn(0, 0, "Warning Handler will be logged to Ivy Logger");
		this.images = new HashMap<String, byte[]>();
		for (String testFileName : IMAGES_NAMES) {
			URL source = FileUtils.getResource(testFileName);
			if(source!=null){
				try {
					byte[] content = IOUtils.toByteArray(source.openStream());
					images.put(testFileName, content);
				} catch (Exception e) {
					// ignore this, since the host may is not online
					e.printStackTrace();
				}				
			} else {
				Ivy.log().debug("{0} cannot be found as resource on the File System!", testFileName);
			}
		}
	}

	@Test
	public void testConvert() throws IOException, Exception {		
		Path dir = Files.createTempDirectory("HtmlToPdfConversion_");
		for (String testFileName : TEST_FILE_NAMES) {
			URL source = FileUtils.getResource(testFileName);

			Path target = Files.createTempFile(dir, testFileName+"_", ".pdf");
			try (FileOutputStream fos = new FileOutputStream(target.toFile())) {
				converter.convert(source.openStream(), fos, warningHandler, images);
			}
			boolean condition = Files.size(target) > 0;
			Assert.assertTrue("Whether the PDF file has content", condition);
			if (condition) {
				// Files.deleteIfExists(target);
				// System.out.println("Deleted " + target.toString());
			}
		}
		Ivy.log().debug("Test PDF's are to be found in {0}", dir);
	}

	@After
	public void tearDown() throws Exception {
	}

}
