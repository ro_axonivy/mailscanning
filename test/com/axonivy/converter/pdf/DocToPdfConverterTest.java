package com.axonivy.converter.pdf;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.soreco.utilities.file.FileUtils;

public class DocToPdfConverterTest {
	private static final String[] TEST_FILE_NAMES = new String[] { "01_Test_eMail_FaxMail2Ivy_Word.doc",
			"01_Test_eMail_FaxMail2Ivy_Word.docx" };
	private DocToPdfConverter converter;
	private WarningHandler warningHandler;

	@Before
	public void setUp() throws Exception {
		converter = new DocToPdfConverter();
		warningHandler = new WarningHandler() {
			@Override
			public void warn(int source, int warningType, String description) {
				System.out.println(String.format("source:%d, warningType:%d, description:%s", source, warningType, description));
			}
		};

	}

	@Test
	public void testConvert() throws IOException, Exception {
		for (String testFileName : TEST_FILE_NAMES) {
			URL source = FileUtils.getResource(testFileName);
			Path target = Files.createTempFile("WordToPdfConversion_" + testFileName, ".pdf");
			try (FileOutputStream fos = new FileOutputStream(target.toFile())) {
				InputStream inputStream = source.openStream();
				if (converter.isConvertable(inputStream)) {
					converter.convert(source.openStream(), fos, warningHandler);
					System.out.println("Saved " + testFileName + " to " + target.toString());
				} else {
					System.out.println("The file " + testFileName + " is not convertable.");
				}
			}
			boolean condition = Files.size(target) > 0;
			Assert.assertTrue("Whether the PDF file has content", condition);
			if (condition) {
				// Files.deleteIfExists(target);
				// System.out.println("Deleted " + target.toString());
			}
		}
	}

	@After
	public void tearDown() throws Exception {
	}

}
