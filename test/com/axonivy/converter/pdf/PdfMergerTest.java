package com.axonivy.converter.pdf;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.log.Logger;
import ch.soreco.utilities.file.FileUtils;
import ch.soreco.webbies.common.db.BlobFile;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Ivy.class)
public class PdfMergerTest {
	private static final String[] TEST_PDF_FILE_NAMES = new String[] {
			"01_Test_eMail_FaxMail2Ivy_bzpFormularPDF_1.pdf", "02_Test_eMail_FaxMail2Ivy_PDF_Quer.pdf","SD_eMailDeckblatt.pdf",
			"04_Test_eMail_FaxMail2Ivy_TIFF.pdf", "Testfax.pdf", "Oversized Pdf File portrait.pdf", "Oversized Pdf File.pdf" };
	private PdfMerger merger;
	private List<BlobFile> pdfs;
	
	@Test
	public void testMerge() throws IOException, Exception {
		Logger logger = Logger.getLogger(Ivy.class);
		mockStatic(Ivy.class);
		when(Ivy.log()).thenReturn(logger);
	
		Path target = Files.createTempFile("MergedPdfs_", ".pdf");
		logger.info("Start merging {0} pdfs into {1}", pdfs.size(), target);
		try (FileOutputStream out = new FileOutputStream(target.toFile())) {
			merger.merge("The merged files.....", out, pdfs);
		}
		System.out.println("Merged " + String.valueOf(pdfs.size()) + " files into:" + target);
		boolean condition = Files.exists(target);
		Assert.assertTrue("The merged file", condition);
	}

	@Before
	public void setUp() throws Exception {
		merger = new PdfMerger();
		
		pdfs = new ArrayList<BlobFile>();
		for (String testFileName : TEST_PDF_FILE_NAMES) {
			URL source = FileUtils.getResource(testFileName);
			try (ByteArrayOutputStream pdfOut = new ByteArrayOutputStream()) {
				IOUtils.copy(source.openStream(), pdfOut);
				byte[] content = pdfOut.toByteArray();
				if(content.length>0){
					BlobFile blob = new BlobFile();
					blob.setBytes(content);
					blob.setFileName(testFileName);
					pdfs.add(blob);					
				}
			}
		}
	}

	@After
	public void tearDown() throws Exception {
	}

}
