package ch.soreco.utilities.ivy;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.ivyteam.ivy.scripting.objects.Duration;

public class DateTimeUtilsTest {
	@Test
	public void testNow() {
		Calendar calendar = Calendar.getInstance();
		calendar.clear(Calendar.MILLISECOND);
		long minimumDate = calendar.getTimeInMillis();
		calendar.add(Calendar.SECOND, 1);
		long maximumDate = calendar.getTimeInMillis();
		DateTime now = DateTimeUtils.now();
		long nowDate = now.toCalendar().getTimeInMillis();
		boolean actual = nowDate >= minimumDate && nowDate <= maximumDate;
		Assert.assertEquals(true, actual);
	}

	@Test
	public void testIsBeforeOrEqualToNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		boolean onNull = DateTimeUtils.isBeforeOrEqualToNow(null);
		Assert.assertEquals(true, onNull);
		
		DateTime now = new DateTime();
		String nowString = sdf.format(now.toJavaDate());
		boolean onNow = DateTimeUtils.isBeforeOrEqualToNow(now);
		Assert.assertEquals("OnNow "+nowString, true, onNow);


		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.SECOND, -1);
		String beforeString = sdf.format(calendar.getTime());
		DateTime before = new DateTime(beforeString);
		String beforeStringOfDateTime = sdf.format(before.toJavaDate());
		Assert.assertEquals(beforeString, beforeStringOfDateTime);
		boolean onBefore = DateTimeUtils.isBeforeOrEqualToNow(before);
		Assert.assertEquals("OnBefore " + beforeString, true, onBefore);

		calendar.add(Calendar.SECOND, 3);
		String afterString = sdf.format(calendar.getTime());
		DateTime after = new DateTime(afterString);
		String afterStringOfDateTime = sdf.format(after.toJavaDate());
		Assert.assertEquals(afterString, afterStringOfDateTime);
		boolean onAfter = DateTimeUtils.isBeforeOrEqualToNow(after);
		Assert.assertEquals("OnAfter " + afterString, false, onAfter);
	}

	@Test
	public void testToDurationString() {
		String literal = "5m30s";
		Duration duration = DateTimeUtils.toDuration(literal);
		long expected = 330L;
		long actual = duration.toNumber();
		String message = String.format("Literal was %s", literal);
		Assert.assertEquals(message, expected, actual);
	}

	@Test
	public void testAddStringDateTime() {
		DateTime dateTime = new DateTime();
		Date date = dateTime.toJavaDate();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, 5);
		cal.add(Calendar.MINUTE, 15);
		Date expected = cal.getTime();
		String literal = "5h15m";
		DateTime result = DateTimeUtils.add(literal, dateTime);
		Date actual = result.toJavaDate();
		String message = String.format("Literal was %s", literal);
		Assert.assertEquals(message, expected, actual);
	}

	@Test
	public void testAddDurationDateTime() {
		DateTime dateTime = new DateTime();
		Date date = dateTime.toJavaDate();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, 5);
		Date expected = cal.getTime();
		Duration duration = new Duration(300);
		String literal = duration.toIsoFormat();
		DateTime result = DateTimeUtils.add(duration, dateTime);
		Date actual = result.toJavaDate();
		String message = String.format("Literal was %s", literal);
		Assert.assertEquals(message, expected, actual);
	}
}
