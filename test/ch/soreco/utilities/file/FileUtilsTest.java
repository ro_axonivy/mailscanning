package ch.soreco.utilities.file;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Properties;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

public class FileUtilsTest {
	private static final String[][] TEST_CASES = new String[][] {
			{ "Test Spaced Text", "TestSpacedText" },
			{ "Test \\ Text", "TestText" },
			{ "Test / Text", "TestText" },
			{ ".Test Text", "TestText" },
			{ "Test � Text.", "TestaeText" },
			{
					"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
					"SedutperspiciatisundeomnisistenatuserrorsitvoluptatemaccusantiumdoloremquelaudantiumtotamremaperiameaqueipsaquaeabilloinventoreveritatisetquasiarchitectobeataevitaedictasuntexplicaboNemoenimipsamvoluptatemquiavoluptassitaspernaturautoditautfugitsedquiacon" } };

	@Test
	public void testEscapeStringAsFilenameString() {
		for (String[] testCase : TEST_CASES) {
			String text = testCase[0];
			String actual = FileUtils.escapeStringAsFilename(text);
			String expected = testCase[1];
			Assert.assertEquals(expected, actual);
		}
	}

	@Test
	public void testEscapeStringAsFilenamePatternStringStringInt() {
		Pattern POSIX_PATTERN = FileUtils.POSIX_PATTERN;
		for (String[] testCase : TEST_CASES) {
			String text = testCase[0];
			String actual = FileUtils.escapeStringAsFilename(POSIX_PATTERN, text, "", FileUtils.DEFAULT_LIMIT);
			String expected = testCase[1];
			Assert.assertEquals(expected, actual);
			String actualShort = FileUtils.escapeStringAsFilename(POSIX_PATTERN, text, "", 10);
			int len = Math.min(10, expected.length());
			String expectedShort = testCase[1].substring(0, len);
			Assert.assertEquals(expectedShort, actualShort);
		}
	}

	@Test
	public void testEscapeStringAsFilenamePatternStringInt() throws UnsupportedEncodingException {
		Pattern POSIX_PATTERN = FileUtils.POSIX_PATTERN;
		String text = "Test File Name";
		String expected = "Test%20File%20Name";
		String actual = FileUtils.escapeStringAsFilename(POSIX_PATTERN, text, FileUtils.DEFAULT_LIMIT);
		String decoded = URLDecoder.decode(actual, "UTF-8");
		Assert.assertEquals(expected, actual);
		Assert.assertEquals(text, decoded);
	}

	@Test
	public void testEscapeUmlaute() {
		String text = "K�se K�ln F��e �l �bel �� �� ��� � � � �BUNG";
		String actual = FileUtils.escapeUmlaute(text);
		String expected = "Kaese Koeln Fuesse Oel Uebel Aeue Uess AEOEUe Ae Oe Ue UEBUNG";
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testLoadPropertiesFromResource() {
		String name = "mail.properties.xml";
		Properties properties = FileUtils.loadPropertiesFromResource(name);
		Assert.assertNotNull(properties);
		String protocol = properties.getProperty("mail.store.protocol");
		Assert.assertNotNull(protocol);
		Assert.assertEquals("imaps", protocol);
	}

}
