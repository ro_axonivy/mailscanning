package ch.soreco.orderbook.mail.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.itextpdf.text.DocumentException;

public class PDFMergeTest {
	private static final Path MERGE_FILES_OF_DIR = Paths.get("C:/Office/zweiplus/App/test/pdf/input");

	@Test
	public void test() throws IOException, DocumentException {
		List<byte[]> pdfs = new ArrayList<byte[]>();
		try (DirectoryStream<Path> dir = Files.newDirectoryStream(MERGE_FILES_OF_DIR)) {
			Iterator<Path> iterator = dir.iterator();
			while (iterator.hasNext()) {
				Path file = iterator.next();
				byte[] content = Files.readAllBytes(file);
				pdfs.add(content); 
			}
		}
		File resultFile = MERGE_FILES_OF_DIR.resolve("../output/merged.pdf").toFile();
		Files.deleteIfExists(resultFile.toPath());
		FileOutputStream out = new FileOutputStream(resultFile, false);
		try {
			PDFMerge.doMerge("Dies ist ein Auftrag(PDF) welcher aus einer Email generiert wurde.", out, pdfs);
			Assert.assertTrue(resultFile.exists());			
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
