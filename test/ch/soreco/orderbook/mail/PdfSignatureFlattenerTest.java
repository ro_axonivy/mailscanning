package ch.soreco.orderbook.mail;

import static org.junit.Assert.fail;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.log.Logger;

import com.ulcjava.base.shared.internal.IOUtilities;

//@RunWith(PowerMockRunner.class)
//@PrepareForTest(Ivy.class)
public class PdfSignatureFlattenerTest {
	private static final String SIGNED_PDF = "resources/Antrag_Konsolidiert_eSignature_ausgefüllt_unterschrieben.pdf";
	private PdfSignatureFlattener flattener = new PdfSignatureFlattener();

	@Test
	public void testSignatureReport() throws IOException {
//		Logger logger = Logger.getLogger(Ivy.class);
//		mockStatic(Ivy.class);
//		when(Ivy.log()).thenReturn(logger);
		Path path = Paths.get(SIGNED_PDF);
		try (InputStream in = Files.newInputStream(path)) {
			String report = flattener.signatureReport(in);
			System.out.println(report);
		}
	}

	@Test
	public void testFlatten() throws IOException {
//		Logger logger = Logger.getLogger(Ivy.class);
//		mockStatic(Ivy.class);
//		when(Ivy.log()).thenReturn(logger);
		Path path = Paths.get(SIGNED_PDF);
		try (InputStream in = Files.newInputStream(path)) {
			byte[] content = flattener.flatten(in);
			Files.write(Files.createTempFile("FlattenerPdf_", ".pdf"), content); 
		}
	}

}
