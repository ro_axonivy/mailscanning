package ch.soreco.orderbook.mail.scanner;

import java.io.File;
import java.nio.file.Files;

import junit.framework.Assert;

import org.junit.Test;

import ch.soreco.orderbook.mail.MailScannerService;
import ch.soreco.orderbook.mail.domain.Mail;

public class MailScannerServiceIntegration2Test {

	private String username = "bzppsdev";
	private String password = "Welcome$1988";
	private Integer port = 993;
	private String serverAddress = "SWBNTSRV26.soreco.wan";//"SWBNTSRV26.soreco.wan";

	@Test
	public void test() throws Exception {
		MailScannerService.getInstance().connect(serverAddress, port, username, password);
		Integer mailCount = MailScannerService.getInstance().countMailsInInbox();
		System.out.println(String.format("count:%d, ", mailCount));
		File storeTo = Files.createTempDirectory("Mailing").toFile();
		for (int i = 0; i < mailCount; i++) {
			Mail mail = MailScannerService.getInstance().retrieveNextMail();
			Assert.assertNotNull(mail);
			//System.out.println("mail:" + mail.toJson(false)+",");
//			if(i>8){
				String messageNumber = (i<10? "0" : "") + String.valueOf(i);
				File temp = File.createTempFile("Mail_"+messageNumber+"_", ".ser", storeTo);
				mail.serializeTo(temp);
				System.out.println(temp.getAbsolutePath());
//			}

		}
	}
}
