package ch.soreco.orderbook.mail.scanner;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.soreco.orderbook.mail.MailScannerService;
import ch.soreco.orderbook.mail.exception.MailServiceException;

public class MailScannerServiceIntegrationTest {
	private static final String GROUP_EMAIL = "bzpps-test-group@soreco.ch";
	private static final String GROUP_NAME = "SORECO/bzppsdev/bzpps-test-group";
	private static final String USER_NAME = "bzppsdev";
	private static final String USER_PW = "Welcome$1988";
	private static final Integer IMAP_MAIL_PORT = 993;
	private static final String IMAP_MAIL_HOST = "SWBNTSRV26.soreco.wan";// "SWBNTSRV26.soreco.wan";
	private static final String SMTP_MAIL_HOST = "mail.soreco.ch";
	private static final String USER_EMAIL = "bzpps-test@soreco.ch";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	@Test
	public void testGroupMailBox() throws MailServiceException{
		Properties properties = MailScannerService.loadDefaultProperties();
		MailScannerService mailScannerService = MailScannerService.getInstance(properties);		
		mailScannerService.connect(IMAP_MAIL_HOST, IMAP_MAIL_PORT, GROUP_NAME, USER_PW);
		Integer mailsCount = mailScannerService.countMailsInInbox();
		Assert.assertNotNull("The count is expected to return a value", mailsCount);
		mailScannerService.disconnect();
		Assert.assertFalse("Service is disconnected", mailScannerService.isConnected());
	}

	@Test
	public void testCountMailsInInbox() throws MailServiceException {
		MailScannerService mailScannerService = MailScannerService.getInstance();
		mailScannerService.connect(IMAP_MAIL_HOST, IMAP_MAIL_PORT, USER_NAME, USER_PW);
		Integer mailsCount = mailScannerService.countMailsInInbox();
		Assert.assertNotNull("The count is expected to return a value", mailsCount);
		mailScannerService.disconnect();
		Assert.assertFalse("Service is disconnected", mailScannerService.isConnected());
		boolean sent = sendTestMailOverSMTP();
		Assert.assertTrue("Test Mail has been sent", sent);
		try {
			//
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mailScannerService.connect(IMAP_MAIL_HOST, IMAP_MAIL_PORT, USER_NAME, USER_PW);
		Integer newMailsCount = mailScannerService.countMailsInInbox();
		Assert.assertNotNull("The count is expected to return a value", newMailsCount);
		Integer expectedMailsCount = mailsCount + 1;
		Assert.assertEquals("After a new mail has been sent", expectedMailsCount, newMailsCount);
		mailScannerService.disconnect();
		Assert.assertFalse("Service is disconnected", mailScannerService.isConnected());
	}

	// @Test
	// public void testRetrieveNextMail() {
	// //TODO: fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testMarkMailAsProcessed() {
	// //TODO: fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testMarkMailAsFailed() {
	// TODO: fail("Not yet implemented");
	// }

	private boolean sendTestMailOverSMTP() {
		Properties properties = new Properties();
		// Setup mail server
		properties.setProperty("mail.smtp.host", SMTP_MAIL_HOST);
		// Get the default Session object.
		Session session = Session.getInstance(properties);
		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);
			// Set From: header field of the header.
			message.setFrom(new InternetAddress("mailscannerserviceintegrationtest@axonivy.com"));
			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(USER_EMAIL));

			// Set Subject: header field
			message.setSubject("This is the Subject Line!");
			// Now set the actual message
			message.setText("This is actual message");
			// Send message
			Transport.send(message);
			return true;
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		return false;
	}
}
