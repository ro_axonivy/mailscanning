package ch.soreco.orderbook.mail.scanner;

import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.Session;

import org.apache.commons.io.IOUtils;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.globalvars.IGlobalVariableContext;
import ch.ivyteam.log.Logger;
import ch.soreco.orderbook.bo.Attachment;
import ch.soreco.orderbook.bo.Order;
import ch.soreco.orderbook.bo.OrderMatching;
import ch.soreco.orderbook.mail.OrderAdapter;
import ch.soreco.orderbook.mail.domain.AbstractMailPart;
import ch.soreco.orderbook.mail.domain.Mail;
import ch.soreco.orderbook.mail.domain.MailPart;
import ch.soreco.webbies.common.db.BlobFile;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Ivy.class)
public class OrderAdapterTest {
	private static Path getResourcesFolder() {
		return Paths.get("resources").toFile().getAbsoluteFile().toPath();
	}

	@Test
	public void testToOrder() {
		Logger logger = Logger.getLogger(Ivy.class);
		mockStatic(Ivy.class);
		when(Ivy.log()).thenReturn(logger);
		IGlobalVariableContext context = mock(IGlobalVariableContext.class);
		when(context.get("MAIL_PDF_PAGE_HEAD")).thenReturn("Dies ist ein Auftrag welcher aus einer Email generiert wurde.");
		when(Ivy.var()).thenReturn(context);

		// Path root = Paths.get("C:/Users/bst/AppData/Local/Temp/Mailing2471664233187466672");
		// Path first = root.resolve("Mail_00_1932882717105399584.ser");
		// Path second = root.resolve("Mail_01_4597631820619773047.ser");
		// Path third = root.resolve("Mail_02_3022033401425976047.ser");
		// Path fourth = root.resolve("Mail_03_5616175976182559037.ser");
		// Path current = first;
		Mail mail = new Mail();
		mail.setSentDate(new Date());
		mail.setSubject("My Subject of mail");
		List<AbstractMailPart> parts = new ArrayList<AbstractMailPart>();
		MailPart bodyPart = new MailPart();
		String content = ("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head> <style><!-- /* Font Definitions */ @font-face 	{font-family:\"Cambria Math\"; 	panose-1:2 4 5 3 5 4 6 3 2 4;} @font-face 	{font-family:Calibri; 	panose-1:2 15 5 2 2 2 4 3 2 4;} /* Style Definitions */ p.MsoNormal, li.MsoNormal, div.MsoNormal 	{margin:0cm; 	margin-bottom:.0001pt; 	font-size:11.0pt; 	font-family:\"Calibri\",sans-serif; 	mso-fareast-language:EN-US;} a:link, span.MsoHyperlink 	{mso-style-priority:99; 	color:#0563C1; 	text-decoration:underline;} a:visited, span.MsoHyperlinkFollowed 	{mso-style-priority:99; 	color:#954F72; 	text-decoration:underline;} p.msonormal0, li.msonormal0, div.msonormal0 	{mso-style-name:msonormal; 	mso-margin-top-alt:auto; 	margin-right:0cm; 	mso-margin-bottom-alt:auto; 	margin-left:0cm; 	font-size:11.0pt; 	font-family:\"Calibri\",sans-serif;} span.E-MailFormatvorlage18 	{mso-style-type:personal; 	font-family:\"Calibri\",sans-serif; 	color:windowtext;} span.E-MailFormatvorlage19 	{mso-style-type:personal-reply; 	font-family:\"Calibri\",sans-serif; 	color:windowtext;} .MsoChpDefault 	{mso-style-type:export-only; 	font-size:10.0pt;} @page WordSection1 	{size:612.0pt 792.0pt; 	margin:70.85pt 70.85pt 2.0cm 70.85pt;} div.WordSection1 	{page:WordSection1;} --></style><!--[if gte mso 9]><xml><o:shapedefaults v:ext=\"edit\" spidmax=\"1026\" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\" /></o:shapelayout></xml><![endif]--> </head> <body lang=\"EN-GB\" link=\"#0563C1\" vlink=\"#954F72\"> <div class=\"WordSection1\"> <p class=\"MsoNormal\"><o:p>&nbsp;</o:p></p> <p class=\"MsoNormal\"><o:p>&nbsp;</o:p></p> <div> <div style=\"border:none;border-top:solid #E1E1E1 1.0pt;padding:3.0pt 0cm 0cm 0cm\"> <p class=\"MsoNormal\"><b><span lang=\"DE\" style=\"mso-fareast-language:EN-GB\">Von:</span></b><span lang=\"DE\" style=\"mso-fareast-language:EN-GB\"> Roberto Oliva <br> <b>Gesendet:</b> Montag, 15. Januar 2018 14:48<br> <b>An:</b> bzpps Test Mailbox &lt;bzpps-test@soreco.ch&gt;<br> <b>Betreff:</b> Testmail<o:p></o:p></span></p> </div> </div> <p class=\"MsoNormal\"><o:p>&nbsp;</o:p></p> <p class=\"MsoNormal\"><span lang=\"DE-CH\">Ich bin ein Testmail<o:p></o:p></span></p> <p class=\"MsoNormal\"><span lang=\"DE-CH\"><o:p>&nbsp;</o:p></span></p> </div>   </body></html>");
		bodyPart.setContent(content.getBytes());
		bodyPart.setContentType("text/html");
		parts.add(bodyPart);
		mail.setParts(parts);

		List<String> expectedFileNames = new ArrayList<String>();
		expectedFileNames.add(OrderAdapter.MAIL_AUFTRAG_PREFIX + "MySubjectofmail.pdf");// The Report of the incoming mail
		expectedFileNames.add(OrderAdapter.MAIL_CONTENT_PREFIX + "MySubjectofmail.pdf");// The content of the actual mail

		try {
			// Mail mail = Mail.deserializeFrom(current.toFile());
			MailPart tiffPart = toMailPart("Testfax.tif", "image/tiff");
			mail.getParts().add(tiffPart);
			expectedFileNames.add(tiffPart.getFileName() + ".pdf");

			// the signed and unflattened pdf file
			MailPart mailPartPdf = toMailPart("Antrag_Konsolidiert_eSignature_ausgefüllt_unterschrieben.pdf", "text/pdf");
			mail.getParts().add(mailPartPdf);
			expectedFileNames.add(mailPartPdf.getFileName());

			// the oversized pdf file
			MailPart mailPartOversizedPdf = toMailPart("Checklist for a child.pdf", "text/pdf");
			mail.getParts().add(mailPartOversizedPdf);
			expectedFileNames.add(mailPartOversizedPdf.getFileName());

			// changing orientation pdf but all A4
			MailPart differentOrientationPdf = toMailPart("Newsletter.pdf", "text/pdf");
			mail.getParts().add(differentOrientationPdf);
			expectedFileNames.add(differentOrientationPdf.getFileName());

			Order order = OrderAdapter.toOrder(mail, false, false);
			BlobFile blobFile = order.getScanning().getScanBlobFile();
			byte[] orderFile = blobFile.getBytes();
			Path folder = Files.createTempDirectory("OrderBlobs_");
			if (orderFile != null) {
				Path temp = folder.resolve(blobFile.getFileName().replace(":", " "));
				try (InputStream in = new ByteArrayInputStream(orderFile)) {
					Files.copy(in, temp, StandardCopyOption.REPLACE_EXISTING);
				}
			}
			for (Attachment attachment : order.getScanning().getAttachments()) {
				BlobFile attachmentFile = attachment.getFile().getBlobFile();
				if (attachmentFile != null) {
					Path temp = folder.resolve(attachmentFile.getFileName().replace(":", " "));
					try (InputStream in = new ByteArrayInputStream(attachmentFile.getBytes())) {
						Files.copy(in, temp, StandardCopyOption.REPLACE_EXISTING);
					}
				}
			}
			System.out
					.println("Check the files in that folder - whether the conversion of mail body, attachments and mail body was successful.\n"
							+ folder.toString());

			System.out.println("Check whether the order would be correct:\n" + order.toString());
			boolean actual = Files.exists(folder);

			// TODO: test for different mail cases!
			Assert.assertEquals("Whether the folder exists!", true, actual);
			String[] fileList = folder.toFile().list();
			// FIXME: assert the expected files
			// Assert.assertArrayEquals("Whether the folder contains all expected Filenames ", expectedFileNames.toArray(), fileList);
		} catch (IOException e) {
			fail(e.getMessage());
		}

	}

	private MailPart toMailPart(String fileName, String contentType) throws FileNotFoundException, IOException {
		File file = getResourcesFolder().resolve(fileName).toFile();

		MailPart mailPart = new MailPart();
		mailPart.setContentType(contentType);
		mailPart.setDisposition("attachment");
		mailPart.setFileName(fileName);// will become Fax.tif.pdf

		FileInputStream fis = new FileInputStream(file);
		byte[] content = IOUtils.toByteArray(fis);
		mailPart.setContent(content);

		return mailPart;
	}

}
