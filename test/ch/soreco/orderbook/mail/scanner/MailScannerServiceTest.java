package ch.soreco.orderbook.mail.scanner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Session;
import javax.mail.Store;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ch.soreco.orderbook.mail.MailScannerService;
import ch.soreco.orderbook.mail.Pop3sMailService;
import ch.soreco.orderbook.mail.exception.MailServiceException;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Session.class)
public class MailScannerServiceTest {

	private MailScannerService mailScannerService;

	@Mock
	private Store store;

	@Mock
	private Folder inboxFolder;

	@Mock
	private Folder processedMailsFolder;

	@Mock
	private Session session;

	@Before
	public void setUp() throws MailServiceException {
		PowerMockito.mockStatic(Session.class);
		PowerMockito.when(Session.getDefaultInstance(Mockito.any(Properties.class))).thenReturn(session);
		mailScannerService = MailScannerService.getInstance();
	}

	@Test
	public void testGetInstance() throws MailServiceException {
		MailScannerService instance = MailScannerService.getInstance();
		assertNotNull(instance);
	}

	@Test
	public void testConnectWithUnexistingProcessedMailsFolder() throws Exception {
		when(session.getStore()).thenReturn(store);
		when(store.getFolder("Inbox")).thenReturn(inboxFolder);
		when(store.getFolder("Processed Mails")).thenReturn(processedMailsFolder);
		when(processedMailsFolder.exists()).thenReturn(false);

		String username = "bzppsdev";
		String password = "";
		Integer port = 993;
		String serverAddress = "SWBNTSRV26.soreco.wan";
		MailScannerService.getInstance().connect(serverAddress, port, username, password);

		Mockito.verify(session).getStore();
		Mockito.verify(store).connect(Mockito.anyString(), Mockito.anyString());
		Mockito.verify(store).getFolder("Inbox");
		Mockito.verify(store).getFolder("Processed Mails");
		Mockito.verify(inboxFolder).open(Folder.READ_WRITE);
		Mockito.verify(processedMailsFolder).exists();
		Mockito.verify(processedMailsFolder).create(Folder.HOLDS_MESSAGES);
		Mockito.verify(processedMailsFolder).open(Folder.READ_WRITE);

		Mockito.verifyNoMoreInteractions(store, session, inboxFolder, processedMailsFolder);
	}

	@Test
	public void testConnectWithExistingProcessedMailsFolder() throws Exception {
		when(session.getStore()).thenReturn(store);
		when(store.getFolder("Inbox")).thenReturn(inboxFolder);
		when(store.getFolder("Processed Mails")).thenReturn(processedMailsFolder);
		when(processedMailsFolder.exists()).thenReturn(true);

		String username = "bzppsdev";
		String password = "Welcome$1988";
		Integer port = 25;
		String serverAddress = "SWBNTSRV26.soreco.wan";
		MailScannerService.getInstance().connect(serverAddress, port, username, password);

		Mockito.verify(session).getStore();
		Mockito.verify(store).connect(Mockito.anyString(), Mockito.anyString());
		Mockito.verify(store).getFolder("Inbox");
		Mockito.verify(store).getFolder("Processed Mails");
		Mockito.verify(inboxFolder).open(Folder.READ_WRITE);
		Mockito.verify(processedMailsFolder).exists();
		Mockito.verify(processedMailsFolder).open(Folder.READ_WRITE);

		Mockito.verifyNoMoreInteractions(store, session, inboxFolder, processedMailsFolder);
	}

	@Test(expected = MailServiceException.class)
	public void testConnectWithNullValues() throws MailServiceException {
		mailScannerService.connect(null, null, null, null);
	}

	@Test
	public void testDisconnect() throws Exception {
		Pop3sMailService exchangeService = Mockito.mock(Pop3sMailService.class);
		MailScannerService mailScannerServiceNow = MailScannerService.getInstance();
		PowerMockito.whenNew(Pop3sMailService.class).withNoArguments().thenReturn(exchangeService);
		mailScannerServiceNow.disconnect();
		MailScannerService mailScannerServiceThen = MailScannerService.getInstance();
		Assert.assertNotSame(mailScannerServiceNow, mailScannerServiceThen);
	}

	@Test
	public void testCountUnreadMail() {
		fail("Not yet implemented");
	}

	@Test
	public void testRetrieveNextMail() {
		fail("Not yet implemented");
	}

	@Test
	public void testMarkLastMailAsRead() {
		fail("Not yet implemented");
	}

}
