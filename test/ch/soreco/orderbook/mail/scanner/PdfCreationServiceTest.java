package ch.soreco.orderbook.mail.scanner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.soreco.orderbook.mail.PdfCreationService;
import ch.soreco.orderbook.mail.pdf.HtmlUtils;
import ch.soreco.utilities.file.FileUtils;

public class PdfCreationServiceTest {
	private static final String[] TIFF_FILES = new String[] { "04_Test_eMail_FaxMail2Ivy_TIFF.tif", "Testfax.tif" };
	private PdfCreationService pdfCreationService;

	@Before
	public void setUp() throws Exception {
		pdfCreationService = new PdfCreationService();
	}

	@Test
	public void testGeneratePdfFromTiff() throws Exception {
		for (String tiffFile : TIFF_FILES) {
			URL source = FileUtils.getResource(tiffFile);
			ByteArrayOutputStream tiffStream = new ByteArrayOutputStream();
			IOUtils.copy(source.openStream(), tiffStream);

			byte[] content = tiffStream.toByteArray();
			Path target = Files.createTempFile("GeneratedPDFFrom_" + tiffFile, ".pdf");
			try (FileOutputStream out = new FileOutputStream(target.toFile())) {
				pdfCreationService.generatePdfFromTiff(out, content);
			}
			System.out.println("Generated tif into:" + target);
			boolean condition = Files.exists(target);
			Assert.assertTrue("The generated pdf file", condition);
		}
		
	}

	@Test
	public void testGeneratePdfFromPlainText() throws Exception {
		byte[] content = "Test Plain Text PDF\nThis is a plain text with some specials characters - like Chuchichäschtli and ördenliche $."
				.getBytes();
		Path target = Files.createTempFile("GeneratedPDFFrom_PlainText", ".pdf");
		try (FileOutputStream out = new FileOutputStream(target.toFile())) {
			pdfCreationService.generatePdfFromPlainText(out, content);
		}
		System.out.println("Generated tif into:" + target);
		boolean condition = Files.exists(target);
		Assert.assertTrue("The generated pdf file", condition);
	}

	@Test
	public void testGeneratePdfFromHtml() throws Exception {
		// Tif root: "C:/Office/zweiplus/BZP_Desk/Prescanning/Testfax.tif"
		Path root = Paths.get("resources/itext/");
		File source = root.resolve("itext.sample.html").toFile();
		Map<String, byte[]> images = loadImagesOf(root, root);
		String baseUri = root.toUri().toString();
		File file = File.createTempFile("GeneratedPDFFrom_Html", ".pdf");
		FileInputStream fis = new FileInputStream(source);
		byte[] tidySource = HtmlUtils.tidyUp(fis, null, baseUri);
		fis.close();
		ByteArrayInputStream inTidySource = new ByteArrayInputStream(tidySource);
		OutputStream out = new FileOutputStream(file);
		pdfCreationService.createXHtmlPdf(out, inTidySource, images);
		inTidySource.close();
		out.close();
		System.out.println(file.getAbsolutePath());

	}

	private static Map<String, byte[]> loadImagesOf(Path root, Path dir) throws IOException {
		Map<String, byte[]> images = new HashMap<String, byte[]>();
		try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(dir)) {
			for (Path file : dirStream) {
				boolean isDir = Files.isDirectory(file);
				if (isDir) {
					Map<String, byte[]> subImages = loadImagesOf(root, file);
					images.putAll(subImages);
				} else {
					String relative = root.relativize(file).toString();
					FileInputStream fis = new FileInputStream(file.toFile());
					byte[] bytes = IOUtils.toByteArray(fis);
					images.put(relative, bytes);
				}
			}
		}
		return images;
	}

}
