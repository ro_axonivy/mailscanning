package com.axonivy.util.http;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.request.IHttpRequest;

public class JSoupUtils {
	/**
	 * Returns the JSESSIONID cookie as currently executed by process request. Returns null if not available.
	 * 
	 * @return the JSESSIONID cookie as currently executed by process request or null if not available.
	 * @throws EnvironmentNotAvailableException
	 *             if accessing this method outside a request thread
	 */
	public static Cookie getSessionCookie() {
		IHttpRequest httpRequest = (IHttpRequest) Ivy.request();
		Cookie jsessionid = null;
		if (httpRequest != null) {
			HttpServletRequest servletRequest = httpRequest.getHttpServletRequest();
			Cookie[] cookies = servletRequest.getCookies();
			for (Cookie cookie : cookies) {
				String cookieName = cookie.getName();
				if ("JSESSIONID".equals(cookieName)) {
					jsessionid = cookie;
					break;
				}
			}
		}
		return jsessionid;
	}

	/**
	 * Read an input stream, and parse it to a (tidy) String.
	 * 
	 * @param in
	 *            input stream to read. Make sure to close it after parsing.
	 * 
	 * @param charsetName
	 *            (optional) character set of file contents. Set to null to determine from http-equiv meta tag, if
	 *            present, or fall back to UTF-8 (which is often safe to do).
	 * 
	 * @param baseUri
	 *            The URL where the HTML was retrieved from, to resolve relative links against.
	 * @return sane HTML
	 * @throws IOException
	 */
	public static String parseToString(InputStream in, String charsetName, String baseUri) throws IOException {
		Document document = Jsoup.parse(in, charsetName, baseUri);
		String html = document.html();
		return html;
	}

	/**
	 * Read an input stream, and parse it to a (tidy) String.
	 * 
	 * @param html
	 *            HTML to parse.
	 * @param baseUri
	 *            The URL where the HTML was retrieved from, to resolve relative links against.
	 * @return sane HTML
	 * @throws IOException
	 */
	public static String parseToString(String html) throws IOException {
		Document document = Jsoup.parse(html);
		String parsedHtml = document.html();
		return parsedHtml;
	}
	/**
	 * Read an input stream, and parse it to a (tidy) String.
	 * 
	 * @param html
	 *            HTML to parse.
	 * @param charsetName 
	 * @param baseUri
	 *            The URL where the HTML was retrieved from, to resolve relative links against.
	 * @return sane HTML
	 * @throws IOException
	 */
	public static String parseToString(File html, String charsetName) throws IOException {
		Document document = Jsoup.parse(html, charsetName);
		String parsedHtml = document.html();
		return parsedHtml;
	}

	/**
	 * Creates a new Connection to a URL, executes the request as a POST, and parses the result.
	 * 
	 * @param url
	 *            URL to connect to. The protocol must be http or https.
	 * @param cookies
	 *            (optional) map of cookie name - value pairs
	 * @param data
	 *            (optional) map of data parameters
	 * @return parsed Document
	 * @throws java.net.MalformedURLException
	 *             if the request URL is not a HTTP or HTTPS URL, or is otherwise malformed
	 * @throws HttpStatusException
	 *             if the response is not OK and HTTP response errors are not ignored
	 * @throws UnsupportedMimeTypeException
	 *             if the response mime type is not supported and those errors are not ignored
	 * @throws java.net.SocketTimeoutException
	 *             if the connection times out
	 * @throws IOException
	 *             on error
	 */
	public static Document post(String url, Map<String, String> cookies, Map<String, String> data) throws IOException {
		Connection connection = Jsoup.connect(url);
		if (cookies != null) {
			connection.cookies(cookies);
		}
		if (data != null) {
			connection.data(data);
		}
		Document doc = connection.post();
		return doc;
	}

	/**
	 * Creates a new Connection to a URL, executes the request as a POST, and parses the result.
	 * 
	 * @param url
	 *            URL to connect to. The protocol must be http or https.
	 * @param cookies
	 *            (optional) map of cookie name - value pairs
	 * @param data
	 *            (optional) map of data parameters
	 * @return parsed Document
	 * @throws java.net.MalformedURLException
	 *             if the request URL is not a HTTP or HTTPS URL, or is otherwise malformed
	 * @throws HttpStatusException
	 *             if the response is not OK and HTTP response errors are not ignored
	 * @throws UnsupportedMimeTypeException
	 *             if the response mime type is not supported and those errors are not ignored
	 * @throws java.net.SocketTimeoutException
	 *             if the connection times out
	 * @throws IOException
	 *             on error
	 */
	public static String getToString(String url, boolean useSessionCookie) throws IOException {
		Document doc = get(url, useSessionCookie);
		return doc.html();
	}

	/**
	 * Creates a new Connection to a URL, executes the request as a POST, and parses the result.
	 * 
	 * @param url
	 *            URL to connect to. The protocol must be http or https.
	 * @param cookies
	 *            (optional) map of cookie name - value pairs
	 * @param data
	 *            (optional) map of data parameters
	 * @return parsed Document
	 * @throws java.net.MalformedURLException
	 *             if the request URL is not a HTTP or HTTPS URL, or is otherwise malformed
	 * @throws HttpStatusException
	 *             if the response is not OK and HTTP response errors are not ignored
	 * @throws UnsupportedMimeTypeException
	 *             if the response mime type is not supported and those errors are not ignored
	 * @throws java.net.SocketTimeoutException
	 *             if the connection times out
	 * @throws IOException
	 *             on error
	 */
	public static Document get(String url, boolean useSessionCookie) throws IOException {
		Connection connection = Jsoup.connect(url);
		if (useSessionCookie) {
			Cookie cookie = getSessionCookie();
			if (cookie != null) {
				connection.cookie(cookie.getName(), cookie.getValue());
			}
		}
		Document doc = connection.get();
		return doc;
	}

	/**
	 * Get safe HTML from untrusted input HTML, by parsing input HTML and filtering it through a white-list of permitted
	 * tags and attributes. The used {@link Whitelist#basicWithImages()} allows a fuller range of text nodes:
	 * <code>a, b, blockquote, br, cite, code, dd, dl, dt, em, i, li, ol,
	 * p, pre, q, small, strike, strong, sub, sup, u, ul, img</code> and appropriate attributes.
	 * 
	 * Links (a elements) can point to http, https, ftp, mailto, and have an enforced rel=nofollow attribute.
	 * 
	 * @param bodyHtml
	 *            input untrusted HTML (body fragment)
	 * @param baseUri
	 *            URL to resolve relative URLs against
	 * @return safe HTML (body fragment)
	 */
	public static String cleanBasicWithImages(String bodyHtml, String baseUri) {
		Whitelist whitelist = Whitelist.basicWithImages();
		String bodyFragment = Jsoup.clean(bodyHtml, baseUri, whitelist);
		return bodyFragment;
	}
}
