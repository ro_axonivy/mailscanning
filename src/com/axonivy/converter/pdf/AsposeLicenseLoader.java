package com.axonivy.converter.pdf;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import ch.ivyteam.ivy.ThirdPartyLicenses;
import ch.ivyteam.ivy.environment.Ivy;

public class AsposeLicenseLoader {
	private static AsposeLicenseLoader loader;
	private com.aspose.pdf.License pdfLicence;
	private com.aspose.words.License wordsLicense;

	private AsposeLicenseLoader() {

	}

	public static AsposeLicenseLoader getInstance() {
		if (loader == null) {
			loader = new AsposeLicenseLoader();
		}
		return loader;
	}

	private void initPdfLicense() {
		if (pdfLicence == null) {
			pdfLicence = new com.aspose.pdf.License();
			InputStream in = null;
			try {
				in = ThirdPartyLicenses.getDocumentFactoryLicense();
				pdfLicence.setLicense(in);
			} catch (Exception e) {
				Ivy.log().error("Aspose Licence error:{0}", e, e.getMessage());
			} finally {
				if (in != null) {
					IOUtils.closeQuietly(in);
				}
			}
		}

	}
	private void initWordLicense() {
		if (wordsLicense == null) {
			wordsLicense = new com.aspose.words.License();
			InputStream in = null;
			try {
				in = ThirdPartyLicenses.getDocumentFactoryLicense();
				wordsLicense.setLicense(in);
			} catch (Exception e) {
				Ivy.log().error("Aspose Licence error:{0}", e, e.getMessage());
			} finally {
				if (in != null) {
					IOUtils.closeQuietly(in);
				}
			}
		}

	}
	/**
	 * Loads the Aspose Licenses.
	 */
	public static void loadPdfLicense() {
		getInstance().initPdfLicense();
	}
	/**
	 * Loads the Aspose Licenses.
	 */
	public static void loadWordLicense() {
		getInstance().initWordLicense();
	}

}
