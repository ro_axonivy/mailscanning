package com.axonivy.converter.pdf;

public abstract class WarningHandler {
	public abstract void warn(int source, int warningType, String description);
}
