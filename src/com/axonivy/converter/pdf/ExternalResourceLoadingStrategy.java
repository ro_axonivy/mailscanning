package com.axonivy.converter.pdf;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import ch.ivyteam.ivy.environment.Ivy;

import com.aspose.pdf.LoadOptions;
import com.aspose.pdf.LoadOptions.ResourceLoadingResult;
import com.aspose.pdf.LoadOptions.ResourceLoadingStrategy;

public class ExternalResourceLoadingStrategy implements ResourceLoadingStrategy {
	private Map<String, byte[]> resources;
	private ResourceLoadingResult defaultResult;

	public ExternalResourceLoadingStrategy(Map<String, byte[]> resources) {
		this.resources = resources;
		defaultResult = new LoadOptions.ResourceLoadingResult(new byte[0]);
		defaultResult.LoadingCancelled = true;
	}

	/**
	 * Sometimes it's necessary to avoid usage of internal loader of external resources(like images or CSSes) and supply custom method, that
	 * will get requested resources from somewhere. For example during usage of Aspose.Pdf in cloud direct access to referenced files
	 * impossible, and some custom code put into special method should be used. This delegate defines signature of such custom method.
	 */
	@Override
	public ResourceLoadingResult invoke(String resourceURI) {
		if(StringUtils.isNotEmpty(resourceURI)){
			byte[] resultBytes = resources.get(resourceURI);
			if (resultBytes != null) {
				LoadOptions.ResourceLoadingResult result = new LoadOptions.ResourceLoadingResult(resultBytes);
				Ivy.log().debug("Got Preloaded Resource for uri: {0}", resourceURI);
				return result;
			} else {
				String resourceKey = resourceURI.toLowerCase();
				Entry<String, byte[]> resourceEntry = resources.entrySet().stream()
						.filter(res -> res.getKey().toLowerCase().contains(resourceKey) || resourceKey.contains(res.getKey().toLowerCase()))
						.findFirst().orElse(null);
				if (resourceEntry != null && resourceEntry.getValue() != null) {
					Ivy.log().debug("Found Preloaded Resource for uri: {0}", resourceURI);
					return new LoadOptions.ResourceLoadingResult(resourceEntry.getValue());
				}
			}			
		}
		Ivy.log().debug("Did not find preloaded resource for uri: {0}", resourceURI);

		return defaultResult;
	}

}
