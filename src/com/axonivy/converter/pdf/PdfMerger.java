package com.axonivy.converter.pdf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import ch.ivyteam.ivy.environment.Ivy;
import ch.soreco.orderbook.mail.OrderAdapter;
import ch.soreco.webbies.common.db.BlobFile;

import com.aspose.pdf.Document;
import com.aspose.pdf.Font;
import com.aspose.pdf.FontRepository;
import com.aspose.pdf.HorizontalAlignment;
import com.aspose.pdf.Page;
import com.aspose.pdf.PageCollection;
import com.aspose.pdf.PageSize;
import com.aspose.pdf.PdfSaveOptions;
import com.aspose.pdf.Rectangle;
import com.aspose.pdf.Rotation;
import com.aspose.pdf.TextState;
import com.aspose.pdf.facades.PdfFileEditor;
import com.aspose.pdf.facades.PdfPageEditor;
import com.itextpdf.text.DocumentException;

public class PdfMerger {
	private Font fontArial;
	private static final Long A4LSWidth = ((Float) PageSize.getA4().getWidth()).longValue();
	private static final Long A4LSHeight = ((Float) PageSize.getA4().getHeight()).longValue();

	public PdfMerger() {
		AsposeLicenseLoader.loadPdfLicense();
	}
	public void merge(String header, OutputStream out, List<BlobFile> pdfs) throws DocumentException {

		// create PdfFileEditor object
		PdfFileEditor editor = new PdfFileEditor();
		Document concatenatedPdf = new Document();
		Document[] sourcePDFs = new Document[pdfs.size()];

		// convert's the pdfs into an array of (pdf) documents
		int pageSize = 0;
		for (int i = 0; i < pdfs.size(); i++) {
			BlobFile blobFile = pdfs.get(i);
			ByteArrayInputStream source = new ByteArrayInputStream(blobFile.getBytes());
			Document documentToCopy = new Document(source);

			// removes the certificates, signatures and disables form fields.
			documentToCopy.flatten();

			PageCollection pages = documentToCopy.getPages();
			if (!blobFile.getFileName().startsWith(OrderAdapter.MAIL_REPORT_PREFIX)) {
				// the first page contains the report pdf, which isn't included in page count.
				pageSize += pages.size();
			}
			sourcePDFs[i] = documentToCopy;
			boolean hasOversizedPages = false;
			for (int pageNum = 1; pageNum <= pages.size(); pageNum++) {
				Page page = pages.get_Item(pageNum);
				Rectangle rect = page.getRect();

				Long width = ((Double) rect.getWidth()).longValue();
				Long height = ((Double) rect.getHeight()).longValue();

				if (width > height) {
					// flip the values to cover portrait orientation
					width = height;
					height = ((Double) rect.getWidth()).longValue();
				}
				if (A4LSWidth < width || A4LSHeight < height) {
					hasOversizedPages = true;
					System.out.println(String.format(
							"Greater width/height for %s %d width is A4: %s actual: %s, height is A4: %s actual: %s)",
							blobFile.getFileName(), pageNum, String.valueOf(A4LSWidth), String.valueOf(width), String.valueOf(A4LSHeight),
							String.valueOf(height)));
				}
			}

			// Can only zoom the document (not the page)
			if (hasOversizedPages && pages.size() == 1) {
				try {
					PdfPageEditor ppe = new PdfPageEditor();
					ppe.bindPdf(documentToCopy);
					Rectangle rect = documentToCopy.getPages().get_Item(1).getRect();
					float pagesWidth = ((Double) rect.getWidth()).floatValue();
					float pagesHeight = ((Double) rect.getHeight()).floatValue();
					if (pagesWidth > pagesHeight) {
						//flip the values will rotate later the actual page.
						pagesWidth = ((Double) rect.getHeight()).floatValue();
						pagesHeight = pagesWidth;						
					}
					//the lesser the zoom the more content is fit into the viewport. Means that pages of A3 are zoomed 70% to fit into the A4 view port.
					float zoomfactor = Math.min((A4LSWidth / pagesWidth), (A4LSHeight / pagesHeight));
					ppe.setZoom(zoomfactor);
					System.out.println(String.format("Set zoomfactor %s (width %s, height %s) for document %s", String.valueOf(zoomfactor),
							String.valueOf(pagesWidth), String.valueOf(pagesHeight), blobFile.getFileName()));

					ByteArrayOutputStream zoomedPdf = new ByteArrayOutputStream();
					ppe.save(zoomedPdf);
					ByteArrayInputStream newZoomedPdf = new ByteArrayInputStream(zoomedPdf.toByteArray());
					documentToCopy = new Document(newZoomedPdf);
				} catch (Exception e) {
					e.printStackTrace();
					// if something fails we still have a valid (but oversized) pdf!
					Ivy.log().error("Could not zoom PDF {0}", e, blobFile.getFileName());
				}
			} else {
				System.out.println(String.format("Omitted set zoom since no oversized pages are found in document %s.",
						blobFile.getFileName()));
			}
		}

		editor.concatenate(sourcePDFs, concatenatedPdf);

		// Save the concatenated output file (the target document)
		PdfSaveOptions saveOptions = new PdfSaveOptions();
		// Flush to new pdf
		ByteArrayOutputStream concatenatedPdfOut = new ByteArrayOutputStream();
		concatenatedPdf.save(concatenatedPdfOut, saveOptions);
		InputStream concatenatedPdfIn = new ByteArrayInputStream(concatenatedPdfOut.toByteArray());

		// as the concatenation has been done we add the headers
		Document targetPdf = new Document(concatenatedPdfIn);
		int currentPageNum = 1;
		int targetPageNum = 1;
		for (int i = 0; i < pdfs.size(); i++) {
			BlobFile blobFile = pdfs.get(i);
			Document documentToCopy = sourcePDFs[i];
			PageCollection sourcePages = documentToCopy.getPages();
			for (int pageNum = 1; pageNum <= sourcePages.size(); pageNum++) {
				Page targetPage = targetPdf.getPages().get_Item(targetPageNum);
				Rectangle rect = targetPage.getRect();
				double width = rect.getWidth();
				double height = rect.getHeight();

				if (width > height) {
					// as we going rotate the page we switch the height and width to vice versa
					targetPage.setRotate(Rotation.on270);
					System.out.println("Rotated page (" + String.valueOf(targetPageNum) + "):{rect:{x:" + width + ",y:" + height + "}");
					// whether the page is wider than A4 width
					if (height < 595 || height > 598) {
						double newHeight = A4LSWidth.doubleValue();
						// calculate the new width proportionally to the change of height
						double newWidth = width * (newHeight / height);
						System.out.println("Cropped page (" + String.valueOf(targetPageNum) + ") by height to: " + newWidth + ","
								+ newHeight);
						targetPage.setPageSize(newWidth, newHeight);
					}
				} else {
					// whether the page is wider than A4 width
					if (width < 595 || width > 598) {
						double newWidth = A4LSWidth.doubleValue();
						// calculate the new height proportionally to the change of width
						double newHeight = height * (newWidth / width);
						System.out.println("Cropped page (" + String.valueOf(targetPageNum) + ") by width to : " + newWidth + ","
								+ newHeight);
						targetPage.setPageSize(newWidth, newHeight);
					}
				}

				if (!blobFile.getFileName().startsWith(OrderAdapter.MAIL_REPORT_PREFIX)) {
					String subTitle = blobFile.getOriginalFileName() != null ? "Konvertiert von " + blobFile.getOriginalFileName() : null;
					setHeaderText(targetPage, header, subTitle, currentPageNum, pageSize);
					currentPageNum++;
				} else {
					setHeaderText(targetPage, header, null, null, null);
				}
				targetPageNum++;
			}
		}
		// Save the to output file (the target document)
		targetPdf.save(out, saveOptions);
	}

	private void setHeaderText(Page page, String title, String subTitle, Integer pageNum, Integer totalPageCount) {
		if (page != null) {
			// FIXME: loaded local fonts by hard path due to Win10 incompatibility of old Aspos lib.
			String path = "c:/Windows/Fonts/";
			com.aspose.pdf.Document.addLocalFontPath(path);

			com.aspose.pdf.TextStamp titleStamp = new com.aspose.pdf.TextStamp(title);
			titleStamp.setVerticalAlignment(com.aspose.pdf.VerticalAlignment.Top);
			titleStamp.setHorizontalAlignment(com.aspose.pdf.HorizontalAlignment.Left);
			titleStamp.setTopMargin(5);
			titleStamp.setLeftMargin(5);
			setDefaultTextProperties(titleStamp.getTextState());
			page.addStamp(titleStamp);

			String pageNumber = "";
			if (pageNum != null && totalPageCount != null) {
				pageNumber = String.format("Seite %d von %d", pageNum, totalPageCount);
				com.aspose.pdf.TextStamp pageNumberStamp = new com.aspose.pdf.TextStamp(pageNumber);
				pageNumberStamp.setVerticalAlignment(com.aspose.pdf.VerticalAlignment.Top);
				pageNumberStamp.setHorizontalAlignment(com.aspose.pdf.HorizontalAlignment.Right);
				pageNumberStamp.setTopMargin(5);
				pageNumberStamp.setRightMargin(5);
				setDefaultTextProperties(pageNumberStamp.getTextState());
				page.addStamp(pageNumberStamp);
			}
			if (subTitle != null && subTitle.length() > 0) {
				com.aspose.pdf.TextStamp subTitleStamp = new com.aspose.pdf.TextStamp(subTitle);
				subTitleStamp.setVerticalAlignment(com.aspose.pdf.VerticalAlignment.Top);
				subTitleStamp.setHorizontalAlignment(com.aspose.pdf.HorizontalAlignment.Left);
				subTitleStamp.setTopMargin(18);
				subTitleStamp.setLeftMargin(5);
				setDefaultTextProperties(subTitleStamp.getTextState());
				page.addStamp(subTitleStamp);
			}
		}
	}
	private void setDefaultTextProperties(TextState textState) {
		if (fontArial == null) {
			fontArial = FontRepository.findFont("Arial");
		}
		if (fontArial != null) {
			textState.setFont(fontArial);
		}
		textState.setFontSize(9.0F);
		textState.setHorizontalAlignment(HorizontalAlignment.Left);
	}
}
