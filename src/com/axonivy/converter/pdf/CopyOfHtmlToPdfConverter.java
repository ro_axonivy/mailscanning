package com.axonivy.converter.pdf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import ch.soreco.orderbook.mail.PdfCreationService;

import com.aspose.pdf.Document;
import com.aspose.pdf.FileSpecification;
import com.aspose.pdf.Font;
import com.aspose.pdf.FontRepository;
import com.aspose.pdf.FontStyles;
import com.aspose.pdf.TextState;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

public class CopyOfHtmlToPdfConverter {
	private PdfCreationService pdfCreationService;

	// private Font fontArial;
	public CopyOfHtmlToPdfConverter() {
		pdfCreationService = new PdfCreationService();
		// AsposeLicenseLoader.loadPdfLicense();
	}

	public void convert(Path source, Path target) throws Exception {
		// Document pdfDocument = null;
		OutputStream out = new FileOutputStream(target.toFile());
		try (InputStream inputStream = new FileInputStream(source.toFile());) {
			Map<String, byte[]> images = new HashMap<String, byte[]>();
			pdfCreationService.createXHtmlPdf(out, inputStream, images);
			// pdfDocument = new Document(target.toString());
		} finally {
			if (out != null) {
				IOUtils.closeQuietly(out);
			}
		}
		// return pdfDocument;
	}

	// public Document open(Path source) {
	// // open document
	// Document pdfDocument = new Document(source.toString());
	//
	// return pdfDocument;
	// }

	// private Document open(InputStream source, LoadOptions loadOptions) {
	// // open document
	// Document pdfDocument = new Document(source, loadOptions);
	//
	// return pdfDocument;
	// }
	// private Document convert(URI source, LoadOptions loadOptions, Path
	// target) throws IOException {
	// Document pdfDocument = null;
	// URL url = source.toURL();
	// try (InputStream inputStream = url.openStream()) {
	// pdfDocument = open(inputStream, loadOptions);
	// // Save HTML file
	// pdfDocument.save(target.toString());
	// } catch (IOException e) {
	// e.printStackTrace();
	// throw e;
	// }
	// return pdfDocument;
	// }

	// private void convert(InputStream inputStream, String basePath, Path
	// target) throws IOException {
	// HtmlLoadOptions loadOptions = new HtmlLoadOptions(basePath);
	// loadOptions.setUseNewConversionEngine(true);
	// Document pdfDocument = open(inputStream, loadOptions);
	// // Save HTML file
	// pdfDocument.save(target.toString());
	// }

	public void attach(Path source, String sourceDescription, Document pdfDocument) {
		// setup new file to be added as attachment
		FileSpecification fileSpecification = new FileSpecification(source.toString(), sourceDescription);
		// add attachment to document's attachment collection
		pdfDocument.getEmbeddedFiles().add(fileSpecification);
	}

	// private void setHeaderText(Document pdfDocument, String text) {
	// // create text stamp
	// TextStamp textStamp = new TextStamp(text);
	// // set properties of the stamp
	// textStamp.setTopMargin(10);
	// textStamp.setHorizontalAlignment(com.aspose.pdf.HorizontalAlignment.Left);
	// textStamp.setVerticalAlignment(com.aspose.pdf.VerticalAlignment.Top);
	// textStamp.setLeftMargin(10);
	// // set text properties
	// setDefaultTextProperties(textStamp.getTextState());
	//
	// // create page number stamp
	// PageNumberStamp pageNumberStamp = new PageNumberStamp();
	// // whether the stamp is background
	// pageNumberStamp.setBackground(false);
	// pageNumberStamp.setFormat("Seite # von " +
	// pdfDocument.getPages().size());
	// pageNumberStamp.setBottomMargin(10);
	// pageNumberStamp.setRightMargin(10);
	// pageNumberStamp.setHorizontalAlignment(com.aspose.pdf.HorizontalAlignment.Right);
	// pageNumberStamp.setStartingNumber(1);
	// // set text properties
	// setDefaultTextProperties(pageNumberStamp.getTextState());
	// // iterate through all pages of PDF file
	// PageCollection pages = pdfDocument.getPages();
	// for (int pageNum = 1; pageNum <= pages.size(); pageNum++) {
	// // add stamp to all pages of PDF file
	// Page page = pages.get_Item(pageNum);
	// if(page!=null){
	// page.addStamp(textStamp);
	// page.addStamp(pageNumberStamp);
	// }
	// }
	// }
	public void setHeaderText(String header, InputStream in, OutputStream out) throws IOException, PDFException {
		PdfReader pdfReader = new PdfReader(in);
		try {
			PdfStamper pdfStamper = new PdfStamper(pdfReader, out);
			int pageCount = pdfReader.getNumberOfPages();
			for (int i = 1; i <= pageCount; i++) {
				PdfImportedPage page = pdfStamper.getImportedPage(pdfReader, i);
				// add header
				PdfPTable table = new PdfPTable(2);

				int headerPositionX = 34;
				float headerPositionY = 830;
				float totalWidth = page.getWidth() - (headerPositionX * 2);
				table.setWidths(new int[] { 24, 10 });
				table.setTotalWidth(totalWidth);
				table.setLockedWidth(true);
				table.getDefaultCell().setFixedHeight(20);
				table.getDefaultCell().setBorder(Rectangle.BOTTOM);
				table.addCell(header);
				table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				table.addCell(String.format("Seite %d von %d", i, pageCount));
				table.writeSelectedRows(0, -1, headerPositionX, headerPositionY, pdfStamper.getOverContent(i));
			}
			pdfStamper.close();
		} catch (DocumentException e) {
			throw new PDFException(e);
		}
	}
	// private void setDefaultTextProperties(TextState textState) {
	// if(fontArial==null){
	// fontArial = FontRepository.findFont("Arial");
	// }
	// if(fontArial!=null){
	// textState.setFont(fontArial);
	// }
	// textState.setFontSize(9.0F);
	// // textState.setFontStyle(FontStyles.Bold);
	// textState.setFontStyle(FontStyles.Italic);
	// // textState.setForegroundColor(com.aspose.pdf.Color.getBlue());
	// }

}
