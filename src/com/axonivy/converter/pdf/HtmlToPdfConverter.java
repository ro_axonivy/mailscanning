package com.axonivy.converter.pdf;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import com.aspose.pdf.Document;
import com.aspose.pdf.HtmlLoadOptions;
import com.aspose.pdf.IWarningCallback;
import com.aspose.pdf.PdfSaveOptions;

public class HtmlToPdfConverter extends PdfConverter {
	public HtmlToPdfConverter() {
		AsposeLicenseLoader.loadPdfLicense();
	}

	@Override
	public void convert(InputStream source, OutputStream target, final WarningHandler warningHandler) throws Exception {
		convert(source, target, warningHandler, null);
	}

	public void convert(InputStream source, OutputStream target, final WarningHandler warningHandler, Map<String, byte[]> resources)
			throws Exception {
		HtmlLoadOptions loadOptions = new HtmlLoadOptions();
		PdfSaveOptions saveOptions = new PdfSaveOptions();
		loadOptions.setUseNewConversionEngine(true);
		if (resources != null) {
			loadOptions.CustomLoaderOfExternalResources = new ExternalResourceLoadingStrategy(resources);
		}
		if (warningHandler != null) {
			IWarningCallback warningCallBack = new PdfLoadWarningHandler() {
				@Override
				public void warn(int source, int warningType, String description) {
					warningHandler.warn(source, warningType, description);
				}
			};
			loadOptions.setWarningHandler(warningCallBack);
			saveOptions.setWarningHandler(warningCallBack);
		}
		Document pdfDocument = new Document(source, loadOptions);
		// Save HTML file
		pdfDocument.save(target, saveOptions);
	}
}
