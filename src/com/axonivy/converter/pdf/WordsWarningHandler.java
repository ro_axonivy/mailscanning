package com.axonivy.converter.pdf;

import com.aspose.words.IWarningCallback;
import com.aspose.words.WarningInfo;

public abstract class WordsWarningHandler extends WarningHandler implements IWarningCallback {
	/**
	 * Our callback only needs to implement the "Warning" method. This method is
	 * called whenever there is a potential issue during document processing.
	 * The callback can be set to listen for warnings generated during document
	 * load and/or document save.
	 * 
	 */
	@Override
	public void warning(WarningInfo info) {
		warn(info.getSource(), info.getWarningType(), info.getDescription());
	}
}
