package com.axonivy.converter.pdf;

import java.io.InputStream;
import java.io.OutputStream;

import com.aspose.words.Document;
import com.aspose.words.FileFormatInfo;
import com.aspose.words.FileFormatUtil;
import com.aspose.words.IWarningCallback;
import com.aspose.words.LoadFormat;
import com.aspose.words.LoadOptions;
import com.aspose.words.PdfSaveOptions;

public class DocToPdfConverter extends PdfConverter {
	public DocToPdfConverter() {
		AsposeLicenseLoader.loadWordLicense();
	}

	@Override
	public void convert(InputStream source, OutputStream target, final WarningHandler warningHandler) throws Exception {
		LoadOptions loadOptions = new LoadOptions();
		PdfSaveOptions saveOptions = new PdfSaveOptions();
		if(warningHandler!=null){
			IWarningCallback docLoadWarningHandler = new WordsWarningHandler() {
				@Override
				public void warn(int source, int warningType, String description) {
					warningHandler.warn(source, warningType, description);
				}
			};
			loadOptions.setWarningCallback(docLoadWarningHandler);			
			saveOptions.setWarningCallback(docLoadWarningHandler);
		}
		Document doc = new Document(source, loadOptions);
		doc.save(target, saveOptions);
	}

	public boolean isConvertable(InputStream stream) {
		boolean isConvertable = false;
		// Check the file format and move the file to the appropriate folder.
		FileFormatInfo info;
		try {
			info = FileFormatUtil.detectFileFormat(stream);
			// Now copy the document into the appropriate folder.
			if (info.isEncrypted()) {
				// may supported but encrypted.
				isConvertable = false;
			} else {
				switch (info.getLoadFormat()) {
				case LoadFormat.DOC_PRE_WORD_97:
					// Pre Word is not supported
					isConvertable = false;
					break;
				case LoadFormat.UNKNOWN:
					// Unknown and not supported
					isConvertable = false;
					break;
				default:
					// supported
					isConvertable = true;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			isConvertable = false;
		}
		return isConvertable;
	}
}
