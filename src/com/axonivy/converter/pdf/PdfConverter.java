package com.axonivy.converter.pdf;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;

public abstract class PdfConverter {
	/**
	 * Opens an existing document from a stream. Automatically detects the file
	 * format, converts the document to PDF and saves it to the specified target
	 * stream.
	 * 
	 * @param source
	 *            the source stream
	 * @param target
	 *            the target stream
	 * @throws Exception
	 */
	public abstract void convert(InputStream source, OutputStream target, WarningHandler warningHandler)
			throws Exception;

	/**
	 * Opens an existing document with a stream. Automatically detects the file
	 * format, converts the document to PDF and saves it to the specified
	 * target.
	 * 
	 * @param source
	 *            the source file
	 * @param target
	 *            the target file
	 * @param targetSaveOptions
	 *            options specifying how the file is saved
	 * @throws Exception
	 */
	public void convert(Path source, Path target, WarningHandler warningHandler, OpenOption... targetSaveOptions)
			throws Exception {
		InputStream input = Files.newInputStream(source);
		try (OutputStream output = Files.newOutputStream(target, targetSaveOptions)) {
			convert(input, output, warningHandler);
		}
	}
}
