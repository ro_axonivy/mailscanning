package com.axonivy.converter.pdf;

import com.aspose.pdf.IWarningCallback;
import com.aspose.pdf.WarningInfo;

public abstract class PdfLoadWarningHandler extends WarningHandler implements IWarningCallback {
	/**
	 * Our callback only needs to implement the "Warning" method. This method is
	 * called whenever there is a potential issue during document processing.
	 * The callback can be set to listen for warnings generated during document
	 * load and/or document save.
	 * 
	 * @return
	 */
	public com.aspose.pdf.internal.p110.z1.z1 warning(WarningInfo info) {
		warn(-1, -1, info.getWarningMessage());
		return null;
	}
}
