package ch.soreco.orderbook.mail;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import ch.soreco.orderbook.mail.pdf.PDFWriter;

/**
 * Provides functionalities to convert a given text into a pdf.
 */
public class PdfCreationService {
	public void createXHtmlPdf(OutputStream out, InputStream xmlContent, Map<String, byte[]> images) throws Exception {
		try (PDFWriter pdf = new PDFWriter(out)) {
			pdf.parseXHtml(xmlContent, images, null);
		}
	}
	public void generatePdfFromTiff(OutputStream out, byte[] content) throws Exception{
		try (PDFWriter pdf = new PDFWriter(out)) {
			pdf.parseTif(content);
		}
	}
	public void generatePdfFromPlainText(OutputStream out, byte[] content) throws Exception{
		try (PDFWriter pdf = new PDFWriter(out)) {
			pdf.parsePlainText(content);
		}
	}
}
