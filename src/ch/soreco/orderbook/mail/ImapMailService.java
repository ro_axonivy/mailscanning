package ch.soreco.orderbook.mail;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.search.FlagTerm;
import javax.mail.search.SearchTerm;

import ch.ivyteam.ivy.environment.Ivy;
import ch.soreco.orderbook.mail.exception.MailServiceException;

import com.sun.mail.imap.IMAPFolder;

/**
 * Provides functionalities to access a defined IMAPs mailbox.
 */
public class ImapMailService extends AbstractMailService {
	/**
	 * Creates a new instance.
	 * 
	 * @throws MailServiceException
	 */
	public ImapMailService(final Properties properties) throws MailServiceException {
		super(properties);
	}

	@Override
	public void setFlags(Message message, Flags flags, boolean value) throws MailServiceException {
		try {
			message.setFlags(flags, value);
		} catch (MessagingException e) {
			throw new MailServiceException(e);
		}
	}

	@Override
	public MessageFolder createMessageFolder(String name) throws MailServiceException {
		return new ImapMessageFolder(getStore(), name);
	}

	public class ImapMessageFolder extends MessageFolder {
		public ImapMessageFolder(Store store, String folderName) throws MailServiceException {
			super(store, folderName);
		}

		@Override
		public String getUID(Message message) throws MailServiceException {
			long uid;
			try {
				UIDFolder folder = (UIDFolder) getFolder();
				uid = folder.getUID(message);
			} catch (MessagingException e) {
				throw new MailServiceException(e);
			}
			return String.valueOf(uid);
		}

		@Override
		public Message findMessageByUID(String uid) throws MailServiceException {
			Long id = Long.valueOf(uid);
			UIDFolder uidFolder = (UIDFolder) getFolder();
			Message message = null;
			try {
				message = uidFolder.getMessageByUID(id);
			} catch (MessagingException e) {
				String error = String.format("Unable to find message from uid %s", uid);
				throw new MailServiceException(error, e);
			}
			return message;
		}

		/**
		 * Search whole folder for messages matching the term
		 * {@link Flags.Flag.SEEN} is false</code>.<br>
		 * <strong>DO NOT USE</strong> within Ivy Engine Environments - this
		 * would cause a java.lang.ClassNotFoundException:
		 * com.sun.mail.imap.IMAPFolder as the rt.jar does not load/activate the
		 * required Classes.<br>
		 * If the property mail.imap.throwsearchexception is true, and the
		 * search term is too complex for the IMAP protocol, SearchException is
		 * thrown. Otherwise, if the search term is too complex, super.search is
		 * called to do the search on the client.
		 * 
		 * @return
		 * @throws MailServiceException
		 * @see {@link IMAPFolder#search(SearchTerm)}
		 */
		public List<Message> findUnreadedMessages() throws MailServiceException {

			Flags seen = new Flags(Flags.Flag.SEEN);
			FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
			List<Message> newMessages = new ArrayList<Message>();
			IMAPFolder folder = (IMAPFolder) getFolder();
			try {
				Message messages[] = folder.search(unseenFlagTerm);
				for (Message message : messages) {
					newMessages.add(message);
				}
			} catch (MessagingException e) {
				throw new MailServiceException(e);
			}

			return newMessages;
		}

		public List<Message> findUnreadMessages() throws MailServiceException {
			Flags seen = new Flags(Flags.Flag.SEEN);
			FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
			List<Message> newMessages = new ArrayList<Message>();
			String className = "com.sun.mail.imap.IMAPFolder";
			String methodName = "search";
			Object object = getFolder();
			Parameter invokeParameter = new Parameter();
			invokeParameter.setValue(unseenFlagTerm);
			invokeParameter.setType(SearchTerm.class);

			Ivy.log().trace("Invoke of search with " + unseenFlagTerm.getClass().getName());
			Object result = invokeOn(className, methodName, object, invokeParameter);
			if (result != null) {
				Message messages[] = (Message[]) result;
				for (Message message : messages) {
					newMessages.add(message);
				}
			}

			return newMessages;
		}

		@Override
		public void moveMessageTo(Message message, MessageFolder target) throws MailServiceException {
			if(message!=null){
				Message[] msgs = new Message[] { message };
				Folder folder = target.getFolder();
				try {
					moveMessages(msgs, folder);
				} catch (MessagingException e) {
					throw new MailServiceException(e);
				}				
			}
		}

		/**
		 * Copy the specified messages from this folder, to the specified
		 * destination. Depends on the MOVE extension (RFC 6851). As the method
		 * is only available at version 1.5.4 and the current mail api refers to
		 * 1.4.7 the message is copied to target and deleted from the source.
		 * 
		 * @param messages
		 *            the messages to move
		 * @param folder
		 *            the folder to move the messages to
		 * @throws MessagingException
		 * @throws MailServiceException
		 */
		private void moveMessages(Message[] messages, Folder target) throws MessagingException {
			if (!target.exists()) {
				target.create(Folder.HOLDS_MESSAGES);
			}
			Folder folder = getFolder();
			folder.copyMessages(messages, target);
			folder.setFlags(messages, new Flags(Flags.Flag.DELETED), true);
			
		}

		/**
		 * get the count of unread messages within java runtime
		 * 
		 * @return number of unread messages. -1 may be returned by certain
		 *         implementations if this method is invoked on a closed folder.
		 * @see IMAPFolder#getUnreadMessageCount()
		 */
		public int getUnreadedMessageCount() throws MailServiceException {
			int count = 0;
			try {
				// the class IMAPFolder is loaded by the classic Java Class
				// Loading using activation.jar
				IMAPFolder folder = (IMAPFolder) getFolder();
				count = folder.getUnreadMessageCount();
			} catch (MessagingException e) {
				throw new MailServiceException(e);
			}
			return count;
		}

		/**
		 * @see IMAPFolder#getUnreadMessageCount()
		 */
		@Override
		public int getUnreadMessageCount() throws MailServiceException {
			String className = "com.sun.mail.imap.IMAPFolder";
			String methodName = "getUnreadMessageCount";
			Object object = getFolder();
			Object result = invokeOn(className, methodName, object);
			int count = 0;
			if (result != null && result instanceof Integer) {
				count = (Integer) result;
			}
			return count;
		}

		private Object invokeOn(String className, String methodName, Object object, Parameter... parameters)
				throws MailServiceException {

			Class<?>[] paramTypes = null;
			Object[] paramValues = null;
			if (parameters != null && parameters.length > 0) {
				paramValues = new Object[parameters.length];
				paramTypes = new Class<?>[parameters.length];
				for (int i = 0; i < parameters.length; i++) {
					Object value = parameters[i].getValue();
					Class<?> type = parameters[i].getType();
					paramValues[i] = value;
					if (type != null) {
						paramTypes[i] = type;
					} else if (value != null) {
						paramTypes[i] = value.getClass();
					}
				}
			}
			// FIXME Ivy-Classloading-Hack: remove the whole classloading-stuff.
			Thread currentThread = Thread.currentThread();
			ClassLoader oldClassLoader = currentThread.getContextClassLoader();
			ClassLoader classLoaderOfSessionClass = Session.class.getClassLoader();

			currentThread.setContextClassLoader(classLoaderOfSessionClass);
			Class<?> clazz = null;
			try {
				clazz = Class.forName(className, true, classLoaderOfSessionClass);
				//Ivy.log().trace("ImapMailService initialized class by name:" + clazz.getName());
			} catch (ClassNotFoundException e1) {
				throw new MailServiceException(e1);
			}
			Object result = null;
			// FIXME end of preparation
			try {
				Object invokeTo = clazz.cast(object);
				Method methodOfClazz = clazz.getMethod(methodName, paramTypes);
				if (paramValues != null) {
					result = methodOfClazz.invoke(invokeTo, paramValues);
				} else {
					result = methodOfClazz.invoke(invokeTo);
				}
			} catch (Exception e) {
				// FIXME start of resetting the classloader due to the
				// Ivy-Classloading-Hack
				currentThread.setContextClassLoader(oldClassLoader);
				String error = String.format("Unable to invoke %s#%s on object %s", className, methodName,
						object.toString());
				throw new MailServiceException(error, e);
				// FIXME end of resetting the classloader
			}
			return result;
		}

		private class Parameter {
			private Object value;
			private Class<?> type;

			public Object getValue() {
				return value;
			}

			public void setValue(Object value) {
				this.value = value;
			}

			public Class<?> getType() {
				return type;
			}

			public void setType(Class<?> type) {
				this.type = type;
			}
		}
	}

}
