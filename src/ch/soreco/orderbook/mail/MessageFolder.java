package ch.soreco.orderbook.mail;

import java.util.List;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Store;
import javax.mail.search.SearchTerm;

import com.sun.mail.imap.IMAPFolder;

import ch.ivyteam.ivy.environment.Ivy;
import ch.soreco.orderbook.mail.exception.MailServiceException;

/**
 * An abstract class that contains the functionality common to messaging
 * services, such as stores and transports.
 */
public abstract class MessageFolder implements AutoCloseable {
	private Folder folder;
	private Store store;
	private String folderName;

	/**
	 * Constructor used to instantiate an existing folder.
	 * 
	 * @param store
	 *            the store containing this folder
	 * @param folderName
	 *            name of this folder
	 * @throws MailServiceException
	 */
	public MessageFolder(Store store, String folderName) throws MailServiceException {
		this.store = store;
		this.folderName = folderName;
		open();
	}

	/**
	 * gets the folder and opens it with {@link Folder#READ_WRITE}
	 * 
	 * @throws MailServiceException
	 */
	private void open() throws MailServiceException {
		try {
			folder = store.getFolder(folderName);
			folder.open(Folder.READ_WRITE);
		} catch (MessagingException e) {
			throw new MailServiceException("There was an error while opening the folder " + folderName + ".", e);
		}
	}

	/**
	 * Get the folder from with this {@link MessageFolder} was obtained.
	 * 
	 * @return the folder
	 */
	public Folder getFolder() {
		return folder;
	}

	/**
	 * Gets the name of this folder.
	 * 
	 * @return the name of this folder
	 */
	public String getFolderName() {
		return folderName;
	}

	/**
	 * Get the UID for the specified message.
	 * 
	 * @param message
	 *            Message from this folder
	 * @return UID for this message
	 */
	public abstract String getUID(Message message) throws MailServiceException;

	/**
	 * Get the Message corresponding to the given UID.
	 * 
	 * @param uid
	 *            UID for the desired message
	 * @return the Message object. null is returned if no message corresponding
	 *         to this UID is obtained.
	 * @throws MailServiceException
	 */
	public abstract Message findMessageByUID(String uid) throws MailServiceException;

	/**
	 * get the count of unread messages
	 * 
	 * @return number of unread messages. -1 may be returned by certain
	 *         implementations if this method is invoked on a closed folder.
	 * @see IMAPFolder#getUnreadMessageCount()
	 */
	public abstract int getUnreadMessageCount() throws MailServiceException;

	/**
	 * Search whole folder for messages matching the term
	 * {@link Flags.Flag.SEEN} is false</code>.<br>
	 * If the property mail.imap.throwsearchexception is true, and the search
	 * term is too complex for the IMAP protocol, SearchException is thrown.
	 * Otherwise, if the search term is too complex, super.search is called to
	 * do the search on the client.
	 * 
	 * @return list of messages that match {@link Flags.Flag.SEEN} is false
	 * @throws MailServiceException
	 * @see {@link IMAPFolder#search(SearchTerm)}
	 */
	public abstract List<Message> findUnreadMessages() throws MailServiceException;

	/**
	 * Copy the specified message from this folder, to the specified
	 * destination. Depends on the MOVE extension (RFC 6851).
	 * 
	 * @param message
	 *            the message to move
	 * @param folder
	 *            the folder to move the messages to
	 * @throws MailServiceException
	 */
	public abstract void moveMessageTo(Message message, MessageFolder target) throws MailServiceException;

	/**
	 * Close this folder and expunges all deleted messages.
	 * 
	 * @see Folder#close(boolean)
	 */
	public void close() {
		if (folder != null) {
			try {
				folder.close(true);
			} catch (MessagingException e) {
				String error = String.format("There was an error while closing the folder '%s'.", folderName);
				Ivy.log().error(error, e);
			}
		}
	}
}
