package ch.soreco.orderbook.mail.persistence;

import java.sql.SQLException;

import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.soreco.orderbook.bo.Order;
import ch.soreco.orderbook.bo.OrderMatching;
import ch.soreco.orderbook.bo.Scanning;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.orderbook.mail.dao.OrderDao;

/**
 * Order Persistence Service {@link #save(Order)} :save a new Order having a
 * Single Scanning which may has some attachments {@link #delete(Integer)}:
 * delete a order by given orderId and its Scannings which may has attachments;
 */
public class OrderService {
	private OrderDao orderDao;

	public OrderService() {
		orderDao = new OrderDao();
	}

	/**
	 * Save an order with a Scanning which may has attachments. The save method
	 * requires to have set {@link Scanning#getOrderMatching()} within the
	 * {@link Order#getScanning()} Some values will be mapped to {@link Order}
	 * and {@link Scanning}:
	 * <table border="1">
	 * <tr>
	 * <th>Order Method</th>
	 * <th>OrderMatching Method</th>
	 * <th>Default</th>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setOrderStart(DateTime)}</td>
	 * <td>&nbsp;</td>
	 * <td>new {@link DateTime}</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setLastEdit(DateTime)}</td>
	 * <td>&nbsp;</td>
	 * <td>new {@link DateTime}</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setOrderType(String)}</td>
	 * <td>{@link OrderMatching#getOrderType()}</td>
	 * <td>{@link OrderType#Default} if null or empty</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setOrderState(String)}</td>
	 * <td>{@link OrderMatching#getStateAfterCreate()}</td>
	 * <td>{@link OrderState#InDispatchingNew} if null, empty or
	 * {@link OrderState#Default}</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setPriority(String)}</td>
	 * <td>{@link OrderMatching#getOrderPriority()}</td>
	 * <td>null if priority is 0</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setOrderId(Integer)}</td>
	 * <td>{@link Order#getOrderId()}</td>
	 * <td>&nbsp;</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setIsMain(Boolean)}</td>
	 * <td>&nbsp;</td>
	 * <td>always true as multiple scannings are not supported by this method</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setIsDossier(Boolean)}</td>
	 * <td>&nbsp;</td>
	 * <td>always false as multiple scannings are not supported by this method</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setDocType(String)}</td>
	 * <td>{@link OrderMatching#getMapToDocType()}</td>
	 * <td>&nbsp;</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setOrderType(orderType)}</td>
	 * <td>{@link Order#getOrderType()}</td>
	 * <td>The order type as defined above</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setScanningId(Integer)}</td>
	 * <td>{@link Scanning#getScanningId()}</td>
	 * <td>From the previously persisted Scanning</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setScanId(String)}</td>
	 * <td>{@link Scanning#getScanId()}</td>
	 * <td>From the previously persisted Scanning</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setScanFileId(Integer)}</td>
	 * <td>{@link Scanning#getScanFileId()}</td>
	 * <td>From the previously persisted Scanning</td>
	 * </tr>
	 * </table>
	 * 
	 * @throws UnsupportedOperationException
	 *             if Order has no Scanning set, the Scanning has no
	 *             OrderMatching, the order has multple Scannings set or the
	 *             order is already persisted
	 * @param order
	 *            Order with a scanning file
	 * @return the saved order
	 * @throws Exception
	 * @throws SQLException
	 * @throws PersistencyException
	 */
	public Order save(Order order) throws Exception {
		if (order.getScanning() == null) {
			throw new UnsupportedOperationException("Save of an Order without Scanning is not supported!");
		} else if (order.getScanning().getOrderMatching() == null) {
			throw new UnsupportedOperationException(
					"Save of an Order with Scanning having no OrderMatching is not supported!");
		} else if (order.getScannings() != null) {
			throw new UnsupportedOperationException("Save of an Order with multiple scannings is not supported!");
		} else if (order.getOrderId() != null && order.getOrderId() > 0) {
			throw new UnsupportedOperationException("Save of a already persisted Order is not supported!");
		}
		// Get transient scanning data
		Scanning scanning = order.getScanning();
		OrderMatching orderMatching = scanning.getOrderMatching();
		order = save(order, orderMatching);
		return order;
	}

	/**
	 * Save an order with a Scanning which may has attachments. The save method
	 * requires to have set {@link Scanning#getOrderMatching()} within the
	 * {@link Order#getScanning()} Some values will be mapped to {@link Order}
	 * and {@link Scanning}:
	 * <table border="1">
	 * <tr>
	 * <th>Order Method</th>
	 * <th>OrderMatching Method</th>
	 * <th>Default</th>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setOrderStart(DateTime)}</td>
	 * <td>&nbsp;</td>
	 * <td>new {@link DateTime}</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setLastEdit(DateTime)}</td>
	 * <td>&nbsp;</td>
	 * <td>new {@link DateTime}</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setOrderType(String)}</td>
	 * <td>{@link OrderMatching#getOrderType()}</td>
	 * <td>{@link OrderType#Default} if null or empty</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setOrderState(String)}</td>
	 * <td>{@link OrderMatching#getStateAfterCreate()}</td>
	 * <td>{@link OrderState#InDispatchingNew} if null, empty or
	 * {@link OrderState#Default}</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setPriority(String)}</td>
	 * <td>{@link OrderMatching#getOrderPriority()}</td>
	 * <td>null if priority is 0</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setOrderId(Integer)}</td>
	 * <td>{@link Order#getOrderId()}</td>
	 * <td>&nbsp;</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setIsMain(Boolean)}</td>
	 * <td>&nbsp;</td>
	 * <td>always true as multiple scannings are not supported by this method</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setIsDossier(Boolean)}</td>
	 * <td>&nbsp;</td>
	 * <td>always false as multiple scannings are not supported by this method</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setDocType(String)}</td>
	 * <td>{@link OrderMatching#getMapToDocType()}</td>
	 * <td>&nbsp;</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Scanning#setOrderType(orderType)}</td>
	 * <td>{@link Order#getOrderType()}</td>
	 * <td>The order type as defined above</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setScanningId(Integer)}</td>
	 * <td>{@link Scanning#getScanningId()}</td>
	 * <td>From the previously persisted Scanning</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setScanId(String)}</td>
	 * <td>{@link Scanning#getScanId()}</td>
	 * <td>From the previously persisted Scanning</td>
	 * </tr>
	 * <tr>
	 * <td>{@link Order#setScanFileId(Integer)}</td>
	 * <td>{@link Scanning#getScanFileId()}</td>
	 * <td>From the previously persisted Scanning</td>
	 * </tr>
	 * </table>
	 * 
	 * @throws UnsupportedOperationException
	 *             if Order has no Scanning set, the Scanning has no
	 *             OrderMatching, the order has multple Scannings set or the
	 *             order is already persisted
	 * @param order
	 *            Order with a scanning file
	 * @return the saved order
	 * @throws Exception
	 * @throws SQLException
	 * @throws PersistencyException
	 */
	public Order save(Order order, OrderMatching orderMatching) throws Exception {
		if (orderMatching == null) {
			throw new UnsupportedOperationException("Save of an Order without OrderMatching is not supported!");
		} else if (order.getScannings() != null) {
			throw new UnsupportedOperationException("Save of an Order with multiple scannings is not supported!");
		} else if (order.getOrderId() != null && order.getOrderId() > 0) {
			throw new UnsupportedOperationException("Save of a already persisted Order is not supported!");
		}
		DateTime now = new DateTime();
		order.setOrderStart(now);
		order.setLastEdit(now);
		

		// Get transient scanning data
		Scanning scanning = order.getScanning();
		if (scanning == null) {
			scanning = new Scanning();
		}
		scanning.setOrderMatching(orderMatching);
		String docType = orderMatching.getMapToDocType();

		if(docType==null||docType.isEmpty()){
			docType = orderMatching.getDocType();
		}
		scanning.setDocType(docType);
		// Map orderMatching values to order as described in method javadoc
		order = mapOrderMatchingToOrder(order, orderMatching);

		// Save the order now as we need to map the orderId
		order = orderDao.merge(order);

		// Map Order values to Scanning
		scanning.setOrderId(order.getOrderId());
		scanning.setOrderType(order.getOrderType());
		scanning.setIsMain(true);
		scanning.setIsDossier(false);

		// Save the scanning
		ScanningService scanningService = new ScanningService();
		try {
			scanning = scanningService.save(scanning);
		} catch (Exception e) {
			this.delete(order);
			throw e;
		}
		// Map the persisted scanning data back to order
		order.setScanningId(scanning.getScanningId());
		order.setScanId(scanning.getScanId());
		order.setScanFileId(scanning.getScanFileId());

		order = orderDao.merge(order);
		return order;
	}

	/**
	 * Maps values of OrderMatching to Order
	 * 
	 * @param order
	 * @return mapped Order
	 */
	private Order mapOrderMatchingToOrder(Order order, OrderMatching orderMatching) {
		DateTime now = new DateTime();
		order.setOrderStart(now);
		order.setLastEdit(now);

		// Map orderMatching values to order as described in method javadoc
		String orderType = orderMatching.getOrderType();
		if (orderType == null || orderType.equals("")) {
			orderType = OrderType.Default.toString();
		}
		String teams = orderMatching.getMapToTeams();
		order.setTeams(teams);
		order.setOrderType(orderType);

		String orderState = orderMatching.getStateAfterCreate();
		if (orderState == null || orderState.equals("") || orderState.equals(OrderState.Default.toString())) {
			orderState = OrderState.InDispatchingNew.toString();
		}
		order.setOrderState(orderState);
		if (orderMatching.getOrderPriority() != null && orderMatching.getOrderPriority() == 0) {
			order.setPriority(null);
		} else {
			order.setPriority(orderMatching.getOrderPriority());
		}
		return order;
	}

	/**
	 * Delete an order by its given orderId May existing scannings are deleted
	 * by {@link ScanningService.delete(Integer)}
	 * 
	 * @param orderId
	 */
	public void removeById(Integer orderId) {
		Order order = orderDao.findById(orderId);
		this.delete(order);
	}

	private void delete(Order order) {
		orderDao.remove(order);
	}
}
