package ch.soreco.orderbook.mail.persistence;

import java.sql.SQLException;

import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.scripting.objects.Date;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import java.util.List;
import ch.soreco.common.bo.File;
import ch.soreco.orderbook.bo.Attachment;
import ch.soreco.orderbook.bo.Scanning;
import ch.soreco.orderbook.mail.dao.FileDao;
import ch.soreco.orderbook.mail.dao.ScanningDao;
import ch.soreco.webbies.common.db.BlobFile;
/**
 * Saves or deletes a scanning
 */
public class ScanningService {
	private ScanningDao scanningDao;
	public ScanningService(){
		scanningDao = new ScanningDao();
	}

	/**
	 * Save the scanning and may existing attachments
	 * @param scanning
	 * @return Saved scanning
	 * @throws Exception 
	 * @throws SQLException 
	 * @throws PersistencyException 
	 */
	public Scanning save(Scanning scanning) throws PersistencyException, SQLException, Exception{
		//First of all we save the main scanning blob file
		//if an exception is thrown here no scanning has been created
		//map back gained scanning file id
		scanning.setScanFileId(saveScanningBlobFile(scanning.getScanBlobFile()));
		scanning.setScanDate(new Date());
		scanning.setScanTime(new DateTime());
		//get transient attachments data
		List<Attachment> attachments = scanning.getAttachments();
		scanning = scanningDao.merge(scanning);
		//Loop may existing Attachments and persist them with attachment service.
		if(attachments!=null){
			AttachmentService attachmentService = new AttachmentService();
			for(Attachment attachment:attachments){
				attachment.setScanningId(scanning.getScanningId());
				attachment = attachmentService.save(attachment);
			}			
		}
		return scanning;
	}
	/**
	 * Save the Scanning Blob File 
	 * @param scanning
	 * @return
	 * @throws Exception 
	 * @throws SQLException 
	 * @throws PersistencyException 
	 */
	private Integer saveScanningBlobFile(BlobFile scanningBlobFile) throws PersistencyException, SQLException, Exception{
		FileDao fileDao = new FileDao();
		File scanningFile = fileDao.saveFromBlobFile(scanningBlobFile);		
		return scanningFile.getFileId();
	}
}
