package ch.soreco.orderbook.mail.persistence;

import java.sql.SQLException;

import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.soreco.common.bo.File;
import ch.soreco.orderbook.bo.Attachment;
import ch.soreco.orderbook.mail.dao.AttachmentDao;
import ch.soreco.orderbook.mail.dao.FileDao;
/**
 * Save an Attachment with this Service
 */
public class AttachmentService {
	private AttachmentDao attachmentDao;
	FileDao fileDao;
	public AttachmentService(){
		attachmentDao = new AttachmentDao();
		fileDao = new FileDao();
	}
	/**
	 * Saves an attachment, be aware that the attachment should contain a ScanningId and a BlobFile to be saved with {@link FileService} 
	 * @param attachment: the attachment to be saved.
	 * @return the saved attachment
	 * @throws PersistencyException
	 * @throws SQLException
	 * @throws Exception
	 */
	public Attachment save(Attachment attachment) throws PersistencyException, SQLException, Exception{
		if(attachment.getScanningId()==null){
			throw new UnsupportedOperationException("Save on an attachment without ScanningId is not supported!");
		} else if(attachment.getScanningId()==0){
			throw new UnsupportedOperationException("Save on an attachment without ScanningId is not supported!");			
		}
		if(attachment.getFile()!=null&&attachment.getFile().getBlobFile()!=null){
			new FileDao();
			File attachmentFile = attachment.getFile();
			//Uploads the attachment to database and returns the File entity
			attachmentFile = fileDao.saveFromBlobFile(attachmentFile.getBlobFile());
			attachment.setFile(attachmentFile);
			//save the mapping of scanning to attachment to file
			attachment = attachmentDao.merge(attachment);
		}
		return attachment;
	}
}
