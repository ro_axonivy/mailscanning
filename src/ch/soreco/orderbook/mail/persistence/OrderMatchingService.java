package ch.soreco.orderbook.mail.persistence;

import ch.soreco.orderbook.bo.OrderMatching;
import ch.soreco.orderbook.mail.dao.OrderMatchingDao;
/**
 * Use this Service to find an OrderMatching by docType
 */
public class OrderMatchingService {
	private OrderMatchingDao orderMatchingDao;
	public OrderMatchingService(){
		orderMatchingDao = new OrderMatchingDao();
	}
	/**
	 * Finds the first OrderMatching by given docType
	 * @param docType: the docType to find, e.g. "IL"
	 * @return first item matching the docType, may be null
	 */
	public OrderMatching findFirstByDocType(String docType){
		return orderMatchingDao.findFirstByDocType(docType);
	}
}
