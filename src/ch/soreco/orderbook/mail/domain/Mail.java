package ch.soreco.orderbook.mail.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.mail.Address;
import javax.persistence.Transient;

/**
 * Represents a mail with its attachments.
 */
public class Mail implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String JSON_TEMPLATE = "{messageNumber:%d, sentDate:'%s', messageId:'%s', uid:'%s', from:'%s', to:'%s', cc:'%s', bcc:'%s', subject:'%s', mailParts:'%s'}";
	private int messageNumber;
	private Date sentDate;
	private String messageId;
	private String uid;
	private Address[] from;
	private Address[] to;
	private Address[] cc;
	private Address[] bcc;
	private String subject;
	private List<AbstractMailPart> parts;



	public int getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(int messageNumber) {
		this.messageNumber = messageNumber;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Address[] getFrom() {
		return from;
	}

	public void setFrom(Address[] from) {
		this.from = from;
	}

	public Address[] getTo() {
		return to;
	}

	public void setTo(Address[] to) {
		this.to = to;
	}

	public Address[] getCc() {
		return cc;
	}

	public void setCc(Address[] cc) {
		this.cc = cc;
	}

	public Address[] getBcc() {
		return bcc;
	}

	public void setBcc(Address[] bcc) {
		this.bcc = bcc;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	public List<AbstractMailPart> getParts() {
		return parts;
	}

	public void setParts(List<AbstractMailPart> parts) {
		this.parts = parts;
	}

	@Transient
	private String getAddresses(Address[] addresses) {
		StringBuilder fromAddresses = new StringBuilder("");
		if (addresses != null && addresses.length > 0) {
			fromAddresses.append(addresses[0].toString());
			for (int i = 1; i < addresses.length; i++) {
				fromAddresses.append(";");
				fromAddresses.append(addresses[i].toString());
			}
		} else {
			fromAddresses.append("null");
		}
		return fromAddresses.toString();
	}

	@Deprecated
	@Transient
	public String toJson(boolean includeContent) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		String formattedSentDate = sentDate != null ? sdf.format(sentDate) : "null";
		String fromAddresses = getAddresses(from);
		String toAddresses = getAddresses(to);
		String ccAddresses = getAddresses(cc);
		String bccAddresses = getAddresses(bcc);
		StringBuilder mailPartsJson = new StringBuilder("");
		if (parts != null && parts.size() > 0) {
			mailPartsJson.append("[\n\t");
			mailPartsJson.append(parts.get(0).toJson(includeContent));
			for (int i = 1; i < parts.size(); i++) {
				mailPartsJson.append("\n\t,");
				mailPartsJson.append(parts.get(i).toJson(includeContent));
			}
			mailPartsJson.append("\n]");
		} else {
			mailPartsJson.append("null");
		}
		return String.format(JSON_TEMPLATE, messageNumber, formattedSentDate, messageId, uid, fromAddresses, toAddresses,
				ccAddresses, bccAddresses, subject, mailPartsJson.toString());
	}

	@Transient
	public void serializeTo(File file) throws IOException {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))){
			out.writeObject(this);
		}
	}
	@Transient
	public static Mail deserializeFrom(File file) throws IOException, ClassNotFoundException {
		Mail mail = null;
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))){
			mail = (Mail) in.readObject();
		}
		return mail;
	}

}
