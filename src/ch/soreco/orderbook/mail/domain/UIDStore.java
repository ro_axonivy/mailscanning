package ch.soreco.orderbook.mail.domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.mail.Flags;
import javax.mail.Flags.Flag;

public class UIDStore {
	private File storeFile = null;
	public UIDStore(File storeFile) throws IOException{
		this.storeFile = storeFile;
		load(storeFile);
	}
	private Map<String, UIDState> uids = new HashMap<String, UIDState>();
	public void clear(){
		uids.clear();
	}
	public boolean containsKey(String uid){
		boolean inList = uids.containsKey(uid);
		return inList;
	}
	public void setFlags(String uid, Flags flags, boolean value){
		if(uids.containsKey(uid)){
			UIDState state = uids.get(uid);
			state.setPersisted(false);
			if(state.getFlags()!=null){
				if(value){
					state.getFlags().add(flags);					
				} else {
					state.getFlags().remove(flags);
				}
			} else if(value){
				Flags newflags = new Flags(flags);
				state.setFlags(newflags);
			}
		} else {
			UIDState state = new UIDState();
			state.setPersisted(false);
			if(value){
				state.setFlags(flags);				
			}
			uids.put(uid, state);
		}
	}
	public Flags getFlags(String uid){
		Flags flags = null;
		if(uids.containsKey(uid)){
			UIDState state = uids.get(uid);
			flags = state.getFlags();
		}
		return flags;
		
	}
	public void remove(String uid){
		uids.remove(uid);
	}
	
	private void load(File storeFile) throws IOException {
		if (storeFile != null && storeFile.exists()) {
			BufferedReader reader = new BufferedReader(new FileReader(storeFile));
			try {
				String uidRecord = reader.readLine();
				while (uidRecord != null) {
					String[] fields = uidRecord.split(";");
					String uid = fields.length>0? fields[0]:null;
					if(uid!=null){
						UIDState state = new UIDState();
						state.setPersisted(true);
						//TODO: persistence of Flags
						state.setFlags(new Flags(Flags.Flag.SEEN));
						uids.put(uid, state);				
					}
					uidRecord = reader.readLine();
				}
			} finally {
				reader.close();
			}
		}
	}
	public void save() throws IOException{
		save(this.storeFile);
	}
	private void save(File file) throws IOException {
		FileWriter writer = new FileWriter(file);
		try {
			Iterator<String> iterator = uids.keySet().iterator();
			while (iterator.hasNext()) {
				String uid = (String) iterator.next();
				UIDState state = uids.get(uid);
				if (!state.isPersisted()) {
					writer.write(uid);
					if(state.getFlags()!=null){
						Flag[] systemFlags = state.getFlags().getSystemFlags();
						if(systemFlags!=null){
							for(Flags.Flag flag :systemFlags){
								writer.write(";");
								writer.write(flag.toString());								
							}
						}
						String[] userFlags = state.getFlags().getUserFlags();
						if(systemFlags!=null&&userFlags.length>0){
							for(String flag :userFlags){
								writer.write(";");
								writer.write(flag);					
							}
						}
					}
					if (iterator.hasNext())
						writer.write('\n');
				}
			}
		} finally {
			writer.close();
		}
	}
	/**
	 * if we've seen this UID on the server, store it in the list if it was not
	 * already in the list, return true to indicate that the mail is new.
	 * 
	 * @param uid
	 * @return wheter the uid is stored or not.
	 */
	public boolean is2New(String uid, Flags flags) {
		boolean inList = uids.containsKey(uid);
		if(!inList){
			UIDState state = new UIDState();
			state.setPersisted(false);
			Flags newFlags = new Flags(flags);
			state.setFlags(newFlags);
			uids.put(uid, state);	
		}
		return !inList;
	}
	public class UIDState implements Serializable {
		private static final long serialVersionUID = 1L;
		private boolean isPersisted;
		private Flags flags;
		public boolean isPersisted() {
			return isPersisted;
		}
		public void setPersisted(boolean isPersisted) {
			this.isPersisted = isPersisted;
		}
		public Flags getFlags() {
			return flags;
		}
		public void setFlags(Flags flags) {
			this.flags = flags;
		}

	}
}