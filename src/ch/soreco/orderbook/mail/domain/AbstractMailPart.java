package ch.soreco.orderbook.mail.domain;

import java.io.Serializable;

import javax.persistence.Transient;

public abstract class AbstractMailPart implements Serializable {
	private static final long serialVersionUID = 1L;
	@Deprecated
	@Transient
	public abstract String toJson(boolean includeContent);
}
