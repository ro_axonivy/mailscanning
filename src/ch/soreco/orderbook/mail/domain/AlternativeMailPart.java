package ch.soreco.orderbook.mail.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

public class AlternativeMailPart extends AbstractMailPart {
	private static final long serialVersionUID = 1L;
	private List<AbstractMailPart> alternatives = new ArrayList<AbstractMailPart>();
	public void add(AbstractMailPart part){
		if(part!=null){
			alternatives.add(part);
		}
	}
	public void addAll(List<AbstractMailPart> parts){
		if(parts!=null){
			parts.addAll(parts);			
		}
	}
	public List<AbstractMailPart> getAlternatives() {
		return alternatives;
	}

	@Override
	@Deprecated
	@Transient
	public String toJson(boolean includeContent) {
		StringBuilder json = new StringBuilder("");
		json.append("{alternatives: ");
		if (alternatives != null && alternatives.size() > 0) {
			json.append("[\n\t");
			json.append(alternatives.get(0).toJson(includeContent));
			for (int i = 1; i < alternatives.size(); i++) {
				json.append("\n\t,");
				json.append(alternatives.get(i).toJson(includeContent));
			}
			json.append("\n]");
		} else {
			json.append("null");
		}
		json.append("}");

		return json.toString();
	}

}
