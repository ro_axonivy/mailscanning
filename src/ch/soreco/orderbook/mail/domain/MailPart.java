package ch.soreco.orderbook.mail.domain;

import javax.mail.Part;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

public class MailPart extends AbstractMailPart {
	private static final long serialVersionUID = 1L;
	private static final String JSON_TEMPLATE = "{disposition:'%s',fileName:'%s',contentType:'%s', content:'%s'}";

	private String disposition;
	private String fileName;
	private String contentId;
	private String contentType;
	private byte[] content;

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	@Transient
	public String getType() {
		String type = "";
		if (getContentType() != null && getContentType().indexOf(";") > -1) {
			int firstIndexOfSemicolon = getContentType().indexOf(";");
			type = getContentType().substring(0, firstIndexOfSemicolon);
		}
		return type;
	}

	@Transient
	private boolean isAttachment() {
		return getDisposition() != null && getFileName() != null && Part.ATTACHMENT.equalsIgnoreCase(getDisposition())
				&& StringUtils.isNotBlank(getFileName());
	}

	@Deprecated
	@Transient
	public String toJson(boolean includeContent) {
		String contentText = "";
		if (includeContent && !isAttachment() && getContentType() != null && !getContentType().startsWith("image")
				&& !getContentType().startsWith("application")) {
			contentText = new String(getContent());
		}
		return String.format(JSON_TEMPLATE, getDisposition(), getFileName(), getContentType(), contentText);
	}

}
