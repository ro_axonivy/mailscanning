package ch.soreco.orderbook.mail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderNotFoundException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;

import ch.ivyteam.ivy.environment.Ivy;
import ch.soreco.orderbook.mail.domain.Mail;
import ch.soreco.orderbook.mail.domain.UIDStore;
import ch.soreco.orderbook.mail.exception.MailServiceException;
import ch.soreco.orderbook.mail.exception.mail.MessageContentReadingException;

import com.sun.mail.pop3.POP3Folder;

/**
 * Provides functionalities to access a defined pop3s exchange mailbox.
 */
public class Pop3sMailService extends AbstractMailService {
	private UIDStore uids = null;
	private File uidStoreFile = null;

	/**
	 * Creates a new instance.
	 * 
	 * @throws MailServiceException
	 */
	public Pop3sMailService(final Properties properties) throws MailServiceException {
		super(properties);
		uidStoreFile = new File("resources/UIDStore.txt");
		try {
			uids = new UIDStore(uidStoreFile);
		} catch (IOException e) {
			String message = String.format("Cannot load the pop3 mail UIDStore at %s", uidStoreFile.getAbsolutePath());
			Ivy.log().error(message, e);
			throw new MailServiceException(message, e);
		}
	}

	@Override
	public MessageFolder createMessageFolder(String name) throws MailServiceException {
		return new POP3MessageFolder(getStore(), name);
	}

	@Override
	public void setFlags(Message message, Flags flags, boolean value) throws MailServiceException {
		try {
			message.setFlags(flags, value);
			Flags messageFlags = message.getFlags();
			POP3Folder folder = (POP3Folder) message.getFolder();
			String uid = folder.getUID(message);
			uids.setFlags(uid, messageFlags, value);
		} catch (MessagingException e) {
			throw new MailServiceException(e);
		}
	}

	public class POP3MessageFolder extends MessageFolder {

		public POP3MessageFolder(Store store, String folderName) throws MailServiceException {
			super(store, folderName);
		}

		public List<Message> findUnreadMessages() throws MailServiceException {
			List<Message> messages = null;
			try {
				messages = fetchUnreadMessages(true);
			} catch (IOException | MessagingException e) {
				throw new MailServiceException(e);
			}
			return messages;
		}

		private POP3Folder getPOP3Folder() {
			return (POP3Folder) getFolder();
		}

		public String getUID(Message message) throws MailServiceException {
			String uid;
			try {
				uid = getPOP3Folder().getUID(message);
			} catch (MessagingException e) {
				throw new MailServiceException(e);
			}
			return uid;
		}

		private List<Message> fetchUnreadMessages(boolean flagAsSeen) throws IOException, MessagingException,
				MailServiceException {
			POP3Folder pop3folder = getPOP3Folder();

			FetchProfile profile = new FetchProfile();
			profile.add(UIDFolder.FetchProfileItem.UID);
			Message[] messages = pop3folder.getMessages();
			pop3folder.fetch(messages, profile);
			List<Message> newMessages = new ArrayList<Message>();
			boolean hadNewMessages = false;
			for (int i = 0; i < messages.length; i++) {
				String uid = pop3folder.getUID(messages[i]);
				if (!isSeen(uid)) {
					newMessages.add(messages[i]);
					if (flagAsSeen) {
						// TODO: flag as seen should be delivered by message not
						// by uuid store
						uids.setFlags(uid, new Flags(Flags.Flag.SEEN), true);
					}
					hadNewMessages = true;
				}
			}
			if (hadNewMessages) {
				uids.save();
			}
			return newMessages;
		}

		/**
		 * if we've seen this UID on the server, store it in the list if it was
		 * not already in the list, return true to indicate that the mail is
		 * new.
		 * 
		 * @param uid
		 * @return wheter the uid is stored or not.
		 */
		public boolean isSeen(String uid) {
			Flags flags = uids.getFlags(uid);
			return flags != null && flags.contains(Flags.Flag.SEEN);
		}

		@Override
		public int getUnreadMessageCount() throws MailServiceException {
			try {
				return fetchUnreadMessages(false).size();
			} catch (IOException | MessagingException e) {
				throw new MailServiceException(e);
			}
		}

		/**
		 * Get the Message corresponding to the given message number an
		 * converted to {@link Mail}. <br>
		 * <br>
		 * A Message object's message number is the relative position of this
		 * Message in its Folder. Messages are numbered starting at 1 through
		 * the total number of message in the folder. Note that the message
		 * number for a particular Message can change during a session if other
		 * messages in the Folder are deleted and the Folder is expunged. <br>
		 * Message objects are light-weight references to the actual message
		 * that get filled up on demand. Hence Folder implementations are
		 * expected to provide light-weight Message objects. <br>
		 * Unlike Folder objects, repeated calls to getMessage with the same
		 * message number will return the same Message object, as long as no
		 * messages in this folder have been expunged. <br>
		 * Since message numbers can change within a session if the folder is
		 * expunged , clients are advised not to use message numbers as
		 * references to messages. Use Message objects instead.
		 * 
		 * @param msgnum
		 *            the message number
		 * @return the Message object
		 * @throws FolderNotFoundException
		 *             if this folder does not exist.
		 * @throws IllegalStateException
		 *             if this folder is not opened
		 * @throws IndexOutOfBoundsException
		 *             if the message number is out of range.
		 * @throws MessagingException
		 *             for other failures
		 * @see Folder#getMessage(int)
		 */
		@Deprecated
		public Message getMail(int msgnum) {
			Message currentMessage = null;
			// FIXME Ivy-Classloading-Hack: remove the whole classloading-stuff.
			Thread currentThread = Thread.currentThread();
			ClassLoader oldClassLoader = currentThread.getContextClassLoader();
			ClassLoader classLoaderOfSessionClass = Session.class.getClassLoader();
			currentThread.setContextClassLoader(classLoaderOfSessionClass);
			// FIXME end of preparation
			try {
				currentMessage = getFolder().getMessage(msgnum);
			} catch (Exception e) {
				// FIXME start of resetting the classloader due to the
				// Ivy-Classloading-Hack
				currentThread.setContextClassLoader(oldClassLoader);
				String error = String.format("Unable to get Mail %d", msgnum);
				throw new MessageContentReadingException(error, e);
			}
			// FIXME end of resetting the classloader
			return currentMessage;
		}

		@Override
		public Message findMessageByUID(String uid) throws MailServiceException {
			throw new MailServiceException("Unsupported Operation");
		}

		@Override
		public void moveMessageTo(Message message, MessageFolder target) throws MailServiceException {
			throw new MailServiceException("Unsupported Operation");
		}
	}
}
