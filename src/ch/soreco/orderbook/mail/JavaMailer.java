package ch.soreco.orderbook.mail;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.eclipse.core.resources.IProject;

import ch.ivyteam.ivy.components.config.EmailConfiguration;
import ch.ivyteam.ivy.email.EmailSetupConfiguration;
import ch.ivyteam.ivy.email.EmailSetupProviderUtil;
import ch.ivyteam.ivy.email.SimpleMailSender;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.project.IIvyProject;
import ch.ivyteam.ivy.security.SecurityManagerFactory;

@SuppressWarnings("restriction")
public class JavaMailer {

    private static final String AXON_LOGO = "axonLogo";

    public static void send() throws Exception
    {
        File myImage = getCmsImageFile("/Project/Banner");
        Map<String, File> attachments = new HashMap<>();
        attachments.put(AXON_LOGO, myImage);

        EmailConfiguration mail = new EmailConfiguration();
        mail.setFrom("developer@axonivy.com");
        mail.setTo("manager@axonivy.com");
        mail.setSubject("hi from script");
        mail.setMessageContent("<html>hi, i'm a cool mail with embedded attachments! <img src=\"cid:"+AXON_LOGO+"\"></html>");
        mail.setAttachments(attachments.keySet().toArray(new String[attachments.size()]));
        
//        new SimpleMailSender(attachments, mail, Ivy.log(), mailSetup()).sendMessage();
        new SimpleMailSender(attachments, mail, Ivy.log()).sendMessage();
    }

    private static File getCmsImageFile(String coUri) throws IOException {
        File myImage = Files.createTempFile("mailImage", ".png").toFile();
        Ivy.cms().getContentObject(coUri).getValues().get(0)
            .exportContentToFile(myImage, null);
        return myImage;
    }

    /**
     * @return the mail configuration: designer+engine aware, so that mail sending with test-preferences is possible!
     * @throws Exception 
     */
    private static EmailSetupConfiguration mailSetup() throws Exception
    {
        return SecurityManagerFactory.getSecurityManager().executeAsSystem(
          new Callable<EmailSetupConfiguration>() {
            @Override
            public EmailSetupConfiguration call() throws Exception {
                IProject project = Ivy.request().getProcessModelVersion().getProject();
                IIvyProject ivyProject = (IIvyProject)project.getAdapter(IIvyProject.class);
                return EmailSetupProviderUtil.getEmailSenderConfiguration(ivyProject);
            }
        });
    }

//  /**
//   * @param mail
//   * @return converts {@link EmailConfiguration} from 5.1 and older to {@link EMailConfig} from 6.0 and newer
//   */
//  private static EMailConfig make60ApiCompatible(EmailConfiguration mail) 
//  {
//      return new BackgroundOperationLegacyService()
//        .convertMailAnythingAspect(mail.getAnythingContainer());
//  }

}