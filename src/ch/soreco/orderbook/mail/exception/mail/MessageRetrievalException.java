package ch.soreco.orderbook.mail.exception.mail;

/**
 * Should be thrown if there is an error while retrieving the next message from the mailbox.
 */
public class MessageRetrievalException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new {@link MessageRetrievalException} with the specified detail message. <br/>
	 * The cause is not initialized, and may subsequently be initialized by a call to {@link #initCause}.
	 * 
	 * @param message
	 *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()}
	 *            method.
	 */
	public MessageRetrievalException(String message) {
		super(message);
	}
}
