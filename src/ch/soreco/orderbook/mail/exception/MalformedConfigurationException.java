package ch.soreco.orderbook.mail.exception;

/**
 * Should be thrown if the a provided configuration is malformed.
 */
public class MalformedConfigurationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new {@link MalformedConfigurationException} with the specified detail message. <br/>
	 * The cause is not initialized, and may subsequently be initialized by a call to {@link #initCause}.
	 * 
	 * @param message
	 *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()}
	 *            method.
	 */
	public MalformedConfigurationException(String message) {
		super(message);
	}
}
