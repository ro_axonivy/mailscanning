package ch.soreco.orderbook.mail.exception;

/**
 * Should be thrown if any error occurs while communicating with the mail
 * server.
 */
public class MailServiceException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new {@link MailServiceException} with the specified detail
	 * message. <br/>
	 * The cause is not initialized, and may subsequently be initialized by a
	 * call to {@link #initCause}.
	 * 
	 * @param message
	 *            the detail message. The detail message is saved for later
	 *            retrieval by the {@link #getMessage()} method.
	 */
	public MailServiceException(String message) {
		super(message);
	}

	/**
	 * Constructs a new {@link MailServiceException} with the specified detail
	 * message and cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this runtime exception's detail message.
	 * 
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link #getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link #getCause()} method). (A <tt>null</tt> value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.)
	 */
	public MailServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new {@link MailServiceException} with the specified cause.
	 * 
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link #getCause()} method). (A <tt>null</tt> value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.)
	 */
	public MailServiceException(Throwable cause) {
		super(cause);
	}
}
