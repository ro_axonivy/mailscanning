package ch.soreco.orderbook.mail.pdf;

import java.io.IOException;
import java.io.InputStream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HtmlUtils {
	/**
	 * 
	 * @param in
	 *            input stream to read. Make sure to close it after parsing.
	 * 
	 * @param charsetName
	 *            (optional) character set of file contents. Set to null to
	 *            determine from http-equiv meta tag, if present, or fall back
	 *            to UTF-8 (which is often safe to do).
	 * 
	 * @param baseUri
	 *            The URL where the HTML was retrieved from, to resolve relative
	 *            links against.
	 * @return sane HTML
	 * @throws IOException
	 */
	@SuppressWarnings("restriction")
	public static byte[] tidyUp(InputStream in, String charsetName, String baseUri) throws IOException {
		Document document = Jsoup.parse(in, charsetName, baseUri);
		String html = document.html();
		return html.getBytes();
	}

	// @SuppressWarnings("restriction")
	// @Deprecated
	// public static void tidyUp(String path) throws IOException {
	// File html = new File(path);
	// byte[] xhtml = Jsoup.parse(html, null).html().getBytes();
	// File dir = new File("results/xml");
	// dir.mkdirs();
	// FileOutputStream fos = new FileOutputStream(new File(dir,
	// html.getName()));
	// fos.write(xhtml);
	// fos.close();
	// }
}
