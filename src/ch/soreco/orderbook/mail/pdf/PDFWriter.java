package ch.soreco.orderbook.mail.pdf;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import ch.ivyteam.ivy.environment.Ivy;
import ch.soreco.orderbook.mail.exception.pdf.PdfCreationException;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.io.RandomAccessSource;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.Base64;
import com.itextpdf.text.pdf.codec.TiffImage;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.itextpdf.tool.xml.pipeline.html.LinkProvider;

/**
 * A DocWriter class for PDF.<br>
 * <br>
 * When this PdfWriter is added to a certain PdfDocument, the PDF representation
 * of every Element added to this Document will be written to the OutputStream.
 */
public class PDFWriter implements AutoCloseable {
	private Document document = new Document();
	private PdfWriter writer;

	/**
	 * 
	 * @param out
	 *            The OutputStream the writer has to write to.
	 * @throws DocumentException
	 */
	public PDFWriter(OutputStream out) throws DocumentException {
		writer = PdfWriter.getInstance(document, out);
		document.open();
	}

	public boolean addSubject(String subject) {
		return document.addSubject(subject);
	}

	/**
	 * parses given input stream XHTML/CSS or XML flow to PDF
	 * 
	 * @param is
	 *            the {@link InputStream} of the XHTML source
	 * @throws IOException
	 *             if the InputStream could not be read.
	 * @see XMLWorkerHelper#parseXHtml(PdfWriter, Document, InputStream)
	 */
	public void parseXHtml(InputStream is) throws IOException {
		XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
	}

	/**
	 * Reads pages from tif images and adds them to the document
	 * 
	 * @param tifContent
	 * @throws DocumentException
	 */
	public void parseTif(byte[] tifContent) throws DocumentException {
		RandomAccessSourceFactory fac = new RandomAccessSourceFactory();
		RandomAccessSource source = fac.createSource(tifContent);
		RandomAccessFileOrArray file = new RandomAccessFileOrArray(source);
		int pages = TiffImage.getNumberOfPages(file);
		for (int page = 1; page <= pages; page++) {
			Image image = TiffImage.getTiffImage(file, page);
			float scaler = 0;
			if (image.getWidth() > image.getHeight()) {
				image.setRotationDegrees(90);
			}
			// Scale to fit into A4: 597.6,842.4
			float targetWidth = 595;
			scaler = (targetWidth / image.getWidth()) * 100;
			image.scalePercent(scaler);
			document.newPage();
			document.add(image);
		}
	}

	/**
	 * Reads pages from plain text and adds them as paragraphs to the document
	 * 
	 * @param textContent
	 *            the text to add
	 * @throws DocumentException
	 */
	public void parsePlainText(byte[] textContent) throws IOException, DocumentException {
		BufferedReader reader = null;
		try (InputStream is = new ByteArrayInputStream(textContent)) {
			reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				Paragraph p = new Paragraph(line);
				p.setAlignment(Element.ALIGN_LEFT);
				document.add(p);
			}
		}
	}

	/**
	 * 
	 * @param is
	 *            the {@link InputStream} of the XHTML source
	 * @param imageRootPath
	 *            a rootPath to set as prefix of src attribute. May be null.
	 * @param linkRoot
	 *            the root for links in the document. May be null.
	 * @throws IOException
	 * @throws PdfCreationException
	 */
	public void parseXHtml(InputStream is, final String imageRootPath, final String linkRoot)
			throws PdfCreationException {
		ExtendedImageProvider imageProvider = new ExtendedImageProvider(imageRootPath);
		parseXHtml(is, imageProvider, linkRoot);
	}

	/**
	 * 
	 * @param is
	 *            the {@link InputStream} of the XHTML source
	 * @param images
	 *            a map of images - of which the key is mapped to the src
	 *            attribute.
	 * @param linkRoot
	 *            the root for links in the document. May be null.
	 * @throws IOException
	 * @throws PdfCreationException
	 */
	public void parseXHtml(InputStream is, Map<String, byte[]> images, final String linkRoot)
			throws PdfCreationException {
		ExtendedImageProvider imageProvider = new ExtendedImageProvider(images);
		parseXHtml(is, imageProvider, linkRoot);
	}

	/**
	 * 
	 * @param is
	 *            the {@link InputStream} of the XHTML source
	 * @param imageProvider
	 *            the {@link AbstractImageProvider} to use.
	 * @param linkRoot
	 *            the root for links in the document. May be null.
	 * @throws IOException
	 * @throws PdfCreationException
	 */
	public void parseXHtml(InputStream is, final AbstractImageProvider imageProvider, final String linkRoot)
			throws PdfCreationException {
		// HTML
		HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
		htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
		if (imageProvider != null) {
			htmlContext.setImageProvider(imageProvider);
		}
		htmlContext.setAcceptUnknown(true).autoBookmark(true);
		if (linkRoot != null) {
			htmlContext.setLinkProvider(new LinkProvider() {
				public String getLinkRoot() {
					return linkRoot;
				}
			});
		}
		// CSS
		CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);

		// Pipelines
		PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
		HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
		CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
		// XML Worker
		XMLWorker worker = new XMLWorker(css, true);
		XMLParser parser = new XMLParser(worker);
		// parser.addListener(worker);
		try {
			parser.parse(is);
		} catch (Exception e) {
			throw new PdfCreationException("Could not create PDF from Input Stream.", e);
		}
	}

	/**
	 * Closes the document.<br>
	 * 
	 * Once all the content has been written in the body, you have to close the
	 * body. After that nothing can be written to the body anymore.
	 * 
	 * @see Document#close()
	 */
	@Override
	public void close() throws Exception {
		document.close();
	}

	private class ExtendedImageProvider extends AbstractImageProvider {
		private Path rootPath = null;
		private Map<String, byte[]> images;
		private Image errorImage = null;

		private ExtendedImageProvider() {

		}

		private ExtendedImageProvider(String imageRootPath) {
			verifyNotNull(imageRootPath, "The image root path can not be null!");
			rootPath = Paths.get(imageRootPath);
		}

		private ExtendedImageProvider(Map<String, byte[]> images) {
			verifyNotNull(images, "The image map can not be null!");
			this.images = images;
		}

		private Image resolveImage(String src) throws BadElementException, MalformedURLException, IOException {
			int pos = src.indexOf("base64,");

			if (src.startsWith("data") && pos > 0) {
				byte[] img = Base64.decode(src.substring(pos + 7));
				return Image.getInstance(img);
			}
			if (images != null && images.containsKey(src)) {
				byte[] imageBytes = images.get(src);
				return Image.getInstance(imageBytes);
			}
			String otherSrc = src.replace("/", "\\");
			if (images != null && images.containsKey(otherSrc)) {
				byte[] imageBytes = images.get(otherSrc);
				return Image.getInstance(imageBytes);
			}

			if (src.startsWith("cid:") && src.contains("@")) {
				String cidSource = src.substring(4, src.indexOf("@"));
				Ivy.log().trace("Search for image " + cidSource);
				if (images.containsKey(cidSource)) {
					byte[] imageBytes = images.get(cidSource);
					return Image.getInstance(imageBytes);
				}
			}

			if (rootPath != null && src != null) {
				Path srcPath = rootPath.resolve(src);
				if (Files.exists(srcPath)) {
					return Image.getInstance(srcPath.toUri().toString());
				}
			}

			if (errorImage != null) {
				return errorImage;
			} else {
				URL url = ch.soreco.utilities.file.FileUtils.getResource("ban.png");
				if (url != null) {
					errorImage = Image.getInstance(url);
					return errorImage;
				}
			}
			return null;
		}

		@Override
		public Image retrieve(String src) {
			Image image = null;
			try {
				image = resolveImage(src);
			} catch (Exception e) {
				Ivy.log().error(String.format("Could not resolve image %s", src), e);
			}
			return image;
		}

		@Override
		public String getImageRootPath() {
			return null;
		}

		private void verifyNotNull(Object obj, String errorMessage) {
			if (obj == null) {
				throw new IllegalArgumentException(errorMessage);
			}
		}
	}
}