package ch.soreco.orderbook.mail.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

public class PDFMerge {
	private PDFMerge() {

	}

	public static void doMerge(String header, OutputStream out, List<byte[]> pdfs) throws IOException, PDFException {
		PDFMerge merger = new PDFMerge();
		merger.merge(header, out, pdfs);
	}

	public void merge(String header, OutputStream out, List<byte[]> pdfs) throws IOException, PDFException {
		Document copyTo = new Document();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			PdfCopy copy = new PdfCopy(copyTo, baos);
			copyTo.open();
			PdfReader.unethicalreading = true;
			// loop over the documents you want to concatenate
			for (int i = 0; i < pdfs.size(); i++) {
				PdfReader reader = new PdfReader(pdfs.get(i));
				// loop over the pages in that document
				int n = reader.getNumberOfPages();
				for (int page = 0; page < n;) {
					copy.addPage(copy.getImportedPage(reader, ++page));
				}
				copy.freeReader(reader);
				reader.close();
			}
		} catch (DocumentException e) {
			throw new PDFException(e);
		} finally {
			// step 5
			copyTo.close();
		}
		PdfReader pdfReader = new PdfReader(baos.toByteArray());
		try {
			PdfStamper pdfStamper = new PdfStamper(pdfReader, out);
			int pageCount = pdfReader.getNumberOfPages();
			for (int i = 1; i <= pageCount; i++) {
				PdfImportedPage page = pdfStamper.getImportedPage(pdfReader, i);
				//add header
	            PdfPTable table = new PdfPTable(2);
	            
	            int headerPositionX =34;
	            float headerPositionY = 830;
	            float totalWidth = page.getWidth()-(headerPositionX*2);
	            table.setWidths(new int[]{24, 10});
				table.setTotalWidth(totalWidth);
	            table.setLockedWidth(true);
	            table.getDefaultCell().setFixedHeight(20);
	            table.getDefaultCell().setBorder(Rectangle.BOTTOM);
	            table.addCell(header);
	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(String.format("Seite %d von %d", i, pageCount));
				table.writeSelectedRows(0, -1, headerPositionX, headerPositionY, pdfStamper.getOverContent(i));
			}
			pdfStamper.close();
		} catch (DocumentException e) {
			throw new PDFException(e);
		}
	}
}
