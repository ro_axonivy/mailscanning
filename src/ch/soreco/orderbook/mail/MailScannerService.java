package ch.soreco.orderbook.mail;

import java.util.List;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.Session;

import ch.ivyteam.ivy.environment.Ivy;
import ch.soreco.orderbook.mail.domain.Mail;
import ch.soreco.orderbook.mail.exception.MailServiceException;
import ch.soreco.orderbook.mail.exception.MalformedConfigurationException;
import ch.soreco.utilities.file.FileUtils;

/**
 * Provides various functions to access a mailbox.
 */
public class MailScannerService {
	private static MailScannerService INSTANCE;
	private static final String CONFIG_FILE_NAME = "mail.properties.xml";
	// private static final String MAIL_PROTOCOL_KEY = "mail.store.protocol";
	private static final String MAIL_PROTOCOL_IMAP = "imaps";
	private static final String INBOX_FOLDER_NAME = "Inbox";
	private static final String PROCESSED_FOLDER_NAME = "Processed";
	public static final String PROCESSED_HEADER = "x-ivy-order-id";

	private AbstractMailService mailService;

	/**
	 * This constructor is private due to the singleton pattern.
	 * 
	 * @throws MailServiceException
	 */
	private MailScannerService(Properties properties) throws MailServiceException {
		mailService = new ImapMailService(properties);
		/*
		 * If it would be needed to use pop3 protocol we could use the
		 * properties to configure this As of today POP3s Services have some
		 * unimplemented methods. <code>String protocol =
		 * properties.getProperty("mail.store.protocol", MAIL_PROTOCOL_IMAP); if
		 * (protocol.equalsIgnoreCase(MAIL_PROTOCOL_IMAP)) { mailService = new
		 * ImapMailService(properties); } else { mailService = new
		 * Pop3sMailService(properties); } </code>
		 */
	}

	/**
	 * Check whether this Service is initialized and connected to store. Pings
	 * the server connection.
	 * 
	 * @return true if the service is initialized and connected, false if it is
	 *         not initialized or connected
	 */
	public boolean isConnected() {
		return mailService != null && mailService.isConnected();
	}

	/**
	 * The session from which the store was created.
	 * 
	 * @return the session
	 */
	public Session getSession() {
		return mailService != null ? mailService.getSession() : null;
	}

	/**
	 * 
	 * @param mailServer
	 * @param port
	 * @return
	 */
	public static Properties loadDefaultProperties() {
		Properties properties = FileUtils.loadPropertiesFromResource(CONFIG_FILE_NAME);
		if (properties == null) {
			properties = new Properties();
			String message = String.format("Could not find mail property resource %s", CONFIG_FILE_NAME);
			Ivy.log().debug(message);
		}
		if (properties.size() == 0) {
			properties.put("mail.imaps.auth", "true");
			properties.put("mail.imaps.partialfetch", "false");
			properties.put("mail.store.protocol", MAIL_PROTOCOL_IMAP);
			String message = String.format("No mail properties set.", MAIL_PROTOCOL_IMAP);
			Ivy.log().trace(message);
		} else if (!properties.containsKey("mail.store.protocol")) {
			properties.put("mail.store.protocol", MAIL_PROTOCOL_IMAP);
			String message = String.format("No mail.store.protocol set. Using %s protocol as default.",
					MAIL_PROTOCOL_IMAP);
			Ivy.log().debug(message);
		}
		return properties;
	}

	/**
	 * Access the current instance of the {@link MailScannerService}.
	 * 
	 * @throws MailServiceException
	 */
	public static MailScannerService getInstance() throws MailServiceException {
		if (INSTANCE == null) {
			Properties properties = loadDefaultProperties();
			INSTANCE = new MailScannerService(properties);
		}
		return INSTANCE;
	}

	/**
	 * Access the current instance of the {@link MailScannerService}.
	 * 
	 * @throws MailServiceException
	 */
	public static MailScannerService getInstance(Properties properties) throws MailServiceException {
		if (INSTANCE == null) {
			INSTANCE = new MailScannerService(properties);
		}
		return INSTANCE;
	}

	/**
	 * Connects to the specified mailbox.
	 * 
	 * @throws MailServiceException
	 * 
	 * 
	 * @throws IllegalArgumentException
	 *             if one or more connection parameters are null oder empty.
	 */
	public void connect(String mailServer, Integer port, String username, String password) throws MailServiceException {
		verifyConfigurationIsWellformed(mailServer, port, username, password);
		try {
			if (mailService.isConnected()) {
				mailService.close();
			}
			mailService.connect(mailServer, port, username, password);
		} catch (MailServiceException e) {
			String message = String.format("Could not connect to %s@%s:%d", username, mailServer, port);
			throw new MailServiceException(message, e);
		}
	}

	private void verifyConfigurationIsWellformed(String mailServer, Integer port, String username, String password) {
		try {
			verifyNotNullNorEmpty(mailServer, "Mailserver must not be null nor empty.");
			verifyNotNullNorEmpty(username, "Username must not be null nor empty.");
			verifyNotNullNorEmpty(password, "Password must not be null nor empty.");
			verifyNotNull(port, "Port must not be null.");
		} catch (IllegalArgumentException e) {
			String errorMessage = e.getMessage();
			throw new MalformedConfigurationException(errorMessage);
		}
	}

	/**
	 * Disconnects the Service from the currently instantiated message folder.
	 */
	public void disconnect() {
		mailService.close();
		INSTANCE = null;
	}

	/**
	 * Disconnects the Service from the currently instantiated message folder.
	 */
	public void disconnectSilent() {
		String connectionString = mailService.getConnectionString();
		try {
			mailService.close();
		} catch (Exception e) {
			Ivy.log().error("Could not disconnect from {0}.", e, connectionString);
		}
		INSTANCE = null;
	}

	/**
	 * Returns the count of mails in the currently instantiated message folder.
	 * 
	 * @throws MailServiceException
	 */
	public Integer countMailsInInbox() throws MailServiceException {
		int count = 0;
		try {
			MessageFolder inbox = mailService.getMessageFolder(INBOX_FOLDER_NAME);
			count = inbox.getUnreadMessageCount();
		} catch (Exception e) {
			String error = String.format("Unable to count mails of folder %s.", INBOX_FOLDER_NAME);
			throw new MailServiceException(error, e);
		}
		return count;
	}

	/**
	 * Retrieved the next mail from the currently instantiated message folder.
	 * 
	 * @throws MailServiceException
	 */
	public Mail retrieveNextMail() throws MailServiceException {
		MessageFolder inbox = mailService.getMessageFolder(INBOX_FOLDER_NAME);
		List<Message> unreadMessages = inbox.findUnreadMessages();
		Mail mail = null;
		if (unreadMessages != null && unreadMessages.size() > 0) {
			Message message = unreadMessages.get(0);
			String uid = inbox.getUID(message);
			mail = mailService.parseMessageToMail(message, uid);
		}
		return mail;
	}

	/**
	 * Moves the mail as given by uid to the processed folder.
	 * 
	 * @throws MailServiceException
	 */
	public void markMailAsProcessed(String uid, Integer orderId) throws MailServiceException {
		Message message = findMessageByUID(uid);
		MessageFolder source = mailService.getMessageFolder(INBOX_FOLDER_NAME);
		MessageFolder target = mailService.getMessageFolder(PROCESSED_FOLDER_NAME);
		mailService.setFlags(message, new Flags(Flags.Flag.ANSWERED), true);
		source.moveMessageTo(message, target);
	}

	/**
	 * Marks the last received mail as failed on the currently instantiated
	 * folder.
	 * 
	 * @throws MailServiceException
	 */
	public void markMailAsFailed(String uid) throws MailServiceException {
		Message message = findMessageByUID(uid);
		mailService.setFlags(message, new Flags(Flags.Flag.DRAFT), true);
	}

	private Message findMessageByUID(String uid) throws MailServiceException {
		MessageFolder inbox = mailService.getMessageFolder(INBOX_FOLDER_NAME);
		Message message = inbox.findMessageByUID(uid);
		return message;
	}

	private void verifyNotNullNorEmpty(String string, String errorMessage) {
		verifyNotNull(string, errorMessage);
		if (string.isEmpty()) {
			throw new IllegalArgumentException(errorMessage);
		}
	}

	private void verifyNotNull(Object obj, String errorMessage) {
		if (obj == null) {
			throw new IllegalArgumentException(errorMessage);
		}
	}
}
