package ch.soreco.orderbook.mail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.Address;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import ch.ivyteam.ivy.environment.Ivy;
import ch.soreco.orderbook.bo.Attachment;
import ch.soreco.orderbook.bo.Order;
import ch.soreco.orderbook.bo.Scanning;
import ch.soreco.orderbook.enums.Fdl;
import ch.soreco.orderbook.mail.domain.AbstractMailPart;
import ch.soreco.orderbook.mail.domain.AlternativeMailPart;
import ch.soreco.orderbook.mail.domain.Mail;
import ch.soreco.orderbook.mail.domain.MailPart;
import ch.soreco.orderbook.mail.pdf.HtmlUtils;
import ch.soreco.webbies.common.db.BlobFile;

import com.axonivy.converter.pdf.DocToPdfConverter;
import com.axonivy.converter.pdf.HtmlToPdfConverter;
import com.axonivy.converter.pdf.PdfMerger;
import com.axonivy.converter.pdf.WarningHandler;

/**
 * Provides various functions to converts a Mail to an Order.
 */
public class OrderAdapter {
	public static final String MAIL_AUFTRAG_PREFIX = "mail-auftrag-";
	public static final String MAIL_REPORT_PREFIX = "mail-report-";
	public static final String MAIL_CONTENT_PREFIX = "mail-content-";
	private Mail mail;
	private BlobFile orderBlob;
	private List<BlobFile> attachedBlobs;
	private List<BlobFile> inlineBlobs;
	private Map<String, byte[]> images;
	private static List<String> IMAGE_EXTENSIONS = Arrays.asList(new String[] { "jpg", "png", "gif" });
	private PdfCreationService pdfCreationService;
	private boolean useFirstPdfAsOrder;
	private boolean useSubjectForRefNo;
	private DocToPdfConverter converter;
	private HtmlToPdfConverter htmlConverter;
	private WarningHandler warningHandler;
	private static final String NO_SUBJECT = "Kein Betreff";
	private static final String TIFF_CONTENT_TYPE = "image/tiff";
	private static final String PDF_CONTENT_TYPE = "application/pdf";
	private static final String TXT_CONTENT_TYPE = "text/plain";
	private static final String HTML_CONTENT_TYPE = "text/html";

	private OrderAdapter(Mail mail, boolean useFirstPdfAsOrder, boolean useSubjectForRefNo) {
		this.mail = mail;
		this.useFirstPdfAsOrder = useFirstPdfAsOrder;
		this.useSubjectForRefNo = useSubjectForRefNo;

		pdfCreationService = new PdfCreationService();
		attachedBlobs = new ArrayList<BlobFile>();
		inlineBlobs = new ArrayList<BlobFile>();
		images = new HashMap<String, byte[]>();
		
		converter = new DocToPdfConverter();
		htmlConverter = new HtmlToPdfConverter();
		warningHandler = new WarningHandler() {
			@Override
			public void warn(int source, int warningType, String description) {
				Ivy.log().debug("Conversion Warning: source:%d, warningType:%d, description:%s", source, warningType, description);
			}
		};
	}

	/**
	 * Converts a mail to an {@link Order}
	 * 
	 * @param mail
	 *            the mail to convert
	 * @param useFirstPdfAsOrder
	 *            whether the first attached pdf is used as an {@link Order} or the order pdf is concatenated with all content which is
	 *            renderable as PDF.
	 * @param useSubjectForRefNo
	 *            whether the subject of a mail is set to FAClanNo field or not.
	 * @return the Order
	 * @throws IOException
	 */
	public static Order toOrder(Mail mail, boolean useFirstPdfAsOrder, boolean useSubjectForRefNo) throws IOException {
		OrderAdapter adapter = new OrderAdapter(mail, useFirstPdfAsOrder, useSubjectForRefNo);
		return adapter.convertToOrder();
	}

	private Order convertToOrder() throws IOException {
		List<AbstractMailPart> mailParts = mail.getParts();
		doCollectBlobs(mailParts);

		// initialize a new Order set the transient field scanning
		// with an initialized Scanning containing the orderMatching
		Order order = new Order();
		Scanning scanning = new Scanning();

		String subject = mail.getSubject();
		if (subject != null && subject.length() > 254) {
			subject = subject.substring(0, 254);
		}
		if (useSubjectForRefNo) {
			order.setFAClanId(subject);
		}
		scanning.setContentOfDM(subject);

		
		
		
		List<Fdl> fdls = Arrays.asList(Fdl.values());
		Iterator<Fdl> iter = fdls.iterator();
	    while (iter.hasNext()) {
	    	Fdl fdlValue = iter.next();
	    	String fdlValueString = fdlValue.getFdlName();
	    	Ivy.log().debug(fdlValueString);;
	    	if (subject.endsWith(fdlValueString)) {
	    		order.setFDL(fdlValue.name());
	    		break;
	    	}
	    }

	    if (!useFirstPdfAsOrder) {
			doConvertToPdf(inlineBlobs);
			doConvertToPdf(attachedBlobs);

			// collect all pdf files and build a report what's done - which is then shown to the users.
			generateOrderPdf();
			scanning.setScanBlobFile(orderBlob);

			// Prepare a list of BlobFiles representing the attachments of a Mail
			List<BlobFile> attachmentBlobs = new ArrayList<BlobFile>();
			for (BlobFile blobFile : inlineBlobs) {
				if (!blobFile.getFileName().startsWith("mail-content")) {
					attachmentBlobs.add(blobFile);
				}
			}
			attachmentBlobs.addAll(attachedBlobs);
			// Prepare the list of attachments by the list of BlobFiles built above
			List<Attachment> attachments = new ArrayList<Attachment>();
			for (BlobFile attachmentBlob : attachmentBlobs) {
				Attachment attachment = new Attachment();
				ch.soreco.common.bo.File attachmentFile = new ch.soreco.common.bo.File();
				attachmentFile.setBlobFile(attachmentBlob);
				attachment.setFile(attachmentFile);
				attachments.add(attachment);
			}
			scanning.setAttachments(attachments);
		} else {
			BlobFile firstPdf = null;
			BlobFile firstOther = null;

			int unknownAttachmentBlobNameCount = 0;
			for (BlobFile blobFile : attachedBlobs) {
				if (hasContent(blobFile.getBytes())) {
					String fileName = blobFile.getFileName();
					String extension = blobFile.getExtension();
					if (fileName == null) {
						fileName = "Anhang (" + String.valueOf(unknownAttachmentBlobNameCount) + ")";
						fileName = buildFileNameWithExtension(fileName, extension, blobFile.getContentType());
						blobFile.setFileName(fileName);
					}
					boolean isPdfExtension = isPdfExtension(extension);
					if (isPdfExtension && firstPdf == null) {
						firstPdf = blobFile;
					}
					if (!isPdfExtension && firstOther == null) {
						firstOther = blobFile;
					}
				}
			}
			if (firstPdf != null) {
				// FIXME: Flatten the PDF when needed
				scanning.setScanBlobFile(firstPdf);
			} else if (firstOther != null) {
				scanning.setScanBlobFile(firstOther);
			} else {
				String mailSubject = mail.getSubject();
				Date sentDate = mail.getSentDate();
				Address[] from = mail.getFrom();
				Address[] to = mail.getTo();
				Address[] cc = mail.getCc();
				String warning = "Auftr�ge in diesem Postfach m�ssen ein PDF enthalten. Es wurde jedoch kein PDF gefunden. Bitte pr�fen Sie den Sachverhalt.";
				StringBuilder reportHtml = buildReportHtml(mailSubject, sentDate, from, to, cc, null, null, warning);
				BlobFile report = null;
				try {
					byte[] generated = generatePdfFromXml(reportHtml.toString().getBytes());
					String fileName = MAIL_REPORT_PREFIX + subject;
					fileName = escapeFileName(fileName);
					fileName = fileName + ".pdf";
					report = createNewPdfBlobFile(generated, fileName);
				} catch (Exception e) {
					Ivy.log().error("Could not create mail report with {0}", e, reportHtml);
					String fileName = MAIL_REPORT_PREFIX + subject;
					fileName = escapeFileName(fileName);
					fileName = fileName + ".html";
					byte[] htmlContent = reportHtml.toString().getBytes();
					report = createNewHtmlBlobFile(htmlContent, fileName);
				}
				scanning.setScanBlobFile(report);
			}
		}
		order.setScanning(scanning);

		return order;
	}

	private void doCollectBlobs(List<AbstractMailPart> mailParts) {
		if (mailParts != null && mailParts.size() > 0) {
			for (AbstractMailPart mailPart : mailParts) {
				// System.out.println("doCollectBlobsFor:" +
				// mailPart.toJson(false));

				if (mailPart instanceof AlternativeMailPart) {
					AlternativeMailPart alternative = (AlternativeMailPart) mailPart;
					List<AbstractMailPart> alternatives = alternative.getAlternatives();
					MailPart htmlMailPart = null;
					MailPart textMailPart = null;
					for (AbstractMailPart alternativeMailPart : alternatives) {
						if (alternativeMailPart instanceof MailPart) {
							MailPart mailPartAlternative = (MailPart) alternativeMailPart;
							String contentType = mailPartAlternative.getContentType();
							if (contentType != null && contentType.startsWith(HTML_CONTENT_TYPE)) {
								htmlMailPart = mailPartAlternative;
							} else if (contentType != null && contentType.startsWith(TXT_CONTENT_TYPE)) {
								textMailPart = mailPartAlternative;
							}
						}
					}
					if (htmlMailPart != null) {
						BlobFile blob = toBlobFile(htmlMailPart);
						inlineBlobs.add(blob);
					} else if (textMailPart != null) {
						BlobFile blob = toBlobFile(textMailPart);
						inlineBlobs.add(blob);
					} else {
						doCollectBlobs(alternatives);
					}
				} else if (mailPart instanceof MailPart) {
					MailPart part = (MailPart) mailPart;
					String disposition = part.getDisposition();
					if (disposition != null && disposition.equalsIgnoreCase("ATTACHMENT")) {
						BlobFile blob = toBlobFile(part);
						if (blob.getContentType() != null && blob.getContentType().startsWith(TIFF_CONTENT_TYPE)) {
							inlineBlobs.add(blob);
						} else {
							attachedBlobs.add(blob);
						}
					} else if (part.getContent() != null) {
						String fileName = part.getFileName();
						String contentType = part.getContentType();
						boolean isImage = (contentType != null && contentType.startsWith("image")) || isImage(fileName);
						if (fileName != null && isImage) {
							images.put(fileName, part.getContent());
							if(StringUtils.isNotEmpty(part.getContentId())){
								images.put(part.getContentId(), part.getContent());				
								images.put("<"+part.getContentId()+">", part.getContent());				
							}
						} else {
							BlobFile blob = toBlobFile(part);
							inlineBlobs.add(blob);
						}
					}
				}
			}
		}
	}

	private boolean isImage(String fileName) {
		String extension = findExtension(fileName);
		boolean isImage = IMAGE_EXTENSIONS.contains(extension);
		return isImage;
	}

	private boolean isPdfExtension(String extension) {
		return (extension != null && extension.equals("pdf"));
	}

	private boolean hasContent(byte[] content) {
		return content != null && content.length > 0;
	}

	private void doConvertToPdf(List<BlobFile> blobFiles) {
		if (blobFiles != null && blobFiles.size() > 0) {
			for (BlobFile blobFile : blobFiles) {
				String contentType = (blobFile.getContentType() != null ? blobFile.getContentType().toLowerCase() : null);
				byte[] content = blobFile.getBytes();
				String fileName = blobFile.getFileName();
				if (contentType != null && hasContent(content)) {
					byte[] newContent = null;
					System.out.println("Convert " + fileName + " (" + contentType + ")...");
					if (contentType.startsWith(HTML_CONTENT_TYPE)) {
						try {
							//byte[] tidyContent = tidyUp(content);
							// System.out.println(new String(tidyContent));
							newContent = generatePdfFromXml(content);
						} catch (Exception e) {
							Ivy.log().error("Could not convert file: {0}({1}) of mail with subject {2} sent at {3}", e, fileName,
									contentType, mail.getSubject(), mail.getSentDate());
						}
					} else if (contentType.startsWith(TXT_CONTENT_TYPE)) {
						try {
							newContent = generatePdfFromPlainText(content);
						} catch (Exception e) {
							Ivy.log().error("Could not convert file: {0}({1}) of mail with subject {2} sent at {3}", e, fileName,
									contentType, mail.getSubject(), mail.getSentDate());
						}
					} else if (contentType.startsWith(TIFF_CONTENT_TYPE)) {
						try {
							newContent = generatePdfFromTiff(content);
						} catch (Exception e) {
							Ivy.log().error("Could not convert file: {0}({1}) of mail with subject {2} sent at {3}", e, fileName,
									contentType, mail.getSubject(), mail.getSentDate());
						}
					} else {
						try {
							newContent = generatePdfFromBinaryContent(content);
						} catch (Exception e) {
							Ivy.log().error("Could not convert file: {0}({1}) of mail with subject {2} sent at {3}", e, fileName,
									contentType, mail.getSubject(), mail.getSentDate());
						}
					}
					System.out.println("Convert " + fileName + " (" + contentType + ")...finished with "
							+ (newContent != null ? content.length : 0) + " bytes.");
					if (newContent != null) {
						blobFile.setContentType(PDF_CONTENT_TYPE);
						blobFile.setBytes(newContent);
						blobFile.setExtension("pdf");
						blobFile.setOriginalFileName(fileName);
						Ivy.log().debug("Converted file: {0}({1}) of mail with subject {2} sent at {3}", fileName, contentType,
								mail.getSubject(), mail.getSentDate());
						if (fileName != null) {
							fileName = fileName + ".pdf";
							blobFile.setFileName(fileName);
						}
					}
				}

			}
		}
	}

	private byte[] tidyUp(byte[] xmlContent) throws IOException {
		byte[] tidySource = null;
		ByteArrayInputStream in = new ByteArrayInputStream(xmlContent);
		tidySource = HtmlUtils.tidyUp(in, null, "");
		return tidySource;
	}

	private byte[] generatePdfFromXml(byte[] content) throws Exception {
		if (Ivy.log().isDebugEnabled()) {
			String imgNames = "";
			if (images != null) {
				List<String> imageNameList = images.keySet().stream().collect(Collectors.toList());		
				imgNames = StringUtils.join(imageNameList);
			}
			Ivy.log().debug("Generate PDF with images:{0} and HTML:\n{1}", imgNames, new String(content));
		}

		byte[] newContent = null;
		try (ByteArrayInputStream source = new ByteArrayInputStream(content); ByteArrayOutputStream target = new ByteArrayOutputStream()) {
			System.out.println("Generate PDF from html...");
			htmlConverter.convert(source, target, warningHandler, images);
			target.flush();
			newContent = target.toByteArray();
			System.out.println("Generate PDF from html...finished");
		}
		return newContent;

	}

	private byte[] generatePdfFromTiff(byte[] tiffContent) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.out.println("Generate PDF from Plain TIFF...");
		pdfCreationService.generatePdfFromTiff(out, tiffContent);
		System.out.println("Generate PDF from Plain TIFF...finished");
		out.flush();
		out.close();
		return out.toByteArray();
	}

	private byte[] generatePdfFromPlainText(byte[] plainTextContent) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.out.println("Generate PDF from plain Text:" + new String(plainTextContent));
		pdfCreationService.generatePdfFromPlainText(out, plainTextContent);
		System.out.println("Generate PDF from plain text...finished");
		out.flush();
		out.close();
		return out.toByteArray();
	}

	private byte[] generatePdfFromBinaryContent(byte[] content) throws Exception {
		byte[] newContent = null;
		System.out.println("Generate PDF from binary...");
		try (ByteArrayInputStream convertableSource = new ByteArrayInputStream(content);
				ByteArrayInputStream source = new ByteArrayInputStream(content);
				ByteArrayOutputStream target = new ByteArrayOutputStream()) {
			if (converter.isConvertable(convertableSource)) {
				converter.convert(source, target, warningHandler);
				target.flush();
				newContent = target.toByteArray();
			}
		}
		System.out.println("Generate PDF from binary...finished");
		return newContent;
	}

	private void generateOrderPdf() {
		List<BlobFile> pdfs = new ArrayList<BlobFile>();

		List<String> fileNames = new ArrayList<String>();
		List<String> unmergedFileNames = new ArrayList<String>();

		// set the current timestamp as the mail subject
		Date dateTime = new Date();
		String subject = new SimpleDateFormat("yyMMddHHmmss").format(dateTime);
		int unknownInboxBlobNameCount = 0;

		BlobFile firstPdf = null;
		for (BlobFile blobFile : inlineBlobs) {
			if (hasContent(blobFile.getBytes())) {
				String extension = blobFile.getExtension();
				String fileName = blobFile.getFileName();
				if (fileName == null) {
					if (unknownInboxBlobNameCount > 0) {
						fileName = MAIL_CONTENT_PREFIX + String.valueOf(unknownInboxBlobNameCount) + subject;
					} else {
						fileName = MAIL_CONTENT_PREFIX + subject;
					}
					fileName = escapeFileName(fileName);
					fileName = buildFileNameWithExtension(fileName, extension, blobFile.getContentType());
					blobFile.setFileName(fileName);
					unknownInboxBlobNameCount++;
				}
				if (!fileName.startsWith("mail-content")) {
					fileNames.add(fileName);
				}
				boolean isPdf = isPdfExtension(extension);
				if (isPdf) {
					pdfs.add(blobFile);
				} else {
					unmergedFileNames.add(fileName);
				}
				if (firstPdf == null) {
					firstPdf = blobFile;
				}
			}

		}
		int unknownAttachmentBlobNameCount = 0;
		for (BlobFile blobFile : attachedBlobs) {
			if (hasContent(blobFile.getBytes())) {
				String fileName = blobFile.getFileName();
				String extension = blobFile.getExtension();
				if (fileName == null) {
					unknownAttachmentBlobNameCount++;
					fileName = "Anhang (" + String.valueOf(unknownAttachmentBlobNameCount) + ")";
					fileName = buildFileNameWithExtension(fileName, extension, blobFile.getContentType());
					blobFile.setFileName(fileName);
				}
				fileNames.add(fileName);

				boolean isPdf = isPdfExtension(extension);
				if (isPdf) {
					pdfs.add(blobFile);
				} else {
					unmergedFileNames.add(fileName);
				}
				if (firstPdf == null) {
					firstPdf = blobFile;
				}
			}
		}

		String mailSubject = mail.getSubject();
		Date sentDate = mail.getSentDate();
		Address[] from = mail.getFrom();
		Address[] to = mail.getTo();
		Address[] cc = mail.getCc();
		// FIXME: may provide a log with warnings/pokemon exception messages
		StringBuilder reportHtml = buildReportHtml(mailSubject, sentDate, from, to, cc, fileNames, unmergedFileNames, null);
		BlobFile report = null;
		try {
			byte[] generated = generatePdfFromXml(reportHtml.toString().getBytes());
			String fileName = MAIL_REPORT_PREFIX + subject;
			fileName = escapeFileName(fileName);
			fileName = fileName + ".pdf";
			Ivy.log().debug("Generated {0}", fileName);
			report = createNewPdfBlobFile(generated, fileName);
			pdfs.add(0, report); // add the report to pdf bytes to merge
		} catch (Exception e) {
			Ivy.log().error("Could not create mail report with {0}", e, reportHtml);
			String fileName = MAIL_REPORT_PREFIX + subject;
			fileName = escapeFileName(fileName);
			fileName = fileName + ".html";
			byte[] htmlContent = reportHtml.toString().getBytes();
			// FIXME: catch like a pokemon and provide a default PDF attention - something bad could happen if we return null when no other
			// pdf is present.
			report = createNewHtmlBlobFile(htmlContent, fileName);
			Ivy.log().debug("Generated {0}", fileName);
		}

		if (pdfs.size() > 0) {
			try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
				// the header text is configurable through a global variable
				// contains "Dies ist ein Auftrag welcher aus einer Email generiert wurde."
				String header = Ivy.var().get("MAIL_PDF_PAGE_HEAD");

				PdfMerger merger = new PdfMerger();
				merger.merge(header, out, pdfs);// writes all pdfs into one new concatenated pdf output stream
				byte[] mergedPdf = out.toByteArray();// collect all the stuff
				String fileName = MAIL_AUFTRAG_PREFIX + subject;
				fileName = escapeFileName(fileName);
				fileName = fileName + ".pdf";
				orderBlob = createNewPdfBlobFile(mergedPdf, fileName);// convert to blob for persistence
			} catch (Exception e) {
				Ivy.log().error("Could not merge pdfs of <{0}> sent at {1}", e, mail.getSubject(), mail.getSentDate());
				if (firstPdf != null) {
					orderBlob = firstPdf;
				} else {
					orderBlob = report;
				}
			}
		} else {
			orderBlob = report;
		}
	}

	private BlobFile createNewPdfBlobFile(byte[] content, String fileName) {
		return createNewBlobFile(content, PDF_CONTENT_TYPE, "pdf", fileName);
	}

	private BlobFile createNewHtmlBlobFile(byte[] content, String fileName) {
		return createNewBlobFile(content, HTML_CONTENT_TYPE, "html", fileName);
	}

	private BlobFile createNewBlobFile(byte[] content, String contentType, String extension, String fileName) {
		BlobFile blobFile = new BlobFile();
		blobFile.setBytes(content);
		blobFile.setContentType(contentType);
		blobFile.setExtension(extension);
		blobFile.setFileName(fileName);
		return blobFile;
	}

	private StringBuilder buildReportHtml(String subject, Date sentDate, Address[] from, Address[] to, Address[] cc,
			List<String> fileNames, List<String> unmergedFileNames, String warning) {
		StringBuilder reportHtml = new StringBuilder("");
		String formattedDate = "-Datum unbekannt-";
		if (sentDate != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
			formattedDate = sdf.format(sentDate);
		}
		reportHtml.append("<html><body>");
		reportHtml.append("<table><tbody>");
		String sender = buildAddresText(from);
		String receiver = buildAddresText(to);
		String ccs = buildAddresText(cc);
		StringBuilder attachmentFileNames = new StringBuilder("");
		if (fileNames != null && fileNames.size() > 0) {
			for (String fileName : fileNames) {
				attachmentFileNames.append(fileName + "\n");
			}
		}
		reportHtml.append(reportRow("<b>Von:</b>", sender));
		reportHtml.append(reportRow("<b>Gesendet:</b>", formattedDate));
		reportHtml.append(reportRow("<b>An:</b>", receiver));
		reportHtml.append(reportRow("<b>Kopie an:</b>", ccs));
		reportHtml.append(reportRow("<b>Betreff:</b>", subject));
		reportHtml.append(reportRow("<b>Dateien:</b>", attachmentFileNames.toString()));
		if (warning != null && !warning.isEmpty()) {
			reportHtml.append(reportRow("<b>Warnung:</b>", warning));
		}
		reportHtml.append("</tbody></table>");
		if (unmergedFileNames != null && unmergedFileNames.size() > 0) {
			reportHtml.append("<p>&nbsp;<br />&nbsp;<br />&nbsp;<br /></p><hr />");
			reportHtml.append("<p style=\"color:red\"><b>Achtung!</b></p>");
			if (unmergedFileNames.size() > 1) {
				reportHtml
						.append("<p><b>Die folgenden Dateien wurden nicht an das Fax/email-PDF angeh�ngt.</b><br /><b>Die Dateien m�ssen f�r die Archivierung ausgedruckt und ins Post-Scanning �bergeben werden.</b><br />&nbsp;</p>");
			} else {
				reportHtml
						.append("<p><b>Die folgenden Datei wurde nicht an das Fax/email-PDF angeh�ngt.</b><br /><b>Die Datei muss f�r die Archivierung ausgedruckt und ins Post-Scanning �bergeben werden.</b><br />&nbsp;</p>");
			}
			reportHtml.append("<ul>");
			for (String fileName : unmergedFileNames) {
				reportHtml.append("<li><b>" + fileName + "</b></li>");
			}
			reportHtml.append("</ul>");
			reportHtml.append("<p>&nbsp;</p><hr />");
		}
		reportHtml.append("</body></html>");
		Ivy.log().debug("Converted Mail: {0}", reportHtml);
		return reportHtml;
	}

	private String buildAddresText(Address[] addresses) {
		StringBuilder receiver = new StringBuilder("");
		if (addresses != null) {
			for (Address adr : addresses) {
				if (receiver.length() > 0) {
					receiver.append(";");
				}
				receiver.append(adr.toString());
			}
		}
		return receiver.toString();
	}

	private String reportRow(String property, String value) {
		String escapedValue = value != null ? StringEscapeUtils.escapeHtml(value) : "";
		String html = String
				.format("<tr><td valign=\"top\" style=\"vertical-align:top;\" >%s</td><td>%s</td></tr>", property, escapedValue);
		return html;
	}

	private BlobFile toBlobFile(MailPart part) {
		String contentType = part.getContentType();
		byte[] content = part.getContent();
		String fileName = part.getFileName();
		String extension = findExtension(fileName);
		BlobFile blob = createNewBlobFile(content, contentType, extension, fileName);
		return blob;
	}

	private String escapeFileName(String subject) {
		if (subject != null) {
			subject = ch.soreco.utilities.file.FileUtils.escapeStringAsFilename(subject);
		}
		return subject;
	}

	private String findExtension(String fileName) {
		String extension = "";
		if (fileName != null) {
			int lastIndexOfDot = fileName.lastIndexOf(".") + 1;
			if (lastIndexOfDot > 0 && lastIndexOfDot < fileName.length()) {
				extension = fileName.substring(lastIndexOfDot).toLowerCase();
			}
		}
		return extension;
	}

	private String buildFileNameWithExtension(String fileName, String extension, String contentType) {
		if (extension != null) {
			fileName = fileName + "." + extension;
		} else {
			String guessedExtension = guessExtensionByContentType(contentType);
			if (guessedExtension != null) {
				fileName = fileName + guessedExtension;
			}
		}
		return fileName;
	}

	private String guessExtensionByContentType(String contentType) {
		String extension = null;
		if (contentType != null) {
			if (contentType.startsWith(HTML_CONTENT_TYPE)) {
				extension = ".html";
			} else if (contentType.startsWith(TXT_CONTENT_TYPE)) {
				extension = ".txt";
			} else if (contentType.startsWith(TIFF_CONTENT_TYPE)) {
				extension = ".tif";
			} else if (contentType.startsWith(PDF_CONTENT_TYPE)) {
				extension = ".pdf";
			}
		}
		return extension;
	}
}
