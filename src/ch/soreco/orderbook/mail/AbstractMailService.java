package ch.soreco.orderbook.mail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import ch.ivyteam.ivy.environment.Ivy;
import ch.soreco.orderbook.mail.domain.AbstractMailPart;
import ch.soreco.orderbook.mail.domain.AlternativeMailPart;
import ch.soreco.orderbook.mail.domain.Mail;
import ch.soreco.orderbook.mail.domain.MailPart;
import ch.soreco.orderbook.mail.exception.MailServiceException;
import ch.soreco.orderbook.mail.exception.mail.MessageContentReadingException;
import ch.soreco.utilities.file.FileUtils;

/**
 * An abstract class that contains the functionality common to messaging services, such as stores and transports.
 */
public abstract class AbstractMailService implements AutoCloseable {
	private Session session;
	private Store store;
	private Map<String, MessageFolder> folders;
	private static final String RFC822_CONTENT_TYPE = "message/rfc822";
	private static final String TXT_CONTENT_TYPE = "text/plain";
	private static final String HTML_CONTENT_TYPE = "text/html";
	private String connectionString;
	private Properties properties;

	/**
	 * Creates a new instance of {@link AbstractMailService}. Building a {@link Session} by given properties and its {@link Store}.
	 * 
	 * @param properties
	 *            Properties object. Used only if a new Session object is created. It is expected that the client supplies values for the
	 *            properties listed in Appendix A of the JavaMail spec (particularly mail.store.protocol, mail.transport.protocol,
	 *            mail.host, mail.user, and mail.from) as the defaults are unlikely to work in all cases.
	 * 
	 */
	public AbstractMailService(final Properties properties) throws MailServiceException {
		this.properties = properties;
		session = Session.getDefaultInstance(properties, null);
		try {
			store = session.getStore();
		} catch (MessagingException e) {
			throw new MailServiceException(e);
		}
	}

	/**
	 * 
	 * @return the store
	 */
	protected Store getStore() {
		return store;
	}

	/**
	 * The session from which the store was created.
	 * 
	 * @return the session
	 */
	protected Session getSession() {
		return session;
	}

	/**
	 * connects the service to the specified address.
	 * 
	 * @param host
	 *            the host to connect to
	 * @param port
	 *            the port to connect to (-1 means the default port)
	 * @param user
	 *            the user name
	 * @param password
	 *            this user's password
	 * @throws MailServiceException
	 */
	public void connect(final String host, final int port, final String user, final String password) throws MailServiceException {
		try {
			store.connect(host, port, user, password);
			if (this.properties != null && this.properties.get("mail.store.protocol") != null) {
				String protocol = properties.getProperty("mail.store.protocol");
				connectionString = String.format("%s://%s@%s:%s", protocol, user, host, String.valueOf(port));
			} else {
				connectionString = String.format("%s@%s:%s", user, host, String.valueOf(port));
			}
		} catch (MessagingException e) {
			throw new MailServiceException(e);
		}
	}

	public String getConnectionString() {
		return connectionString;
	}

	/**
	 * Check whether this Service is connected to store. Pings the server connection.
	 * 
	 * @return true if the service is connected, false if it is not connected
	 */
	public boolean isConnected() {
		boolean isConnected = store.isConnected();
		return isConnected;
	}

	public MessageFolder getMessageFolder(String name) throws MailServiceException {
		MessageFolder folder = null;
		if (folders != null && folders.containsKey(name)) {
			folder = folders.get(name);
		} else {
			if (folders == null) {
				folders = new HashMap<String, MessageFolder>();
			}
			folder = createMessageFolder(name);
			folders.put(name, folder);
		}
		return folder;
	}

	public abstract MessageFolder createMessageFolder(String name) throws MailServiceException;

	/**
	 * Set the specified flag on this message to the specified value. This will result in a MessageChangedEvent being delivered to any
	 * MessageChangedListener registered on this Message's containing folder. <br>
	 * <br>
	 * The default implementation uses the setFlags method.
	 * 
	 * @param message
	 * @param flags
	 *            Flags object containing the flags to be set
	 * @param value
	 *            the value to be set
	 * @throws MailServiceException
	 * @see {@link Message#setFlags(Flags, boolean)}
	 */
	public abstract void setFlags(Message message, Flags flags, boolean value) throws MailServiceException;

	public Mail parseMessageToMail(Message message, String uid) throws MailServiceException {
		Mail mail;
		try {
			mail = toMail(message, uid);
		} catch (MessagingException | IOException e) {
			throw new MailServiceException(e);
		}
		return mail;
	}

	private Mail toMail(Message message, String uid) throws MessagingException, IOException {
		Mail mail = new Mail();
		int messageNumber = message.getMessageNumber();
		mail.setMessageNumber(messageNumber);
		Date sentDate = message.getSentDate();
		mail.setSentDate(sentDate);
		String messageId = getMessageHeader(message, "Message-ID");
		mail.setMessageId(messageId);

		mail.setUid(uid);

		Address[] from = message.getFrom();
		mail.setFrom(from);
		Address[] to = message.getRecipients(RecipientType.TO);
		mail.setTo(to);
		Address[] cc = message.getRecipients(RecipientType.CC);
		mail.setCc(cc);
		Address[] bcc = message.getRecipients(RecipientType.BCC);
		mail.setBcc(bcc);
		String subject = message.getSubject();
		mail.setSubject(subject);
		List<AbstractMailPart> parts = collectMailParts(message);
		mail.setParts(parts);
		return mail;
	}

	private String getMessageHeader(Message message, String key) throws MessagingException {
		String[] msgId = message.getHeader(key);
		return StringUtils.join(msgId, "/");
	}

	private MailPart toMailPart(Part part) throws MessagingException, IOException {
		String disposition = part.getDisposition();
		String typeWithName = part.getContentType();
		String fileName = part.getFileName();
		try {
			fileName = decodeText(fileName);
		} catch (UnsupportedEncodingException e) {
			Ivy.log().error(String.format("Error during decoding of file name %s", fileName), e);
		}
		InputStream is = part.getInputStream();
		byte[] fileBytes = IOUtils.toByteArray(is);

		MailPart attachment = new MailPart();
		attachment.setDisposition(disposition);
		attachment.setContent(fileBytes);
		attachment.setContentType(typeWithName);
		attachment.setFileName(fileName);

		String[] contentId = part.getHeader("Content-ID");
		if (contentId != null && contentId.length > 0) {
			attachment.setContentId(StringUtils.join(contentId));
		}

		return attachment;
	}

	private MailPart toMailPart(MimeMessage message) throws MessagingException, IOException {
		String disposition = message.getDisposition();
		String fileName = message.getFileName();
		if (fileName != null) {
			try {
				fileName = decodeText(fileName);
			} catch (UnsupportedEncodingException e) {
				Ivy.log().error(String.format("Error during decoding of file name %s", fileName), e);
			}
		} else {
			String subject = message.getSubject();
			if (subject != null) {
				subject = FileUtils.escapeStringAsFilename(subject);
			}
		}
		if (fileName == null) {
			fileName = "NestedMessage";
		}
		fileName = fileName + ".eml";

		byte[] fileBytes = null;
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			message.writeTo(baos);
			fileBytes = baos.toByteArray();
		}

		MailPart attachment = new MailPart();
		attachment.setDisposition(disposition);
		attachment.setContent(fileBytes);
		attachment.setContentType(RFC822_CONTENT_TYPE);
		attachment.setFileName(fileName);

		String[] contentId = message.getHeader("Content-ID");
		if (contentId != null && contentId.length > 0) {
			attachment.setContentId(StringUtils.join(contentId));
		}

		return attachment;
	}

	private String decodeText(String text) throws UnsupportedEncodingException {
		String decoded = null;
		if (text != null) {
			decoded = MimeUtility.decodeText(text);
			// TODO: clarify if normalization is needed
			// decoded= Normalizer.normalize(decoded, Normalizer.Form.NFC);
		}
		return decoded;
	}

	/**
	 * http://www.tutorialspoint.com/javamail_api/javamail_api_fetching_emails. htm
	 * 
	 * @param part
	 * @return
	 * @throws IOException
	 * @throws MessagingException
	 */
	private List<AbstractMailPart> collectMailParts(Part part) throws IOException, MessagingException {
		List<AbstractMailPart> result = new ArrayList<AbstractMailPart>();
		Object content = part.getContent();

		if (content instanceof ByteArrayInputStream) {
			ByteArrayInputStream stream = (ByteArrayInputStream) content;
			MimeMessage message = new MimeMessage(session, stream);
			part = message;
			content = message.getContent();
		}
		if (part.isMimeType(RFC822_CONTENT_TYPE)) {
			MimeMessage message = (MimeMessage) content;
			MailPart nestedMailPart = toMailPart(message);
			result.add(nestedMailPart);
		} else if (content instanceof Multipart && part.isMimeType("multipart/alternative")) {
			Multipart alternativeMultipart = (Multipart) content;
			int count = alternativeMultipart.getCount();
			AlternativeMailPart alternativeMailPart = new AlternativeMailPart();
			for (int i = 0; i < count; i++) {
				Part bodyPart = alternativeMultipart.getBodyPart(i);
				if (bodyPart.isMimeType(TXT_CONTENT_TYPE)) {
					MailPart alternativePlainText = toMailPart(bodyPart);
					alternativeMailPart.add(alternativePlainText);
				} else if (bodyPart.isMimeType(HTML_CONTENT_TYPE)) {
					MailPart alternativeHtmlText = toMailPart(bodyPart);
					alternativeMailPart.add(alternativeHtmlText);
				} else {
					List<AbstractMailPart> otherAlternativeParts = collectMailParts(bodyPart);
					alternativeMailPart.addAll(otherAlternativeParts);
				}
			}
			result.add(alternativeMailPart);

		} else if (content instanceof InputStream || content instanceof String) {
			AbstractMailPart AbstractMailPart = toMailPart(part);
			result.add(AbstractMailPart);
		} else if (content instanceof Multipart) {
			Multipart multipart = (Multipart) content;
			for (int i = 0; i < multipart.getCount(); i++) {
				BodyPart bodyPart = multipart.getBodyPart(i);
				List<AbstractMailPart> bodyPartAttachments = collectMailParts(bodyPart);
				if (bodyPartAttachments != null) {
					result.addAll(bodyPartAttachments);
				}
			}
		} else if (content != null) {
			String contentType = part.getContentType();
			String fileName = part.getFileName();
			String disposition = part.getDisposition();
			int size = part.getSize();
			String exception = String.format("No Content Data Handler for %s: contentType:%s, fileName:%s, disposition:%s, size:%d",
					content.getClass().getName(), contentType, fileName, disposition, size);
			// bodyPart.writeTo(arg0);
			throw new MessageContentReadingException(exception);
		}
		return result;
	}

	/**
	 * Closes the store service and terminate its connection. <br>
	 * Any Messaging components (Folders, Messages, etc.) belonging to this service are invalid after this service is closed. Note that the
	 * service is closed even if this method terminates abnormally by throwing a MessagingException.
	 */
	public void close() {
		if (this.folders != null) {
			for (MessageFolder folder : folders.values()) {
				folder.close();
			}
			folders.clear();
		}
		if (this.store != null) {
			try {
				this.store.close();
			} catch (MessagingException e) {
				Ivy.log().error("Could not close the mail store", e);
			}
		}
	}
}
