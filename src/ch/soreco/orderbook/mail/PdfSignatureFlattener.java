package ch.soreco.orderbook.mail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.aspose.pdf.facades.PdfFileSignature;
import com.axonivy.converter.pdf.AsposeLicenseLoader;

public class PdfSignatureFlattener {
	public PdfSignatureFlattener() {
		AsposeLicenseLoader.loadPdfLicense();
	}

	public String signatureReport(InputStream in) {
		PdfFileSignature pdfSign = new PdfFileSignature();
		pdfSign.bindPdf(in);
		List<?> names = pdfSign.getSignNames();
		StringBuilder report = new StringBuilder("");
		for (int i = 0; i < names.size(); i++) {
			report.append("\tsignature name:" + (String) names.get(i));
			report.append(" coverswholedocument:" + pdfSign.isCoversWholeDocument((String) names.get(i)));
			report.append(" revision:" + pdfSign.getRevision((String) names.get(i)));
			report.append(" verifysigned:" + pdfSign.verifySigned((String) names.get(i)));
			report.append(" reason:" + pdfSign.getReason((String) names.get(i)));
			report.append(" location:" + pdfSign.getLocation((String) names.get(i)));
			report.append(" datatime:" + pdfSign.getDateTime((String) names.get(i)));
			report.append("\n");
		}
		report.append(" totalvision:" + pdfSign.getTotalRevision());
		return report.toString();
	}

	public byte[] flatten(InputStream in) throws IOException {
		PdfFileSignature pdfSign = new PdfFileSignature();
		pdfSign.bindPdf(in);
		if (pdfSign.isContainSignature()) {
			List<?> names = pdfSign.getSignNames();
			for (int i = 0; i < names.size(); i++) {
				pdfSign.removeSignature((String) names.get(i));
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			pdfSign.save(baos);
//			Ivy.log().debug("PDF was signed with {0}", names.size());
			return baos.toByteArray();
		} else {
//			Ivy.log().debug("PDF is not signed");
			return IOUtils.toByteArray(in);
		}
	}
}
