package ch.soreco.utilities.file;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileUtils {
	/**
	 * The pattern below is based on a conservative subset of allowed characters
	 * in the POSIX spec.
	 */
	public static final Pattern POSIX_PATTERN = Pattern.compile("[^A-Za-z0-9_\\-]");
	public static final Pattern FAT32_PATTERN = Pattern.compile("[%\\.\"\\*/:<>\\?\\\\\\|\\+,\\.;=\\[\\]]");
	public static final int DEFAULT_LIMIT = 255;
	private static final String EMPTY_STRING = "";
	private static final String[][] UMLAUT_REPLACEMENTS = new String[][] { { "�", "ue" }, { "�", "oe" }, { "�", "ae" },
			{ "�", "ss" }, { "�(?=[a-z���� ])", "Ue" }, { "�(?=[a-z���� ])", "Oe" }, { "�(?=[a-z���� ])", "Ae" },
			{ "�", "UE" }, { "�", "OE" }, { "�", "AE" } };

	/**
	 * Get a string escaped for use in file systems.<br>
	 * This method uses the pattern <code>[^A-Za-z0-9_\\-]</code> to match
	 * illegal characters, replaces them with an empty String and constrains the
	 * length of the encoded string to the limit of 255 characters.
	 * 
	 * @param text
	 *            the string to escape
	 * @return the escaped text
	 * 
	 */
	public static String escapeStringAsFilename(String text) {
		text = escapeStringAsFilename(POSIX_PATTERN, text, EMPTY_STRING, DEFAULT_LIMIT);
		return text;
	}

	/**
	 * Get a string escaped for use in file systems.<br>
	 * This method uses a RegEx to match illegal characters and constrains the
	 * length of the encoded string to given limit.
	 * 
	 * @param text
	 *            the string to escape
	 * @param replacement
	 *            the character to replace illegal characters with
	 * @param limit
	 *            the maximum length of the file name
	 * @return the escaped text
	 * 
	 */
	public static String escapeStringAsFilename(Pattern pattern, String text, String replacement, int limit) {
		// escape may existing umlaute
		text = escapeUmlaute(text);

		StringBuffer sb = new StringBuffer();
		// Apply the regex.
		Matcher matcher = pattern.matcher(text);

		while (matcher.find()) {
			// Convert matched character to percent-encoded.
			// String replacement = "%" +
			// Integer.toHexString(matcher.group().charAt(0)).toUpperCase();
			matcher.appendReplacement(sb, replacement);
		}
		matcher.appendTail(sb);

		String encoded = sb.toString();
		// Truncate the string.
		int end = Math.min(encoded.length(), limit);
		return encoded.substring(0, end);
	}

	/**
	 * Get a string escaped for use in file systems.<br>
	 * This method uses a RegEx to match illegal characters, percent-encodes
	 * them and constrains the length of the encoded string to given limit.
	 * 
	 * @param text
	 *            the string to escape
	 * @param replacement
	 *            the character to replace illegal characters with
	 * @param limit
	 *            the maximum length of the file name
	 * @return the escaped text
	 * 
	 */
	public static String escapeStringAsFilename(Pattern pattern, String text, int limit) {
		StringBuffer sb = new StringBuffer();
		// Apply the regex.
		Matcher matcher = pattern.matcher(text);

		while (matcher.find()) {
			// Convert matched character to percent-encoded.
			String replacement = "%" + Integer.toHexString(matcher.group().charAt(0)).toUpperCase();
			matcher.appendReplacement(sb, replacement);
		}
		matcher.appendTail(sb);

		String encoded = sb.toString();
		// Truncate the string.
		int end = Math.min(encoded.length(), limit);
		return encoded.substring(0, end);
	}

	/**
	 * Replaces all german umlauts in the given text with the usual replacement
	 * scheme, also taking into account capitalization. A test String such as
	 * "K�se K�ln F��e �l �bel �� �� ��� � � � �BUNG" will yield the result
	 * "Kaese Koeln Fuesse Oel Uebel Aeue Uess AEOEUe Ae Oe Ue UEBUNG"
	 * 
	 * @param text
	 *            the text to escape umlauts of
	 * @return the input string with replaced umlauts
	 */
	public static String escapeUmlaute(String text) {

		String result = text;
		if (result != null) {
			for (String[] umlautReplacement : UMLAUT_REPLACEMENTS) {
				String regex = umlautReplacement[0];
				String replacement = umlautReplacement[1];
				result = result.replaceAll(regex, replacement);
			}
		}
		return result;
	}

	/**
	 * Loads all of the properties represented by the XML document on the
	 * specified path into this properties table. The XML document must have the
	 * following DOCTYPE declaration:
	 * 
	 * <code><!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd"></code>
	 * 
	 * Furthermore, the document must satisfy the properties DTD described
	 * above. The specified stream is closed after this method returns.
	 * 
	 * 
	 * @param filePath
	 *            the path to XML document
	 * @return the loaded {@link Properties} or null
	 * @see Properties#loadFromXML(java.io.InputStream)
	 */
	public static Properties loadPropertiesFromResource(final String name) {
		Properties properties = null;
		InputStream is = getResourceAsStream(name);
		if (is != null) {
			try {
				properties = new Properties();
				properties.loadFromXML(is);
			} catch (IOException e) {
				return null;
			}
		}
		return properties;
	}
	/**
	 * Returns an input stream for reading the specified resource. The search
	 * order is described in the documentation for
	 * {@link ClassLoader#getResource(String)}.
	 * 
	 * @param name
	 *            The resource name
	 * @return An input stream for reading the resource, or null if the resource
	 *         could not be found
	 */
	private static InputStream getResourceAsStream(String name) {
		URL url = getResource(name);
		try {
			return url != null ? url.openStream() : null;
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Finds the resource with the given name. A resource is some data (images,
	 * audio, text, etc) that can be accessed by class code in a way that is
	 * independent of the location of the code. The name of a resource is a
	 * '/'-separated path name that identifies the resource. <br>
	 * <br>
	 * This method will first search the parent class loader for the resource;
	 * if the parent is null the path of the class loader built-in to the
	 * virtual machine is searched. That failing, this method will invoke
	 * {@link ClassLoader#findResource(String)} to find the resource.
	 * 
	 * @param name
	 *            The resource name
	 * @return A URL object for reading the resource, or null if the resource
	 *         could not be found or the invoker doesn't have adequate
	 *         privileges to get the resource.
	 */
	public static URL getResource(String name) {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		URL url = classloader.getResource(name);
		if (url == null && name != null && name.length() > 0 && (name.charAt(0) != '/')) {
			url = classloader.getResource('/' + name);
		}
		return url;
	}
}
