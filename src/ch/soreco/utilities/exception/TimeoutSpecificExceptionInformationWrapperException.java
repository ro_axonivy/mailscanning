package ch.soreco.utilities.exception;

/**
 * Can be used to create a timeout specific exception information wrapper. When using this class it is recommended to
 * only use the default (i.e. empty) constructor. The other constructors have the same functionality as with
 * {@link ExceptionInformationWrapperException}.
 */
public class TimeoutSpecificExceptionInformationWrapperException extends ExceptionInformationWrapperException {
	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_CAUSING_MESSAGE = "A timeout occured";
	private static final String DEFAULT_STACKTRACE = "n/a";

	/**
	 * Creates an exception wrapper that contains all data from a given {@link String}. This constructor is used on the
	 * exception event start to create an instance. <br />
	 * <br />
	 * <b>When the wrapper is created through this constructor the given encoded message is ignored.</b> <br />
	 * <br />
	 * The form of the String must be:<br />
	 * java.lang.RuntimeException:::DetailedMessag:::Stacktrace
	 * 
	 * @param encodedExceptionInformation
	 *            The encoded exception string.
	 */
	public TimeoutSpecificExceptionInformationWrapperException(String encodedExceptionInformation) {
		super(encodedExceptionInformation);
	}

	/**
	 * Creates an exception wrapper that contains all data from a given {@link Throwable}. This constructor is used on
	 * the site where the exception happens to encode the information. <br />
	 * <br />
	 * <b>When the wrapper is created through this constructor the given cause is ignored.</b> <br />
	 * <br />
	 * 
	 * @param cause
	 *            The exception that shall be encoded.
	 */
	public TimeoutSpecificExceptionInformationWrapperException(Throwable cause) {
		super(cause);
	}

	/**
	 * Creates a timeout specific exception information wrapper. The stacktrace as string is 'n/a' and the causing
	 * message is 'A timeout occured'.
	 */
	public TimeoutSpecificExceptionInformationWrapperException() {
	}

	/**
	 * Always returns the default causing message.
	 */
	@Override
	public String getCausingMessage() {
		return DEFAULT_CAUSING_MESSAGE;
	}

	/**
	 * Always returns the default stacktrace as string.
	 */
	@Override
	public String getStackTraceAsString() {
		return DEFAULT_STACKTRACE;
	}
}