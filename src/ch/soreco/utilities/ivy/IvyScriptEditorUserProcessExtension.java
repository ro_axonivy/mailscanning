package ch.soreco.utilities.ivy;


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ScrollPaneConstants;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.process.engine.IRequestId;
import ch.ivyteam.ivy.process.extension.IIvyScriptEditor;
import ch.ivyteam.ivy.process.extension.IProcessExtensionConfigurationEditorEnvironment;
import ch.ivyteam.ivy.process.extension.impl.AbstractProcessExtensionConfigurationEditor;
import ch.ivyteam.ivy.process.extension.impl.AbstractUserProcessExtension;
import ch.ivyteam.ivy.scripting.exceptions.invocation.IvyScriptMethodInvocationException;
import ch.ivyteam.ivy.scripting.language.IIvyScriptContext;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.ivyteam.util.IvyRuntimeException;
import ch.soreco.utilities.exception.ExceptionInformationWrapperException;

/**
 * This PI step just runs the provided piece of ivy script code. <br/>
 * <b>It makes the script execution exception aware.</b>
 */
public class IvyScriptEditorUserProcessExtension extends AbstractUserProcessExtension {

	/**
	 * Runs the provided ivy script from the editor tab.
	 */
	@Override
	public CompositeObject perform(IRequestId requestId, CompositeObject in, IIvyScriptContext context)
			throws Exception {
		try {
			String ivyScriptToExecute = extractIvyScriptToExecute();
			executeIvyScript(context, ivyScriptToExecute);
		} catch (Throwable e) {
			e = findRootCauseIfItsAnIvyScriptError(e);
			Ivy.log().error("An error occured while executing the script.");
			ExceptionInformationWrapperException exceptionInformationException = new ExceptionInformationWrapperException(
					e);
			throw exceptionInformationException;
		}
		return in;
	}

	/**
	 * If the exception occurred in the code that was called by the given ivy script, the exception is wrapped into an
	 * {@link IvyScriptMethodInvocationException} that is wrapped into another exception and so on. This method extracts
	 * the real cause from the given {@link Throwable} and returns it. <br/>
	 * <i>Note: Without this method, the error in the UI would always be an {@link IvyRuntimeException} that tells you
	 * that line xy of block yz had an error, but you have no clue what the problem actually could have been at that
	 * point.</i>
	 */
	private Throwable findRootCauseIfItsAnIvyScriptError(Throwable throwable) {
		Throwable cause = throwable.getCause();
		if (cause == null) {
			return throwable;
		}

		boolean thereIsADeeperCause = true;
		while (thereIsADeeperCause) {
			if (IvyScriptMethodInvocationException.class.isAssignableFrom(cause.getClass())) {
				IvyScriptMethodInvocationException ivyScriptMethodInvocationException = (IvyScriptMethodInvocationException) cause;
				Throwable ivyScriptMethodInvocationExceptionCause = ivyScriptMethodInvocationException.getCause();
				if (ivyScriptMethodInvocationExceptionCause != null) {
					throwable = ivyScriptMethodInvocationExceptionCause;
				}
				break;
			}
			cause = cause.getCause();
			thereIsADeeperCause = cause != null && cause.getCause() != cause;
		}

		return throwable;
	}

	private String extractIvyScriptToExecute() throws IOException {
		String ivyScriptToExecute = getConfigurationProperty(Editor.IVY_SCRIPT_TO_EXECUTE_FIELD_IDENTIFIER);
		return ivyScriptToExecute;
	}

	/**
	 * The {@link Editor} provides a field that a user of the PI step can use to provide the Ivy script that should be
	 * executed.
	 */
	public static class Editor extends AbstractProcessExtensionConfigurationEditor {

		private static final String IVY_SCRIPT_TO_EXECUTE_FIELD_IDENTIFIER = IvyScriptEditorUserProcessExtension.class
				.getName() + ".IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD";
		private static final String IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD_LABEL_TEXT = "The ivy script to be executed in this step";
		private IIvyScriptEditor ivyScriptToExecuteField;

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected void createEditorPanelContent(Container editorPanel,
				IProcessExtensionConfigurationEditorEnvironment editorEnvironment) {

			editorPanel.setLayout(new BorderLayout());
			JPanel form = new JPanel();
			form.setLayout(new GridBagLayout());
			JLabel ivyScriptToExecuteScriptFieldLabel = new JLabel(IVY_SCRIPT_TO_EXECUTE_SCRIPT_FIELD_LABEL_TEXT);
			form.add(ivyScriptToExecuteScriptFieldLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
					GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 0, 0));

			int rows = 30;
			ivyScriptToExecuteField = editorEnvironment.createIvyScriptEditor(null, null, null, rows, false,
					ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

			form.add(ivyScriptToExecuteField.getComponent(), new GridBagConstraints(0, 1, 1, 20, 1.0, 1.0,
					GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 10, 0, 0), 0, 0));
			
			editorPanel.add(form, BorderLayout.CENTER);
			
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		protected void loadUiDataFromConfiguration() {
			String ivyScriptToExecute = getBeanConfigurationProperty(IVY_SCRIPT_TO_EXECUTE_FIELD_IDENTIFIER);
			ivyScriptToExecuteField.setText(ivyScriptToExecute);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected boolean saveUiDataToConfiguration() {
			setBeanConfigurationProperty(IVY_SCRIPT_TO_EXECUTE_FIELD_IDENTIFIER, ivyScriptToExecuteField.getText());
			return true;
		}
	}

}
