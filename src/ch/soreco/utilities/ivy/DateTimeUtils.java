package ch.soreco.utilities.ivy;

import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.ivyteam.ivy.scripting.objects.Duration;

public class DateTimeUtils {
	private DateTimeUtils() {

	}

	/**
	 * Creates a new DateTime object representing the actual time.
	 * 
	 * @return a new {@link DateTime}
	 */
	public static DateTime now() {
		return new DateTime();
	}

	/**
	 * Checks whether the given dateTime is null or before or equal to
	 * {@link #now()}.
	 * 
	 * @param dateTime
	 *            the dateTime to check
	 * @return true if the dateTime is null or the dateTime is before or equal
	 *         to {@link #now()}.
	 */
	public static boolean isBeforeOrEqualToNow(DateTime dateTime) {
		return dateTime == null || (now().compareTo(dateTime) >= 0);
	}
	/**
	 * Adds a duration to given date time.
	 * 
	 * @param durationLiteral
	 *            A duration literal has the following format:
	 *            P[n]Y[n]M[n]DT[n]H[n]M[n]S. Elements may be omitted if their
	 *            value is zero.<br>
	 *            You enter a duration in the ISO 8601 time period format such
	 *            as: <code>12h20m</code> or <code>12h20m30s</code>. An example
	 *            for the full format is: <code>P3Y6M4DT12h30m10s</code>
	 *            <ul>
	 *            <li>P stands for Period</li>
	 *            <li>Y for setting the years.</li>
	 *            <li>M for setting the months.</li>
	 *            <li>D for setting the days.</li>
	 *            <li>T is the separator between the date fields and the time
	 *            fields.</li>
	 *            <li>H or h for setting the hours.</li>
	 *            <li>M or m for setting the minutes.</li>
	 *            <li>S or s for setting the seconds.</li>
	 *            </ul>
	 * 
	 * @param dateTime
	 *            the date to add the duration too.
	 * @return A new date object translated by the duration.
	 */
	public static DateTime add(String durationLiteral, DateTime dateTime) {
		DateTime newDate = null;
		if (durationLiteral != null && !durationLiteral.trim().isEmpty()) {
			Duration duration = toDuration(durationLiteral);
			newDate = add(duration, dateTime);
		}
		return newDate;
	}

	/**
	 * Adds a duration to given date time.
	 * 
	 * @param duration
	 *            A duration
	 * @param dateTime
	 *            the date to add the duration too.
	 * @return A new date object translated by the duration or null if dateTime
	 *         is not set.
	 */
	public static DateTime add(Duration duration, DateTime dateTime) {
		if (dateTime != null) {
			// the resulting DateTime will be cloned before add of duration.
			dateTime = dateTime.add(duration);
		}
		return dateTime;
	}

	/**
	 * Parses a Duration literal and creates a new {@link Duration} object.
	 * 
	 * @param literal
	 *            A duration literal has the following format:
	 *            P[n]Y[n]M[n]DT[n]H[n]M[n]S. Elements may be omitted if their
	 *            value is zero.<br>
	 *            You enter a duration in the ISO 8601 time period format such
	 *            as: <code>12h20m</code> or <code>12h20m30s</code>. An example
	 *            for the full format is: <code>P3Y6M4DT12h30m10s</code>
	 *            <ul>
	 *            <li>P stands for Period</li>
	 *            <li>Y for setting the years.</li>
	 *            <li>M for setting the months.</li>
	 *            <li>D for setting the days.</li>
	 *            <li>T is the separator between the date fields and the time
	 *            fields.</li>
	 *            <li>H or h for setting the hours.</li>
	 *            <li>M or m for setting the minutes.</li>
	 *            <li>S or s for setting the seconds.</li>
	 *            </ul>
	 * @return a new {@link Duration}
	 * @throws IllegalArgumentException
	 *             if the argument has an illegal format.
	 */
	public static Duration toDuration(String literal) {
		return new Duration(literal);
	}
}
